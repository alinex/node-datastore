# Alinex Data Store

The data store is NodeJS module, which helps to work with complex structured data like configurations or data structures. It allows you to read, work and write in different formats and therefore also to transform between them.

## Documentation

Find a complete manual under [alinex.gitlab.io/node-datastore](https://alinex.gitlab.io/node-datastore).

If you want to have an offline access to the documentation, feel free to download the [PDF Documentation](https://alinex.gitlab.io/node-datastore/datastore-book.pdf).

## License

(C) Copyright 2019 Alexander Schilling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

> <https://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
