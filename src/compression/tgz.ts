import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import * as zlib from 'zlib';
import * as tar from 'tar-stream';
import * as streamBuffers from 'stream-buffers';
import { ExitError } from '@alinex/core';

import { Compressor } from './index';

const debug = Debug('datastore:compression:tar');

export const uncompress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`uncompressing ${basename(parsedUri.pathname)}`);
        let archivePath = parsedUri.hash && parsedUri.hash.substr(1);
        // uncompressing
        buffer = await new Promise((resolve, reject) => {
            zlib.gunzip(buffer, {}, function(error, result) {
                if (!error) resolve(result);
                else reject(error);
            });
        });
        // buffer to stream
        const writer = new streamBuffers.WritableStreamBuffer();
        const reader = new streamBuffers.ReadableStreamBuffer();
        reader.put(buffer);
        reader.stop();
        // stepping through archive
        return new Promise(resolve => {
            debug(`extracting ${archivePath}`);
            var extract = tar.extract();
            extract.on('finish', resolve);
            extract.on('entry', function(header, stream, next) {
                stream.on('end', next);
                if (archivePath) {
                    if (header.name == archivePath) {
                        // reading file
                        stream.pipe(writer);
                    } else {
                        stream.resume(); // just auto drain the stream
                    }
                } else {
                    archivePath = header.name;
                    parsedUri.hash = `#${archivePath}`;
                    stream.pipe(writer);
                }
            });
            reader.pipe(extract);
        }).then(() => {
            // return result
            const result = writer.getContents();
            if (!result) throw new Error('Found no content in archive.');
            return result;
        });
    }
);

export const compress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`compressing ${basename(parsedUri.pathname)}`);
        if (!parsedUri.hash) throw new ExitError('No file specified in hash part of URL.');
        let archivePath = parsedUri.hash && parsedUri.hash.substr(1);
        // buffer to stream
        const writer = new streamBuffers.WritableStreamBuffer();
        // creating archive
        buffer = await new Promise(resolve => {
            debug(`including as ${archivePath}`);
            writer.on('finish', resolve);
            var pack = tar.pack();
            pack.entry({ name: archivePath }, buffer);
            pack.finalize();
            pack.pipe(writer);
        }).then(() => {
            // return result
            const result = writer.getContents();
            if (!result) throw new Error('Found no content in archive.');
            return result;
        });
        // compressing
        return new Promise((resolve, reject) => {
            zlib.gzip(buffer, { level: (options && options.compressionLevel) || 9 }, function(
                error,
                result
            ) {
                if (!error) resolve(result);
                else reject(error);
            });
        });
    }
);
