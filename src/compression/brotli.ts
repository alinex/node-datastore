import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import * as zlib from 'zlib';

import { Compressor } from './index';

const debug = Debug('datastore:compression:brotli');

export const uncompress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`uncompressing ${basename(parsedUri.pathname)}`);
        return new Promise((resolve, reject) => {
            zlib.brotliDecompress(buffer, function(error, result) {
                if (!error) resolve(result);
                else reject(error);
            });
        });
    }
);
export const compress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`compressing ${basename(parsedUri.pathname)}`);
        return new Promise((resolve, reject) => {
            zlib.brotliCompress(buffer, function(error, result) {
                if (!error) resolve(result);
                else reject(error);
            });
        });
    }
);
