import { basename, extname } from "path";
import { URL } from "url";
import Debug from "debug";

const debug = Debug("datastore:compression");

export type Compressor = (parsedUri: URL, buffer: Buffer, options?: any) => Promise<Buffer>;

export interface Handler {
    compress: Compressor;
    uncompress: Compressor;
}

export type Compression = "gzip" | "deflate" | "brotli" | "bzip2" | "lzma" | "tar" | "tgz" | "tbz2" | "tlz" | "zip";

export const compressions = ["gzip", "deflate", "brotli", "bzip2", "lzma", "tar", "tgz", "tbz2", "tlz", "zip"];

const alias: { [key: string]: string } = {
    br: "brotli",
    gz: "gzip",
    "tar.gz": "tgz",
    "tar.bz2": "tbz2",
    "tar.lzma": "tlz"
};

function detect(pathname: string): string | null {
    if (!extname(pathname)) return null;
    // double extension
    let ext =
        extname(basename(pathname, extname(pathname)))
            .replace(/^\./, "")
            .toLowerCase() + extname(pathname).toLowerCase();
    if (alias[ext]) ext = alias[ext];
    if (compressions.indexOf(ext) !== -1) return ext;
    // single extension
    ext = extname(pathname)
        .replace(/^\./, "")
        .toLowerCase();
    if (alias[ext]) ext = alias[ext];
    if (compressions.indexOf(ext) !== -1) return ext;
    return detect(basename(pathname, extname(pathname)));
}

const handler = function(parsedUri: URL, options: any): Promise<Handler> {
    let ext: string | null = null;
    if (options && options.compression) {
        ext = options.compression;
        if (ext) {
            if (alias[ext]) ext = alias[ext];
            if (compressions.indexOf(ext) == -1) ext = null;
        }
    } else {
        ext = detect(parsedUri.pathname);
    }
    if (!ext) ext = "none";
    return import(`./${ext}`);
};

export const uncompress = <Compressor>async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
    if (debug.enabled) {
        const size = buffer.length;
        return handler(parsedUri, options).then(mod =>
            mod.uncompress(parsedUri, buffer).then(buffer => {
                if (buffer.length != size) debug(`size ${size} bytes -> ${buffer.length} bytes`);
                return buffer;
            })
        );
    }
    return handler(parsedUri, options).then(mod => mod.uncompress(parsedUri, buffer));
};
export const compress = <Compressor>async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
    if (debug.enabled) {
        const size = buffer.length;
        return handler(parsedUri, options).then(mod =>
            mod.compress(parsedUri, buffer).then(buffer => {
                if (buffer.length != size) debug(`size ${size} bytes -> ${buffer.length} bytes`);
                return buffer;
            })
        );
    }
    return handler(parsedUri, options).then(mod => mod.compress(parsedUri, buffer));
};

export default { compress, uncompress };
