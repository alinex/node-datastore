import { URL } from 'url';

import { Compressor } from './index';

export const uncompress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        return buffer;
    }
);
export const compress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        return buffer;
    }
);
