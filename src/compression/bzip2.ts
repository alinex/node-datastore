import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
// @ts-ignore
import { Bzip2 } from 'compressjs';

import { Compressor } from './index';

const debug = Debug('datastore:compression:gzip');

export const uncompress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`uncompressing ${basename(parsedUri.pathname)}`);
        return Buffer.from(Bzip2.decompressFile(buffer));
    }
);
export const compress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`compressing ${basename(parsedUri.pathname)}`);
        return Buffer.from(Bzip2.compressFile(buffer));
    }
);
