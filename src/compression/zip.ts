import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import * as JSZip from 'jszip';
import { ExitError } from '@alinex/core';

import { Compressor } from './index';

const debug = Debug('datastore:compression:tar');

export const uncompress = <Compressor>(
    async function (parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`opening ${basename(parsedUri.pathname)}`);
        return JSZip.loadAsync(buffer).then(function (zip) {
            const files = Object.keys(zip.folder('')!.files);
            let archivePath = '';
            if (files.length == 1) {
                archivePath = files[0];
                parsedUri.hash = `#${archivePath}`;
            } else {
                if (!parsedUri.hash) throw new ExitError('No file specified in hash part of URL.');
                archivePath = parsedUri.hash.substr(1);
            }
            debug(`extracting ${archivePath}`);
            return zip.file(archivePath)!.async('nodebuffer');
        });
    }
);

export const compress = <Compressor>(
    async function (parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`compressing ${basename(parsedUri.pathname)}`);
        if (!parsedUri.hash) throw new ExitError('No file specified in hash part of URL.');
        const archivePath = parsedUri.hash.substr(1);
        const zip = new JSZip();
        debug(`including as ${archivePath}`);
        zip.file(archivePath, buffer);
        return zip.generateAsync({
            type: 'nodebuffer',
            compression: options && options.compressionLevel ? 'DEFLATE' : 'STORE',
            compressionOptions: { level: options && options.compressionLevel }
        });
    }
);
