import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import * as zlib from 'zlib';

import { Compressor } from './index';

const debug = Debug('datastore:compression:gzip');

export const uncompress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`uncompressing ${basename(parsedUri.pathname)}`);
        return new Promise((resolve, reject) => {
            zlib.gunzip(buffer, {}, function(error, result) {
                if (!error) resolve(result);
                else reject(error);
            });
        });
    }
);
export const compress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`compressing ${basename(parsedUri.pathname)}`);
        return new Promise((resolve, reject) => {
            zlib.gzip(buffer, { level: (options && options.compressionLevel) || 9 }, function(
                error,
                result
            ) {
                if (!error) resolve(result);
                else reject(error);
            });
        });
    }
);
