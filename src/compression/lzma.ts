import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
// @ts-ignore
import * as lzma from 'lzma';

import { Compressor } from './index';

const debug = Debug('datastore:compression:gzip');

export const uncompress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`uncompressing ${basename(parsedUri.pathname)}`);
        return new Promise((resolve, reject) => {
            lzma.decompress(buffer, (result: Buffer, error: any) => {
                if (error) return reject(error);
                resolve(Buffer.from(result));
            });
        });
    }
);
export const compress = <Compressor>(
    async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
        debug(`compressing ${basename(parsedUri.pathname)}`);
        return new Promise((resolve, reject) => {
            lzma.compress(
                buffer,
                (options && options.compressionLevel) || 9,
                (result: Buffer, error: any) => {
                    if (error) return reject(error);
                    resolve(Buffer.from(result));
                }
            );
        });
    }
);
