import { basename } from "path";
import { URL } from "url";
import Debug from "debug";
import * as zlib from "zlib";

import { Compressor } from "./index";

const debug = Debug("datastore:compression:deflate");

export const uncompress = <Compressor>async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
    debug(`uncompressing ${basename(parsedUri.pathname)}`);
    return new Promise((resolve, reject) => {
        zlib.inflate(buffer, {}, function(error, result) {
            if (!error) resolve(result);
            else reject(error);
        });
    });
};
export const compress = <Compressor>async function(parsedUri: URL, buffer: Buffer, options: any): Promise<Buffer> {
    debug(`compressing ${basename(parsedUri.pathname)}`);
    return new Promise((resolve, reject) => {
        zlib.deflate(buffer, { level: (options && options.compressionLevel) || 9 }, function(error, result) {
            debug("xxxxx", result, error);
            if (!error) resolve(result);
            else reject(error);
        });
    });
};
