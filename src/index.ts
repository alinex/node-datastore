import { resolve } from "path";
import { URL } from "url";
import * as objectPath from "object-path";
import async from "@alinex/async";
import * as data from "@alinex/data";
import { MergeInput } from "@alinex/data/lib/merge";
import Debug from "debug";
import * as util from "util";

import protocol from "./protocol";
import * as compressor from "./compression";
import * as formatter from "./format";

const debug = Debug("datastore");

// enable details debugging for core debug usages

const debugDetails = Debug("datastore:details");

let debuglog = util.debuglog;
if (debugDetails.enabled) {
    process.env.NODE_DEBUG = process.env.NODE_DEBUG ? `${process.env.NODE_DEBUG},request` : "request";
    // @ts-ignore
    util.debuglog = (set: string) => {
        //if (set === 'https') return debugDetails;
        debugDetails(`See more using NODE_DEBUG=${set}`);
        return debuglog(set);
    };
}

function parseUri(uri: string): URL {
    if (!uri.match(/^[a-z]+:/)) uri = `file:${resolve(uri)}`;
    let parsed = new URL(uri);
    return parsed;
}

interface Options {
    /**
     * Proxy <host>:<port> to be used for HTTP/HTTPS.
     */
    proxy?: string;
    /**
     * One of: get, delete, head, options, post, put, patch (default: get)
     */
    httpMethod?: string;
    /**
     * Additional HTTP/HTTPS header to send
     */
    httpHeader?: string[];
    /**
     * Additional data to be send as body within HTTP/HTTPS POST or PUT calls
     */
    httpData?: string;
    /**
     * Ignore response code and also go on if not code 2xx.
     */
    ignoreError?: boolean;
    /**
     * Private Open SSH Key used to access with SFTP protocol.
     */
    privateKey?: Buffer | string;
    /**
     * Passphrase for the private Open SSH Key.
     */
    passphrase?: string;
    /**
     * Maximum number of lines to read for file protocol.
     */
    tail?: number;
    /**
     * Search path for postgres queries
     */
    search?: string;
    /**
     * Compression method to use (autodetected from URL).
     */
    compression?: compressor.Compression;
    /**
     * Compression level 0 (no compression) to 9 (best compression, default).
     */
    compressionLevel?: number;
    /**
     * File format to be used (autodetected from URL).
     */
    format?: formatter.Format;
    /**
     * Pattern for text format to be split into pattern matches (Using named groups) for event logs.
     */
    pattern?: RegExp;
    /**
     * Flag to read the CSV, XLS or XLSX always as with records (only needed with less than 3 columns).
     */
    records?: boolean;
    /**
     * Character to be used as field separator in CSV data.
     */
    delimiter?: string;
    /**
     * Character used to quote field values in CSV data.
     */
    quote?: string;
    /**
     * Use module format in storing as JavaScript, so you can load it using normal require or import.
     */
    module?: boolean;
    /**
     * Define the root element name in formatting as XML.
     */
    rootName?: string;
}

/** Definition for MultiFile loading */
interface DataSource extends MergeInput {
    /** URL to load this structure. */
    source?: string;
    /** Additional settings. */
    options?: Options;
    /** Meta information set on loading. */
    meta?: any;
}

/**
 * DataStore can be used to load, save and access different data structures.
 */
class DataStore {
    /**
     * Real content, ready to be used.
     */
    public data: any = {};
    /**
     * Options to be used for load/save and parsing of data formats.
     */
    public options: Options = {};

    private _source: DataSource[] = [];
    private _map: any = {};
    private _load: Date | undefined = undefined;
    private changed = false;

    /**
     * Initialize a new data store.
     * @param input one or multiple data sources to later be used in load/save
     */
    constructor(...input: DataSource[]) {
        if (!input) return;
        this._source = input;
    }

    /**
     * Create a new data store and load it from single source.
     * @param input one or multiple data sources
     */
    public static async load(...input: DataSource[]): Promise<DataStore> {
        const ds = new DataStore();
        return ds.load(...input).then(() => Promise.resolve(ds));
    }

    /**
     * Create a new data store and load it from single source.
     * @param source URL to load
     * @param options options to load source
     */
    public static async url(source: string, options?: Options): Promise<DataStore> {
        const ds = new DataStore();
        return ds.load({ source, options }).then(() => Promise.resolve(ds));
    }

    /**
     * Create a new data store with preset data.
     * @param source data structure to be set
     */
    public static async data(data: any, source?: string): Promise<DataStore> {
      const ds = new DataStore();
      ds.data = data;
      if (source && !source.startsWith('preset:')) source = `preset:${source}`
      ds.source =  source;
      return Promise.resolve(ds);
    }

    /**
     * Load from persistent storage and parse content.
     * @param input URL or list to read from
     * @return parsed data structure (same like ds.data)
     */
    public async load(...input: DataSource[]): Promise<any> {
      // check if already loaded
        if (!input.length && this._load) return Promise.resolve(this.data);
        if (input.length) this._source = input;
        // load array
        debug("Loading: %o", this._source);
      if (this._source.filter(e => e.source && !e.source.startsWith('preset:')).length) await async.map(this._source, this.multiload);
        // get result
        if (this._source.length === 1) this.data = data.clone(this._source[0].data);
        else if (this._source.length) {
            const res = data.clone(data.merge(...this._source));
            this.data = res.data;
            this._map = res.map;
        }
        debug("Result: %o", this.data);
        // all loaded
        this.changed = false;
        return Promise.resolve(this.data);
    }

    private async multiload(entry: DataSource, index?: number, list?: DataSource[]): Promise<void> {
        if (!entry.source) return;
        const source = parseUri(entry.source);
        const options = Object.assign({}, entry.options); // copy which may be changed in compression
        return (
            protocol
                .load(source, options)
                .then(([buffer, meta]) => {
                    entry.meta = meta;
                    return compressor.uncompress(source, buffer, options);
                })
                .then(buffer => {
                    entry.meta.raw = buffer;
                    return formatter.parse(source, buffer, options);
                })
                .then(data => {
                    entry.data = data;
                })
                // ignore errors if multiple sources are used
                .catch(e => {
                    debug("ERROR: %s", entry.source, e.message || e);
                    if (list && list.length == 1) throw e;
                })
        );
    }

    /**
     * Reload and parse already existing content again.
     * @param time number of seconds to wait till reload
     * @return flag if reload was done (only if changed)
     */
    public async reload(time = 0): Promise<boolean> {
        if (!this._source.length) throw new Error("No source defined to reload DataStore from.");
        if (!this._source.filter(e => e.source && !e.source.startsWith('preset:')).length)
            throw Error("The datastore is not associated with a path to load from.");
        if (!this.changed) return false;
        // don't reload for time seconds
        let cache = new Date();
        cache.setSeconds(cache.getSeconds() + time);
        if (this._load && cache < this._load) return false;
        // check for changes
        const list = await async.map(this._source, e => {
            if (!e.source) return Promise.resolve(undefined);
            return protocol.modified(parseUri(e.source));
        });
        const change = list.reduce((prev: any, cur: any) => {
            if (!cur) return prev;
            if (!prev) return prev;
            if (prev < cur) return cur;
            return prev;
        });
        if (this._load && change && change < this._load) return false;
        await this.load();
        return true;
    }

    /**
     * Save data structure to persistent store.
     * @param output URL to be stored to
     * @return true after storing
     */
    public async save(output?: DataSource): Promise<boolean> {
        if (output) this.source = [output];
        if (this._source.length > 1) throw new Error("Storing in multiple source list not possible, yet.");
        if (!this._source.length || !this._source[0].source) throw new Error("No source defined to save DataStore to.");
        const source = parseUri(this._source[0].source);
        const options = Object.assign({}, this._source[0].options); // copy which may be changed in compression
        return formatter
            .format(source, this.data, options)
            .then(buffer => compressor.compress(source, buffer, options))
            .then(buffer => protocol.save(source, buffer, options))
            .then(() => {
                this._load = new Date();
                this.changed = false;
                return true;
            });
    }

    /**
     * Only parse an olready loaded buffer into data structure.
     * @param uri URL specifying where the data is stored, also used to read the format from if not specified as option
     * @param buffer byte array buffer with the file to be parsed
     * @param options additional settings
     * @return parsed data structure (same like ds.data)
     */
    public async parse(uri: string, buffer: Buffer | string, options?: Options): Promise<any> {
        if (uri) this.source = uri;
        if (options) this._source[0].options = Object.assign(this._source[0].options, options);
        if (typeof buffer == "string") buffer = Buffer.from(buffer);
        if (!this._source.length) this._source = [{ source: "file:/dev/null" }];
        const source = parseUri(this._source[0].source || "file:/dev/null");
        this.data = await formatter.parse(source, buffer, this.options);
        return this.data;
    }

    /**
     * Only format the data structure to be stored later.
     * @param uri URL specifying where the data is stored, also used to read the format from if not specified as option
     * @param options additional settings
     * @return byte array buffer with the file to be stored
     */
    public async format(uri: string, options?: Options): Promise<Buffer> {
        let c_uri = parseUri((this._source.length && this._source[0].source) || "file:/dev/null");
        if (uri) c_uri = parseUri(uri);
        let c_options = this.options;
        if (options) c_options = Object.assign({}, this.options, options);
        return await formatter.format(c_uri, this.data, c_options);
    }

    /**
     * Get the DataSource list or source URL.
     */
    get source(): DataSource[] | string | undefined {
        if (this._source.length === 1) return this._source[0].source;
        if (this._source.length > 1) return this._source;
        return undefined;
    }

    /**
     * Set the DataSource list or source URL.
     * @param data URL specifying where the data is/will be stored
     */
    set source(data: DataSource[] | string | undefined) {
        if (data) {
            delete this._load;
            if (typeof data === "string") this._source = [{ source: data }];
            else if (Array.isArray(data) && data.length) this.source = data;
            else throw new Error("Need a DataSource list or source string for DataStore.");
        } else {
            this._source = [];
        }
    }

    /**
     * Get meta information from the DataSource list or source URL.
     */
    get meta(): any {
        if (this._source.length === 1) return this._source[0].meta;
        if (this._source.length > 1) return this._source.map(e => e.meta);
        return undefined;
    }

    /**
     * Get mapping of data elements to it's sources.
     */
    get map(): any {
        return this._map;
    }

    /**
     * Check the given path and return true if this element is defined else false.
     * @param command filter command to select elements
     */
    public has(command?: string): boolean {
        const res = data.filter(this.data, command || "");
        return res ? true : false;
    }

    /**
     * Get the element at the defined path.
     * @param command filter command to select elements
     * @param fallback default value to be used if not found
     */
    public get(command?: string, fallback?: any): any {
        const res = data.filter(this.data, command || "");
        return typeof res === undefined ? fallback : res;
    }

    /**
     * Like get but return the result as new DataStore.
     * @param command filter command to select elements
     * @param fallback default value to be used if not found
     */
    public filter(command?: string, fallback?: any): DataStore {
        const res = data.filter(this.data, command || "");
        const ds = new DataStore();
        ds.data = typeof res === undefined ? fallback : res;
        return ds;
    }

    /**
     * Set the value of an element by path specification.
     * @param path full way to the selected element in the data structure
     * @param value new content to be set
     * @param doNotReplace if set to true, only set if it is currently not set
     * @return value before or undefined if nothing was set
     */
    public set(path: string | number, value: any, doNotReplace?: boolean): any {
        this.changed = true;
        if (path === "" || (Array.isArray(path) && path.length == 0)) {
            if (this.data === undefined) return;
            const old = this.data;
            this.data = value;
            return old;
        }
        let p = typeof path === "number" ? path : path.split(/\[([^\]]*)\]|\./g).filter(e => e);
        return objectPath.set(this.data, p, value, doNotReplace);
    }

    /**
     * Insert elements into array.
     * @param path full way to the selected element in the data structure
     * @param value content to be added
     * @param pos position in the array (0=first, 9999=last, -1=before last)
     */
    public insert(path: string | number, value: any, pos?: number): void {
        this.changed = true;
        let p = typeof path === "number" ? path : path.split(/\[([^\]]*)\]|\./g).filter(e => e);
        objectPath.insert(this.data, p, value, pos);
    }

    /**
     * Add element to the end of the array.
     * @param path full way to the selected element in the data structure
     * @param values content to be added
     */
    public push(path: string | number, ...values: any[]): void {
        this.changed = true;
        let p = typeof path === "number" ? path : path.split(/\[([^\]]*)\]|\./g).filter(e => e);
        values.unshift(p);
        values.unshift(this.data);
        // @ts-ignore
        objectPath.push.apply(objectPath, values);
    }

    /**
     * Keep the specified element but make it an empty array or object.
     * @param path full way to the selected element in the data structure
     * @return the value which was deleted
     */
    public empty(path: string | number): any {
        this.changed = true;
        if (path === "" || (Array.isArray(path) && path.length == 0)) {
            const old = this.data;
            this.data = {};
            return old;
        }
        let p = typeof path === "number" ? path : path.split(/\[([^\]]*)\]|\./g).filter(e => e);
        return objectPath.empty(this.data, p);
    }

    /**
     * Removes a element from the data structure, it will be undefined afterwards.
     * @param path full way to the selected element in the data structure
     * @return the value which was deleted
     */
    public delete(path: string | number): any {
        this.changed = true;
        if (path === "" || (Array.isArray(path) && path.length == 0)) {
            const old = this.data;
            delete this.data;
            return old;
        }
        let p = typeof path === "number" ? path : path.split(/\[([^\]]*)\]|\./g).filter(e => e);
        return objectPath.del(this.data, p);
    }
}

const formats = formatter.formats;
const compressions = compressor.compressions;
export { DataStore, formats, compressions, DataSource, Options };
export default DataStore;
