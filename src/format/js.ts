import { basename, extname } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import * as vm from 'vm';
// @ts-ignore
import * as serialize from 'serialize-to-js';
import * as beautify from 'js-beautify';

import * as Format from './index';

const debug = Debug('datastore:format:js');

const parse = <Format.Parser>function (parsedUri: URL, buffer: Buffer): Promise<any> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`parsing ${pathname}`);
  const content = buffer.toString().replace(/(module.exports\s*=\s*|export\s+default\s+)/, '');
  try {
    return Promise.resolve(vm.runInNewContext(`x=${content}`));
  } catch (err) {
    err.data = buffer.toString()
    return Promise.reject(err)
  };
};

const format = <Format.Formatter>function (parsedUri: URL, data: any, options: any): Promise<Buffer> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`formatting for ${pathname}`);
  let content = serialize(data);
  const module = ['.mjs', '.ts'].includes(extname(pathname).toLowerCase())
    ? 'export default '
    : options && options.module
      ? 'module.exports = '
      : '';
  content = module + content;
  // if (options && options.module) content = `module.exports = ${content}`;
  return Promise.resolve(Buffer.from(beautify(content, { indent_size: 2 })));
};

export { parse, format };
