import { basename } from 'path';
import { promisify } from 'util';
import { URL } from 'url';
import Debug from 'debug';
import * as xml from 'xml2js';

import * as Format from './index';

const parseString = promisify(xml.parseString);

const debug = Debug('datastore:format:xml');

function preformat(obj: any): any {
    if (typeof obj !== 'object') return obj;
    Object.keys(obj).forEach(key => {
        if (obj[key] instanceof Date) {
            // Date -> string
            obj[key] = obj[key].toISOString();
        } else if (typeof obj[key] == 'object' && obj[key]) {
            // go deeper
            preformat(obj[key]);
        }
    });
    return obj;
}

const parse = <Format.Parser>async function (parsedUri: URL, buffer: Buffer): Promise<any> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`parsing ${pathname}`);
    let data = await Format.cast(
        // @ts-ignore
        await parseString(buffer.toString(), {
            trim: true,
            emptyTag: null,
            explicitRoot: false,
            explicitArray: false,
            normalizeTags: true,
            preserveChildrenOrder: true
        }).catch(e => {
            e.data = buffer.toString();
            throw e;
        }), 'boolean', 'number', 'date'
    );
    // remove top level object if only containing one array
    if (Object.keys(data).length == 1) {
        const key = Object.keys(data)[0];
        if (Array.isArray(data[key])) data = data[key];
    }
    return data;
};

const format = <Format.Formatter>(
    function (parsedUri: URL, data: any, options: any): Promise<Buffer> {
        if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
        const pathname = parsedUri.hash || basename(parsedUri.pathname);
        debug(`formatting for ${pathname}`);
        // top-level array to object with array
        if (Array.isArray(data)) {
            data = { entry: data };
        }
        // format
        let rootName = 'data';
        if (options && options.rootName) rootName = options.rootName;
        const builder = new xml.Builder({ rootName, cdata: true });
        return Promise.resolve(
            // @ts-ignore
            Buffer.from(builder.buildObject(preformat(Object.assign({}, data))))
        );
    }
);

export { parse, format };
