import { URL } from 'url';
import { basename } from 'path';
import Debug from 'debug';
import * as yaml from 'js-yaml';

import * as Format from './index';

const debug = Debug('datastore:format:yaml');

const parse = <Format.Parser>function (parsedUri: URL, buffer: Buffer): Promise<any> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`parsing ${pathname}`);
  try {
    return Promise.resolve(
      yaml.load(buffer.toString(), {
        filename: parsedUri.href,
        schema: yaml.DEFAULT_SCHEMA.extend(require('js-yaml-js-types').all)
      })
    )
  } catch (err) {
    err.data = buffer.toString()
    return Promise.reject(err)
  };
};

const format = <Format.Formatter>function (parsedUri: URL, data: any): Promise<Buffer> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`formatting for ${pathname}`);
  return Promise.resolve(
    Buffer.from(
      yaml.dump(data, {
        noRefs: true,
        schema: yaml.DEFAULT_SCHEMA.extend(require('js-yaml-js-types').all)
      })
    )
  );
};

export { parse, format };
