import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import * as Msgpack from 'msgpack5';

import * as Format from './index';

const debug = Debug('datastore:format:msgpack');
const msgpack = Msgpack();

const parse = <Format.Parser>function(parsedUri: URL, buffer: Buffer): Promise<any> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`parsing ${pathname}`);
    return Promise.resolve(msgpack.decode(buffer));
};

const format = <Format.Formatter>function(parsedUri: URL, data: any): Promise<Buffer> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`formatting for ${pathname}`);
    // @ts-ignore
    return Promise.resolve(Buffer.from(msgpack.encode(data)));
};

export { parse, format };
