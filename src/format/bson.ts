import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import * as BSON from 'bson';

import * as Format from './index';

const debug = Debug('datastore:format:bson');

const parse = <Format.Parser>async function(parsedUri: URL, buffer: Buffer): Promise<any> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`parsing ${pathname}`);
    let data = await BSON.deserialize(buffer);
    // change top-level array objects back to real objects
    let num = 0;
    let isArray = true;
    Object.keys(data).forEach(k => {
        if (k !== `${num++}`) isArray = false; // no array
    });
    return isArray ? Object.keys(data).map(k => data[k]) : data;
};

const format = <Format.Formatter>function(parsedUri: URL, data: any): Promise<Buffer> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`formatting for ${pathname}`);
    return Promise.resolve(Buffer.from(BSON.serialize(data)));
};

export { parse, format };
