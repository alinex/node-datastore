import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import * as ini from 'ini';

import * as Format from './index';

const debug = Debug('datastore:format:ini');

function preformat(obj: any): any {
  if (typeof obj !== 'object') return obj;
  Object.keys(obj).forEach(key => {
    if (obj[key] instanceof Date) {
      // Date -> string
      obj[key] = obj[key].toISOString();
    } else if (Array.isArray(obj[key])) {
      // array of objects -> object of objects
      let hasObjects = true;
      let num = 0;
      const conv: any = {};
      obj[key].forEach((e: any) => {
        if (typeof e !== 'object') hasObjects = false;
        conv[num++] = e;
      });
      if (hasObjects) obj[key] = conv;
    } else if (typeof obj[key] == 'object' && obj[key]) {
      // go deeper
      obj[key] = preformat(Object.assign({}, obj[key]));
    }
  });
  return obj;
}

const parse = <Format.Parser>function (parsedUri: URL, buffer: Buffer): Promise<any> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`parsing ${pathname}`);
  try {
    return Promise.resolve(Format.cast(ini.decode(buffer.toString()), 'number', 'date', 'array'));
  } catch (err) {
    err.data = buffer.toString()
    return Promise.reject(err)
  };
};

const format = <Format.Formatter>function (parsedUri: URL, data: any): Promise<Buffer> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`formatting for ${pathname}`);
  return Promise.resolve(Buffer.from(ini.encode(preformat(Object.assign({}, data)))));
};

export { parse, format };
