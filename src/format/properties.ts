import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import * as flat from 'flat';
// @ts-ignore
import * as properties from 'properties';

import * as Format from './index';

const debug = Debug('datastore:format:properties');

function preformat(obj: any): any {
  if (typeof obj !== 'object') return obj;
  Object.keys(obj).forEach(key => {
    if (obj[key] instanceof Date) {
      // Date -> string
      obj[key] = obj[key].toISOString();
    } else if (typeof obj[key] == 'object' && obj[key]) {
      // go deeper
      obj[key] = preformat(Object.assign({}, obj[key]));
    }
  });
  return obj;
}

const parse = <Format.Parser>function (parsedUri: URL, buffer: Buffer): Promise<any> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`parsing ${pathname}`);
  try {
    return Promise.resolve(
      Format.cast(
        flat.unflatten(
          properties.parse(buffer.toString(), {
            sections: true,
            namespaces: true,
            variables: true
          })
        ), 'date', 'array'
      )
    );
  } catch (err) {
    err.data = buffer.toString()
    return Promise.reject(err)
  };
};

const format = <Format.Formatter>function (parsedUri: URL, data: any): Promise<Buffer> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`formatting for ${pathname}`);
  return Promise.resolve(
    Buffer.from(
      properties.stringify(flat.flatten(preformat(Object.assign({}, data))), {
        unicode: true
      })
    )
  );
};

export { parse, format };
