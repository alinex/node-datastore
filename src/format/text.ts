import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import { inspect } from 'util';

import * as Format from './index';

const debug = Debug('datastore:format:text');

const parse = <Format.Parser>function(parsedUri: URL, buffer: Buffer, options: any): Promise<any> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`parsing ${pathname}`);
    let text: any = buffer.toString();
    if (text && options && options.pattern) {
        const data = [...text.matchAll(options.pattern)].map(e => {
            return { ...e.groups };
        });
        debug('Found %d matches using pattern matching', data.length);
        text = data;
    }
    return Promise.resolve(text);
};

const format = <Format.Formatter>function(parsedUri: URL, data: any): Promise<Buffer> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`formatting for ${pathname}`);
    return Promise.resolve(
        Buffer.from(typeof data === 'object' ? inspect(data, { depth: null }) : data)
    );
};

export { parse, format };
