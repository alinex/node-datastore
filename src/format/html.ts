import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import { Parser } from 'htmlparser2';
import { DomHandler } from 'domhandler';

import * as Format from './index';
import { inspect } from 'util';

const debug = Debug('datastore:format:html');

function dom2array(dom: any[]): any[] {
    return dom.map(e => {
        switch (e.type) {
            case 'directive':
                return { '!': e.data };
            case 'text':
                return { _: e.data };
            case 'tag':
                let content: any = [];
                if (Object.keys(e.attribs).length) content.push({ $: e.attribs });
                if (e.children) content = content.concat(dom2array(e.children));
                const data: any = {};
                data[e.name] = content;
                return data;
            default:
                console.log('missing: ' + inspect(e, { depth: 0 }));
                return e;
        }
    });
}

function array2html(data: any[]): string {
    return data
        .map(e => {
            if (e['!']) return `<${e['!']}>`;
            if (e['_']) return e['_'];
            if (e['$']) return '';
            const tag = Object.keys(e)[0];
            //            console.log(tag, e[tag]);
            const attrib = array2attrib(e[tag]);
            if (Object.keys(e[tag]).filter(s => s !== '$').length)
                return `<${tag}${attrib}>` + array2html(e[tag]) + `</${tag}>`;
            return `<${tag}${attrib}/>`;
        })
        .join('');
}

function array2attrib(data: any[]): string {
    const att = data.filter(e => Object.keys(e).filter(n => n == '$').length);
    if (!att.length) return '';
    return Object.keys(att[0]['$'])
        .map((e: any) => ` ${e}="${att[0]['$'][e]}"`)
        .join('');
}

const parse = <Format.Parser>async function (parsedUri: URL, buffer: Buffer): Promise<any> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`parsing ${pathname}`);
    let data: any;
    const handler = new DomHandler(function (error, dom) {
        if (error) {
            const e: any = new Error('Could not parse html: ' + error)
            e.data = buffer.toString()
            return Promise.reject(e);
        }
        data = dom2array(dom);
    });
    const parser = new Parser(handler);
    parser.write(buffer.toString());
    parser.end();
    return data;
};

const format = <Format.Formatter>function (parsedUri: URL, data: any, options: any): Promise<Buffer> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`formatting for ${pathname}`);
    return Promise.resolve(Buffer.from(array2html(data)));
};

export { parse, format };
