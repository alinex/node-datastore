import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import * as toml from '@iarna/toml';

import * as Format from './index';

const debug = Debug('datastore:format:toml');

function cast (obj: any): any {
  if (typeof obj !== 'object') return obj;
  // object -> array
  let num = 0;
  let isArray = true;
  Object.keys(obj).forEach(k => {
    if (k !== `${num++}`) isArray = false; // no array
  });
  if (isArray) obj = Object.keys(obj).map(k => obj[k]);
  return obj;
}

function preformat (obj: any): any {
  if (Array.isArray(obj)) {
    // array of objects -> object of objects
    let hasObjects = true;
    let num = 0;
    const conv: any = {};
    obj.forEach((e: any) => {
      if (typeof e !== 'object') hasObjects = false;
      conv[num++] = e;
    });
    if (hasObjects) obj = conv;
  }
  return obj;
}

const parse = <Format.Parser>function (parsedUri: URL, buffer: Buffer): Promise<any> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`parsing ${pathname}`);
  try {
    return Promise.resolve(cast(toml.parse(buffer.toString())));
  } catch (err) {
    err.data = buffer.toString()
    return Promise.reject(err)
  };
};

const format = <Format.Formatter>function (parsedUri: URL, data: any): Promise<Buffer> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`formatting for ${pathname}`);
  return Promise.resolve(Buffer.from(toml.stringify(preformat(Object.assign({}, data)))));
};

export { parse, format };
