import { URL } from 'url';
import { basename, extname } from 'path';
import Debug from 'debug';
import { DateTime } from "luxon";

import { ExitError } from '@alinex/core';

const debug = Debug('datastore:format');

export type Parser = (parsedUri: URL, buffer: Buffer, options?: any) => Promise<any>;
export type Formatter = (parsedUri: URL, data: any, options?: any) => Promise<Buffer>;

export type Format =
    | 'bson'
    | 'coffee'
    | 'cson'
    | 'csv'
    | 'html'
    | 'ini'
    | 'js'
    | 'json'
    | 'msgpack'
    | 'properties'
    | 'text'
    | 'binary'
    | 'toml'
    | 'xlsx'
    | 'xls'
    | 'xml'
    | 'yaml';

export interface Handler {
    parse: Parser;
    format: Formatter;
}

export const formats = [
    'bson',
    'coffee',
    'cson',
    'csv',
    'html',
    'ini',
    'js',
    'json',
    'msgpack',
    'properties',
    'text',
    'binary',
    'toml',
    'xlsx',
    'xls',
    'xml',
    'yaml'
];

const alias: { [key: string]: string } = {
    htm: 'html',
    javascript: 'js',
    mjs: 'mjs',
    ts: 'js',
    log: 'text',
    txt: 'text',
    yml: 'yaml'
};

function detect(pathname: string): string | null {
    if (!extname(pathname)) return null;
    let ext = extname(pathname)
        .replace(/^\./, '')
        .toLowerCase();
    if (alias[ext]) ext = alias[ext];
    if (formats.indexOf(ext) !== -1) return ext;
    return detect(basename(pathname, extname(pathname)));
}

const handler = function (parsedUri: URL, options: any): Promise<Handler> {
    let ext: string | null = null;
    if (options && options.format) {
        ext = options.format;
        if (ext) {
            if (alias[ext]) ext = alias[ext];
            if (formats.indexOf(ext) == -1) ext = null;
        }
    } else if (parsedUri.hash) {
        ext = detect(parsedUri.hash.substr(1));
    } else {
        if (!parsedUri.pathname) return Promise.reject(`Missing path in ${parsedUri.href}`);
        ext = detect(parsedUri.pathname);
    }
    if (!ext) {
        return Promise.reject(
            new ExitError(
                `Could not detect formatter based on file extension of ${parsedUri.pathname}, specify in options.`,
                'Possible formats are: ' + formats.join(', ')
            )
        );
    }
    return import(`./${ext}`);
};

export const parse = <Parser>async function (parsedUri: URL, buffer: Buffer, options: any): Promise<any> {
    if (debug.enabled) {
        return handler(parsedUri, options).then(mod =>
            mod.parse(parsedUri, buffer, options).then(data => {
                debug('got data:', data);
                return data;
            })
        );
    }
    return handler(parsedUri, options).then(mod => mod.parse(parsedUri, buffer, options));
};
export const format = <Formatter>async function (parsedUri: URL, data: any, options: any): Promise<Buffer> {
    if (!parsedUri.pathname) throw new Error('No pathname given in URI');
    return handler(parsedUri, options).then(mod => mod.format(parsedUri, data, options));
};

export const cast = function (obj: any, ...types: string[]): any {
    if (typeof obj !== 'object') return obj;
    Object.keys(obj).forEach(key => {
        if (typeof obj[key] === 'string') {
            const n = Number(obj[key]);
            // string -> boolean
            if (types.includes('boolean') && obj[key] == 'true') obj[key] = true;
            else if (types.includes('boolean') && obj[key] == 'false') obj[key] = false;
            else if (types.includes('number') && !isNaN(n)) {
                // string -> number
                obj[key] = n;
            } else if (types.includes('date')) {
                // string -> date
                const d = DateTime.fromISO(obj[key])
                if (d.isValid) obj[key] = d.toJSDate();
            } else if (types.includes('isodate') && obj[key].match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}/)) {
                // string -> date
                const d = DateTime.fromISO(obj[key])
                if (d.isValid) obj[key] = d.toJSDate();
            }
        } else if (typeof obj[key] == 'object' && obj[key]) {
            // go deeper
            obj[key] = cast(obj[key], ...types);
        }
    });
    // object -> array
    if (types.includes('array')) {
        let num = 0;
        let isArray = true;
        Object.keys(obj).forEach(k => {
            if (k !== `${num++}`) isArray = false; // no array
        });
        if (isArray) obj = Object.keys(obj).map(k => obj[k]);
    }
    return obj;
}

export default { parse, format, cast };
