import { basename } from "path";
import { URL } from "url";
import Debug from "debug";

import * as Format from "./index";

const debug = Debug("datastore:format:binary");

const parse = <Format.Parser>function(parsedUri: URL, buffer: Buffer, options: any): Promise<any> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`parsing ${pathname}`);
    return Promise.resolve(Uint8Array.from(buffer));
};

const format = <Format.Formatter>function(parsedUri: URL, data: any): Promise<Buffer> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`formatting for ${pathname}`);
    return Promise.resolve(Buffer.from(data));
};

export { parse, format };
