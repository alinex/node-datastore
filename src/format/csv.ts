import { basename } from "path";
import { URL } from "url";
import Debug from "debug";
import { DateTime } from "luxon";
import * as flat from "flat";
import * as csvParse from "csv-parse/lib/sync";
import * as csvStringify from "csv-stringify/lib/sync";

import * as Format from "./index";

const debug = Debug("datastore:format:csv");

function toObject(arr: any[], recordFormat: boolean): any {
  let res: any = recordFormat ? [] : {};
  arr.forEach(row => {
    if (recordFormat) {
      res.push(flat.unflatten(row));
    } else {
      res[row[0]] = row[1] === "" ? null : row[1];
    }
  });
  return recordFormat ? res : flat.unflatten(res);
}
function toArray(obj: any): any[] {
  let res: any[] = [];
  if (Array.isArray(obj)) {
    // top-level array to table
    Object.keys(obj).forEach(key => {
      res.push(flat.flatten(obj[key]));
    });
  } else {
    // format to value list
    obj = flat.flatten(obj);
    Object.keys(obj).forEach(key => {
      res.push([key, obj[key]]);
    });
  }
  return res;
}

const parse = <Format.Parser>function (parsedUri: URL, buffer: Buffer, options: any): Promise<any> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`parsing ${pathname}`);
  // use header if more than two columns in csv
  const delimiter = options && options.delimiter ? options.delimiter : ",";
  const quote = options && options.quote !== undefined ? options.quote : '"';
  let recordFormat =
    (options && options.records) ||
    csvParse(buffer.toString().replace(/\n.*/s, ""), { delimiter: delimiter })[0].length > 2;
  try {
    return Promise.resolve(
      toObject(
        csvParse(buffer.toString(), {
          columns: recordFormat,
          delimiter,
          quote,
          skip_empty_lines: true,
          cast: (value, context) => {
            if (context.quoting === false) {
              if (value === "") return null;
              if (value === "false") return false;
              if (value === "true") return true;
              if (value === "f") return false;
              if (value === "t") return true;
              // quoted string -> number
              const n = Number(value);
              if (isNaN(n)) return value;
              return n;
            } else if (context.index === 1 && value.match(/[-:]/)) {
              // string -> date
              const d = DateTime.fromISO(value)
              return d.isValid ? d.toJSDate() : value;
            } else {
              return value;
            }
          }
        }),
        recordFormat
      )
    )
  } catch (err) {
    err.data = buffer.toString()
    return Promise.reject(err)
  };
};

const format = <Format.Formatter>function (parsedUri: URL, data: any): Promise<Buffer> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`formatting for ${pathname}`);
  const list: any[] = toArray(data);
  return Promise.resolve(
    Buffer.from(
      // @ts-ignore
      csvStringify(list, {
        delimiter: ",",
        header: Array.isArray(list[0]) ? false : true,
        cast: {
          boolean: value => (value ? "true" : "false"),
          // @ts-ignore
          date: value => {
            return { value: value.toISOString(), quoted: true };
          },
          // @ts-ignore
          string: value => {
            return { value: value, quoted: true };
          }
        }
      }).replace(/\n/g, '\r\n')
    )
  );
};

export { parse, format };
