import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';

import * as CSON from 'cson-parser';
// @ts-ignore
import * as coffee from 'coffeescript';

import * as Format from './index';

const debug = Debug('datastore:format:coffee');

const parse = <Format.Parser>function (parsedUri: URL, buffer: Buffer): Promise<any> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`parsing ${pathname}`);
  let text = buffer.toString();
  try {
    text = 'module.exports =\n  ' + text.replace(/\n/g, '\n  ');
    // @ts-ignore
    let m = new module.constructor();
    m._compile(coffee.compile(text), 'inline.coffee');
    return Promise.resolve(Format.cast(m.exports, 'date'));
  } catch (error) {
    error.data = buffer.toString()
    return Promise.reject(error);
  }
};

const format = <Format.Formatter>function (parsedUri: URL, data: any): Promise<Buffer> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`formatting for ${pathname}`);
  return Promise.resolve(Buffer.from(CSON.stringify(data, null, 4)));
};

export { parse, format };
