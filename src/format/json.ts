import { basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';

import * as Format from './index';

const debug = Debug('datastore:format:json');

const parse = <Format.Parser>function (parsedUri: URL, buffer: Buffer): Promise<any> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`parsing ${pathname}`);
  try {
    return Promise.resolve(Format.cast(JSON.parse(buffer.toString()), 'isodate'));
  } catch (err) {
    err.data = buffer.toString()
    return Promise.reject(err)
  };
};

const format = <Format.Formatter>function (parsedUri: URL, data: any): Promise<Buffer> {
  if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
  const pathname = parsedUri.hash || basename(parsedUri.pathname);
  debug(`formatting for ${pathname}`);
  return Promise.resolve(Buffer.from(JSON.stringify(data, null, 4)));
};

export { parse, format };
