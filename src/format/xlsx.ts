import { basename } from "path";
import { URL } from "url";
import Debug from "debug";
import * as flat from "flat";
import * as XLSX from 'xlsx';
// https://github.com/SheetJS/sheetjs

import * as Format from "./index";

const debug = Debug("datastore:format:xlsx");

function toObject(arr: any[], recordFormat: boolean): any {
    let res: any = recordFormat ? [] : {};
    arr.forEach(row => {
        if (recordFormat) {
            res.push(flat.unflatten(row));
        } else {
            res[row[0]] = row[1] === "" ? null : row[1];
        }
    });
    return recordFormat ? res : flat.unflatten(res);
}
function toArray(obj: any): any[] {
    let res: any[] = [];
    if (Array.isArray(obj)) {
        // top-level array to table
        Object.keys(obj).forEach(key => {
            res.push(flat.flatten(obj[key]));
        });
    } else {
        // format to value list
        obj = flat.flatten(obj);
        Object.keys(obj).forEach(key => {
            res.push([key, obj[key]]);
        });
    }
    return res;
}

const parse = <Format.Parser>function (parsedUri: URL, buffer: Buffer, options: any): Promise<any> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`parsing ${pathname}`);
    const wb = XLSX.read(buffer, { type: 'buffer', cellDates: true});
    const ws = wb.Sheets[wb.SheetNames[0]];
    const work: any[] = XLSX.utils.sheet_to_json(ws)
    let colls: string[] = []
    work.forEach(row => {
      if (colls.length < Object.keys(work[0]).length) colls = Object.keys(work[0])
    })
    work.forEach(row => {
      colls.forEach(coll => {
        if (typeof row[coll] == 'undefined') row[coll] = null
      })
    })
    let recordFormat =
        (options && options.records) || Object.keys(work[0]).length > 2;
    return Promise.resolve(toObject(work, recordFormat));
};

const format = <Format.Formatter>function (parsedUri: URL, data: any): Promise<Buffer> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    const pathname = parsedUri.hash || basename(parsedUri.pathname);
    debug(`formatting for ${pathname}`);
    const list: any[] = toArray(data);
    const ws = XLSX.utils.json_to_sheet(list);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Data");
    return Promise.resolve(XLSX.write(wb, { type: 'buffer', bookType: 'xlsx' }));
};

export { parse, format };
