import { dirname, basename } from 'path';
import { URL } from 'url';
import Debug from 'debug';
import * as sftpClient from 'ssh2-sftp-client';

import * as Protocol from './index';

const debug = Debug('datastore:protocol:sftp');
const debugDetails = Debug('datastore:details');

const load = <Protocol.Reader>function(parsedUri: URL, options?: any): Promise<[Buffer, any]> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(
        `loading ${parsedUri.protocol}//${parsedUri.username}@${parsedUri.host}${parsedUri.pathname}`
    );
    const meta: any = { path: parsedUri.pathname };
    let sftp = new sftpClient();
    return <Promise<[Buffer, any]>>sftp
        .connect({
            host: parsedUri.host,
            port: parsedUri.port ? Number(parsedUri.port) : undefined,
            username: parsedUri.username,
            password: parsedUri.password,
            privateKey: options ? options.privateKey : undefined,
            passphrase: options ? options.passphrase : undefined,
            debug: debugDetails.enabled ? debugDetails : undefined
        })
        .then(() => sftp.stat(parsedUri.pathname))
        .then(stat => (meta.stats = stat))
        .then(() => sftp.get(parsedUri.pathname))
        .then(data => {
            sftp.end();
            return [data, meta];
        });
};

const save = <Protocol.Writer>(
    function(parsedUri: URL, buffer: Buffer, options?: any): Promise<void> {
        if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
        debug(
            `storing to ${parsedUri.protocol}//${parsedUri.username}@${parsedUri.host}${parsedUri.pathname}`
        );
        let sftp = new sftpClient();
        return <Promise<void>>sftp
            .connect({
                host: parsedUri.host,
                port: parsedUri.port ? Number(parsedUri.port) : undefined,
                username: parsedUri.username,
                password: parsedUri.password,
                privateKey: options ? options.privateKey : undefined,
                passphrase: options ? options.passphrase : undefined,
                debug: debugDetails.enabled ? debugDetails : undefined
            })
            .then(() => sftp.put(buffer, parsedUri.pathname));
    }
);

const modified = <Protocol.Modifier>async function(parsedUri: URL, options?: any): Promise<Date> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(
        `last modification check ${parsedUri.protocol}//${parsedUri.username}@${parsedUri.host}${parsedUri.pathname}`
    );
    let sftp = new sftpClient();
    return sftp
        .connect({
            host: parsedUri.host,
            port: parsedUri.port ? Number(parsedUri.port) : undefined,
            username: parsedUri.username,
            password: parsedUri.password,
            privateKey: options ? options.privateKey : undefined,
            passphrase: options ? options.passphrase : undefined
        })
        .then(() => sftp.list(dirname(parsedUri.pathname)))
        .then(stats => {
            const f = basename(parsedUri.pathname);
            return new Date(stats.filter(stat => stat.name == f)[0].modifyTime);
        });
};

export { load, save, modified };
