import { URL } from "url";
import { extname } from "path";
import Debug from "debug";
import * as request from "request";

import * as Protocol from "./index";

const debug = Debug("datastore:protocol:http");

const ext2mime: { [key: string]: string } = {
    bson: "application/bson",
    coffee: "text/coffeescript",
    cson: "application/cson",
    csv: "text/comma-separated-values",
    ini: "text/ini",
    js: "text/javascript",
    json: "application/json",
    msgpack: "application/octet-stream",
    properties: "application/json",
    toml: "application/toml",
    xml: "application/xml",
    yaml: "text/yaml",
    text: "text/plain"
};
const mime2ext: { [key: string]: string } = {
    "application/bson": "bson",
    "text/coffeescript": "coffee",
    "application/cson": "cson",
    "text/comma-separated-values": "csv",
    "text/ini": "ini",
    "text/javascript": "js",
    "application/json": "json",
    "application/octet-stream": "msgpack",
    "application/toml": "toml",
    "application/xml": "xml",
    "text/yaml": "yaml",
    "text/plain": "text"
};

const load = <Protocol.Reader>function(parsedUri: URL, options: any = {}): Promise<[Buffer, any]> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(`loading ${parsedUri.pathname}`);
    return new Promise((resolve, reject) => {
        const ext = options.format
            ? options.format
            : extname(parsedUri.pathname)
                  .replace(/^\./, "")
                  .toLowerCase();
        const mimetype = ext2mime[ext] ? ext2mime[ext] : "text/plain";
        const headers: any = { Accept: `${mimetype};q=0.9, */*;q=0.8` };
        if (options.httpHeader)
            options.httpHeader.forEach((line: string) => {
                const p = line.split(/\s*:\s*/, 2);
                if (p.length !== 2) throw new Error(`Incorrect http header given: ${line}`);
                headers[p[0]] = p[1];
            });
        request(
            {
                method: options.httpMethod ? options.httpMethod.toUpperCase() : "GET",
                url: parsedUri.href,
                headers,
                body: options.httpData
            },
            (error, response, body) => {
                if (error) return reject(error);
                if (!options.ignoreError && (response.statusCode < 100 || response.statusCode >= 300))
                    return reject(new Error(`Got code ${response.statusCode}: ${response.statusMessage}`));
                // set format based on content type
                if (!options.format && response.headers["content-type"]) {
                    const mime = response.headers["content-type"].replace(/[; ].*$/, "");
                    if (mime2ext[mime]) options.format = mime2ext[mime];
                    else debug(`Mimetype ${mime} is unknown, format could not be detected.`);
                }
                resolve([
                    body,
                    {
                        status: response.statusCode,
                        statusText: response.statusMessage,
                        headers: response.headers
                    }
                ]);
            }
        );
    });
};

const save = <Protocol.Writer>function(parsedUri: URL, buffer: Buffer, options?: any): Promise<void> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(`storing to ${parsedUri.pathname}`);
    return new Promise((resolve, reject) => {
        const ext =
            options && options.format
                ? options.format
                : extname(parsedUri.pathname)
                      .replace(/^\./, "")
                      .toLowerCase();
        const mimetype = ext2mime[ext] ? ext2mime[ext] : "text/plain";
        const headers: any = {
            Accept: `${mimetype};q=0.9, */*;q=0.8`,
            "content-type": mimetype
        };
        if (options.httpHeader)
            options.httpHeader.forEach((line: string) => {
                const p = line.split(/\s*:\s*/, 2);
                if (p.length !== 2) throw new Error(`Incorrect http header given: ${line}`);
                headers[p[0]] = p[1];
            });
        request(
            {
                method: "POST",
                url: parsedUri.href,
                headers: options.httpHeader,
                body: buffer
            },
            (error, response, body) => {
                console.log(error);
                console.log(response);
                if (error) return reject(error);
                if (response.statusCode < 100 || response.statusCode >= 300)
                    return reject(new Error(`Got code ${response.statusCode}: ${response.statusMessage}`));
                resolve(body);
            }
        );
    });
};

const modified = <Protocol.Modifier>async function(parsedUri: URL, options?: any): Promise<Date> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(`last modification check ${parsedUri.pathname}`);
    return new Promise((resolve, reject) => {
        request({ method: "HEAD", url: parsedUri.href }, (error, response, body) => {
            if (error) reject(error);
            const date = response.headers["last-modified"];
            resolve(date ? new Date(date) : new Date());
        });
    });
};

export { load, save, modified };
