import { URL } from 'url';
import { ExitError } from '@alinex/core';

type Reader = (parsedUri: URL, options?: any) => Promise<[Buffer, any]>;
type Writer = (parsedUri: URL, buffer: Buffer, options?: any) => Promise<void>;
type Modifier = (parsedUri: URL, options?: any) => Promise<Date>;
interface Handler {
    load: Reader;
    save: Writer;
    modified: Modifier;
}

const protocols = ['file', 'sftp', 'ftp', 'ftps', 'http', 'https', 'postgres', 'shell'];

const alias: { [key: string]: string } = {
    https: 'http',
    ftps: 'ftp',
    ssh: 'shell'
};

const handler = function(parsedUri: URL, options?: any): Promise<Handler> {
    if (!parsedUri.protocol)
        return Promise.reject(new Error(`Missing protocol in ${parsedUri.href}`));
    let prot = parsedUri.protocol.replace(/:$/, '').toLowerCase();
    if (alias[prot]) prot = alias[prot];
    if (!prot) {
        return Promise.reject(
            new ExitError(
                `Could not detect protocol, specify in url.`,
                'Possible protocols are: ' + protocols.join(', ')
            )
        );
    }
    return import(`./${prot}`);
};

const load = <Reader>async function(parsedUri: URL, options: any = {}): Promise<[Buffer, any]> {
    return handler(parsedUri).then(mod => {
      const start = Date.now()
      return mod.load(parsedUri, options).then((res: [Buffer, any]) => {
        res[1].time = Date.now() - start
        return [res[0], res[1]]
      })
    });
};
const save = <Writer>async function(parsedUri: URL, buffer: Buffer, options?: any): Promise<void> {
    return handler(parsedUri).then(mod => mod.save(parsedUri, buffer, options));
};
const modified = <Modifier>async function(parsedUri: URL, options?: any): Promise<Date> {
    return handler(parsedUri).then(mod => mod.modified(parsedUri, options));
};

export default { load, save, modified };
export { Reader, Writer, Modifier, Handler };
