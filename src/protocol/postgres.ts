import { inspect } from 'util';
import { URL } from 'url';
import Debug from 'debug';
import * as flat from 'flat';
import { Client } from 'pg';
import { DateTime } from "luxon";
import async from '@alinex/async';

import * as Protocol from './index';

let pg = require('pg');
try {
    // if pg-native is installed, use it
    require.resolve('pg-native');
    pg = pg.native;
    console.log('Using native pg!');
} catch (e) { }

const debug = Debug('datastore:protocol:psql');
const debugDetails = Debug('datastore:details');

// postgres://user:pass@example.com:5432/dbname/schema.table#WHERE ...
// postgres://user:pass@example.com:5432/dbname#SELECT * FROM ...

function cast(value: any) {
    if (value === '') return null;
    if (value === 'false') return false;
    if (value === 'true') return true;
    // string -> number
    const n = Number(value);
    if (!isNaN(n)) return n;
    // string -> date
    if (value.match(/[-:]/)) {
        const d = DateTime.fromISO(value)
        if (d.isValid) return d.toJSDate();
    }
    return value;
}

function toObject(arr: any[], recordFormat: boolean): any {
    let res: any = recordFormat ? [] : {};
    //    console.log(res);
    arr.forEach(row => {
        if (recordFormat) {
            res.push(flat.unflatten(row));
        } else {
            const val: any = Object.values(row);
            res[val[0]] = cast(val[1]);
        }
    });
    return recordFormat ? res : flat.unflatten(res);
}

const load = <Protocol.Reader>function (parsedUri: URL, options: any = {}): Promise<[Buffer, any]> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    options.format = 'json';
    debug(
        `loading ${parsedUri.protocol}//${parsedUri.username}@${parsedUri.host}${parsedUri.pathname}`
    );
    const conn = `${parsedUri.protocol}//${parsedUri.username}:${parsedUri.password}@${parsedUri.host}${parsedUri.pathname}`;
    const sql = decodeURI(parsedUri.hash.substring(1));
    //    debug(`conn  ${conn}`);
    debug(`query   ${sql}`);
    let client: Client;
    return (
        // connect to database
        async
            .retry(() => {
                client = new pg.Client({
                    connectionString: conn
                });
                return client.connect().catch((e: Error) => {
                    debug(`WARN: ${e.message}`);
                    client.end();
                    return Promise.reject(e);
                });
            })
            // query data
            .then(() => options.search && client.query(`SET search_path TO ${options.search}`))
            .then(() => client.query(sql))
            .catch((e: Error) => {
                client.end();
                console.error(e);
                return Promise.reject(e);
            })
            // process results
            .then((res: any) => {
                debugDetails(`postgres rows   ${inspect(res.rowCount)}`);
                debugDetails(`postgres result ${inspect(res.rows)}`);
                const recordFormat = options.records || res.rows.length && Object.keys(res.rows[0]).length > 2;
                return Promise.resolve(toObject(res.rows, recordFormat));
            })
            // end connection and return data
            .then((data: any) => {
                client.end();
                return Promise.resolve([Buffer.from(JSON.stringify(data)), {}]);
            })
    );
};

const save = <Protocol.Writer>(
    function (parsedUri: URL, buffer: Buffer, options?: any): Promise<void> {
        if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
        debug(
            `storing to ${parsedUri.protocol}//${parsedUri.username}@${parsedUri.host}${parsedUri.pathname}`
        );
        return Promise.reject(new Error('Save not implemented here!'));
    }
);

const modified = <Protocol.Modifier>async function (parsedUri: URL, options?: any): Promise<Date> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(
        `last modification check ${parsedUri.protocol}//${parsedUri.username}@${parsedUri.host}${parsedUri.pathname}`
    );
    return Promise.resolve(new Date()); // always use current date
};

export { load, save, modified };
