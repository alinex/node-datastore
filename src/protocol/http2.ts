import { URL } from "url";
import { extname } from "path";
import Debug from "debug";
import axios from "axios";

import * as Protocol from "./index";

const debug = Debug("datastore:protocol:http");

axios.interceptors.request.use(request => {
  debug(`${(<string>request.method).toUpperCase()} ${request.url}`);
  return request;
});
axios.interceptors.response.use(
  response => {
    debug(`Response ${response.status} ${response.statusText}`);
    return response;
  },
  error => {
    debug(error.toString());
  }
);

const ext2mime: { [key: string]: string } = {
  bson: "application/bson",
  coffee: "text/coffeescript",
  cson: "application/cson",
  csv: "text/comma-separated-values",
  ini: "text/ini",
  js: "text/javascript",
  json: "application/json",
  msgpack: "application/octet-stream",
  properties: "application/json",
  toml: "application/toml",
  xml: "application/xml",
  yaml: "text/yaml",
  text: "text/plain"
};
const mime2ext: { [key: string]: string } = {
  "application/bson": "bson",
  "text/coffeescript": "coffee",
  "application/cson": "cson",
  "text/comma-separated-values": "csv",
  "text/ini": "ini",
  "text/javascript": "js",
  "application/json": "json",
  "application/octet-stream": "msgpack",
  "application/toml": "toml",
  "application/xml": "xml",
  "text/yaml": "yaml",
  "text/plain": "text"
};

const load = <Protocol.Reader>(
  function(parsedUri: URL, options: any = {}): Promise<[Buffer, any]> {
    if (!parsedUri.pathname)
      return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(`loading ${parsedUri.pathname}`);
    const ext = options.format
      ? options.format
      : extname(parsedUri.pathname)
          .replace(/^\./, "")
          .toLowerCase();
    const mimetype = ext2mime[ext] ? ext2mime[ext] : "text/plain";
    let proxy: any = undefined;
    if (options.proxy && options.proxy.length) {
      proxy = {};
      const m = options.proxy.split(":", 2);
      proxy.host = m[0];
      proxy.port = m[1];
    }
    const headers: any = { Accept: `${mimetype};q=0.9, */*;q=0.8` };
    if (options.httpHeader)
      options.httpHeader.forEach((line: string) => {
        const p = line.split(/\s*:\s*/, 2);
        if (p.length !== 2)
          throw new Error(`Incorrect http header given: ${line}`);
        headers[p[0]] = p[1];
      });
    return axios({
      proxy,
      url: parsedUri.href,
      method: options.httpMethod ? options.httpMethod.toLowerCase() : "get",
      headers,
      data: options.httpData,
      responseType: "arraybuffer"
    })
      .catch(error => {
        // console.log(error.request.res);
        if (error.response) {
          if (options.ignoreError) return error.response;
          return Promise.reject(
            `Got code ${error.response.status}: ${error.response.statusText}`
          );
        } else if (error.request) {
          error.message = `Got no response from server: ${error.message}`;
          return Promise.reject(error);
        }
        return Promise.reject(error.message);
      })
      .then(response => {
        // set format based on content type
        if (!options.format && response.headers["content-type"]) {
          const mime = response.headers["content-type"].replace(/[; ].*$/, "");
          if (mime2ext[mime]) options.format = mime2ext[mime];
          else
            debug(`Mimetype ${mime} is unknown, format could not be detected.`);
        }
        return Promise.resolve([
          response.data,
          {
            status: response.status,
            statusText: response.statusText,
            headers: response.headers
          }
        ]);
      });
  }
);

const save = <Protocol.Writer>(
  function(parsedUri: URL, buffer: Buffer, options?: any): Promise<void> {
    if (!parsedUri.pathname)
      return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(`storing to ${parsedUri.pathname}`);
    const ext =
      options && options.format
        ? options.format
        : extname(parsedUri.pathname)
            .replace(/^\./, "")
            .toLowerCase();
    const mimetype = ext2mime[ext] ? ext2mime[ext] : "text/plain";
    return axios({
      method: "post",
      url: parsedUri.href,
      data: buffer,
      headers: {
        "content-type": mimetype
      }
    })
      .then(() => Promise.resolve())
      .catch(error => {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
          return Promise.reject(
            `Got code ${error.response.status}: ${error.response.statusText}`
          );
        } else if (error.request) {
          return Promise.reject(`Got no response from server.`);
        }
        return Promise.reject(error.message);
      });
  }
);

const modified = <Protocol.Modifier>(
  async function(parsedUri: URL, options?: any): Promise<Date> {
    if (!parsedUri.pathname)
      return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(`last modification check ${parsedUri.pathname}`);

    return axios({
      method: "head",
      url: parsedUri.href
    })
      .then(response => {
        const date = response.headers["last-modified"];
        return Promise.resolve(date ? new Date(date) : new Date());
      })
      .catch(error => {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
          return Promise.reject(
            `Got code ${error.response.status}: ${error.response.statusText}`
          );
        } else if (error.request) {
          return Promise.reject(`Got no response from server.`);
        }
        return Promise.reject(error.message);
      });
  }
);

export { load, save, modified };
