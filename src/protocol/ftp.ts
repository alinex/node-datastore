import { URL } from 'url';
import Debug from 'debug';
import * as ftp from 'basic-ftp';
import * as streamBuffers from 'stream-buffers';

import * as Protocol from './index';

const debug = Debug('datastore:protocol:ftp');
const debugDetails = Debug('datastore:details');

const load = <Protocol.Reader>(
    async function (parsedUri: URL, options?: any): Promise<[Buffer, any]> {
        if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
        debug(
            `loading ${parsedUri.protocol}//${parsedUri.username}@${parsedUri.host}${parsedUri.pathname}`
        );
        const meta: any = { path: parsedUri.pathname };
        const writer = new streamBuffers.WritableStreamBuffer();
        const client = new ftp.Client();
        if (debugDetails.enabled) {
            client.ftp.verbose = true
            client.ftp.log = debugDetails;
        }
        try {
            await client.access({
                host: parsedUri.host,
                port: parsedUri.port ? Number(parsedUri.port) : undefined,
                user: parsedUri.username,
                password: parsedUri.password,
                secure: parsedUri.protocol == 'ftps'
            });
            meta.stat = { modifyTime: await client.lastMod(parsedUri.pathname) };
            await client.download(writer, parsedUri.pathname);
            client.close();
        } catch (err) {
            client.close();
            throw err;
        }
        const result = writer.getContents();
        if (!result) throw new Error('Got no content from ftp server.');
        return [result, meta];
    }
);

const save = <Protocol.Writer>(
    async function (parsedUri: URL, buffer: Buffer, options?: any): Promise<void> {
        if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
        debug(
            `storing to ${parsedUri.protocol}//${parsedUri.username}@${parsedUri.host}${parsedUri.pathname}`
        );
        const reader = new streamBuffers.ReadableStreamBuffer();
        reader.put(buffer);
        reader.stop();
        const client = new ftp.Client();
        if (debugDetails.enabled) {
            client.ftp.verbose = true
            client.ftp.log = debugDetails;
        }
        try {
            await client.access({
                host: parsedUri.host,
                port: parsedUri.port ? Number(parsedUri.port) : undefined,
                user: parsedUri.username,
                password: parsedUri.password,
                secure: parsedUri.protocol == 'ftps'
            });
            await client.upload(reader, parsedUri.pathname);
            client.close();
        } catch (err) {
            client.close();
            throw err;
        }
    }
);

const modified = <Protocol.Modifier>async function (parsedUri: URL, options?: any): Promise<Date> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(
        `last modification check ${parsedUri.protocol}//${parsedUri.username}@${parsedUri.host}${parsedUri.pathname}`
    );
    let date = new Date();
    const client = new ftp.Client();
    // client.ftp.verbose = true;
    try {
        await client.access({
            host: parsedUri.host,
            port: parsedUri.port ? Number(parsedUri.port) : undefined,
            user: parsedUri.username,
            password: parsedUri.password,
            secure: parsedUri.protocol == 'ftps'
        });
        date = await client.lastMod(parsedUri.pathname);
    } catch (err) {
        // ignore errors and use current date
    }
    client.close();
    return date;
};

export { load, save, modified };
