// import { inspect } from 'util';
import { URL } from 'url';
import Debug from 'debug';
import * as execa from 'execa';
import { Client } from 'ssh2';

import * as Protocol from './index';

const debug = Debug('datastore:protocol:shell');
const debugDetails = Debug('datastore:details');

// shell://#tail -n 10
// shell://user@host#tail -n 10

const load = <Protocol.Reader>(
    async function (parsedUri: URL, options: any = {}): Promise<[Buffer, any]> {
        if (!parsedUri.hash) return Promise.reject(`No command given in ${parsedUri.href}`);
        let cmd = decodeURIComponent(parsedUri.hash.substr(1));
        if (parsedUri.host) {
            if (parsedUri.pathname) cmd = `cd ${parsedUri.pathname} && ${cmd}`;
            return new Promise((resolve, reject) => {
                debug(
                    'loading from remote shell ssh://%s@%s:%s using %o',
                    parsedUri.username,
                    parsedUri.host,
                    parsedUri.port,
                    cmd
                );
                const conn = new Client();
                conn.on('ready', () => {
                    conn.exec(cmd, (err, stream) => {
                        if (err) {
                            conn.end();
                            return reject(err);
                        }
                        const buf: Buffer[] = [];
                        stream
                            .on('end', function () {
                                conn.end(); // close parent (and this) connection
                                return resolve([Buffer.concat(buf), {}]);
                            })
                            .on('data', (data: Buffer) => {
                                debugDetails('got data', data.toString());
                                buf.push(data);
                            });
                    });
                }).connect({
                    host: parsedUri.host,
                    port: parsedUri.port ? Number(parsedUri.port) : undefined,
                    username: parsedUri.username,
                    password: parsedUri.password,
                    privateKey: options ? options.privateKey : undefined,
                    passphrase: options ? options.passphrase : undefined,
                    debug: debugDetails.enabled ? debugDetails : undefined
                });
            });
        }
        debug('loading from shell using: %o', cmd);
        const { stdout, ...meta } = await execa.command(cmd, {
            cwd: parsedUri.host ? undefined : parsedUri.pathname || process.cwd(),
            shell: true,
            reject: !options.ignoreError
        });
        return [Buffer.from(stdout), meta];
    }
);

const save = <Protocol.Writer>(
    async function (parsedUri: URL, buffer: Buffer, options?: any): Promise<void> {
        if (!parsedUri.hash) return Promise.reject(`No command given in ${parsedUri.href}`);
        debug(
            `storing to ${parsedUri.protocol}//${parsedUri.username}@${parsedUri.host}${parsedUri.pathname}`
        );
        let cmd = decodeURI(parsedUri.hash.substr(1));
        if (parsedUri.host) {
            if (parsedUri.pathname) cmd = `cd ${parsedUri.pathname} && ${cmd}`;
            return new Promise((resolve, reject) => {
                debug(
                    'storing through remote shell ssh://%s@%s:%s using %o',
                    parsedUri.username,
                    parsedUri.host,
                    parsedUri.port,
                    cmd
                );
                const conn = new Client();
                conn.on('ready', () => {
                    conn.exec(cmd, (err, stream) => {
                        if (err) {
                            conn.end();
                            return reject(err);
                        }
                        stream.on('end', function () {
                            conn.end(); // close parent (and this) connection
                            return resolve();
                        });
                    });
                }).connect({
                    host: parsedUri.host,
                    port: parsedUri.port ? Number(parsedUri.port) : undefined,
                    username: parsedUri.username,
                    password: parsedUri.password,
                    privateKey: options ? options.privateKey : undefined,
                    passphrase: options ? options.passphrase : undefined,
                    debug: debugDetails.enabled ? debugDetails : undefined
                });
            });
        }
        debug(`storing through shell using: ${cmd}`);
        await execa.command(cmd, {
            input: buffer,
            cwd: parsedUri.host ? undefined : parsedUri.pathname || process.cwd(),
            shell: true
        });
    }
);

const modified = <Protocol.Modifier>async function (parsedUri: URL, options?: any): Promise<Date> {
    if (!parsedUri.hash) return Promise.reject(`No command given in ${parsedUri.href}`);
    debug(
        `last modification check ${parsedUri.protocol}//${parsedUri.username}@${parsedUri.host}${parsedUri.pathname}`
    );
    return Promise.resolve(new Date()); // always use current date
};

export { load, save, modified };
