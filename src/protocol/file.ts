import { URL } from 'url';
import { promises, readSync } from 'fs';
import Debug from 'debug';

import * as Protocol from './index';

const debug = Debug('datastore:protocol:file');

const load = <Protocol.Reader>async function(parsedUri: URL, options = {}): Promise<[Buffer, any]> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(`loading ${parsedUri.pathname}`);
    const meta: any = { path: parsedUri.pathname };
    promises
        .stat(parsedUri.pathname)
        .then(stat => {
            meta.stats = stat;
        })
        .catch(e => undefined); // ignore error
    if (options.tail) {
        let lines = 0;
        const bufferSize = 4096;
        let soFar = Buffer.from('');
        let data = Buffer.from('');
        const fd = await promises.open(parsedUri.pathname, 'r');
        let position = meta.stats.size;
        while (position && lines < options.tail) {
            let buffer = Buffer.alloc(bufferSize);
            var length = Math.min(bufferSize, position);
            position = position - length;
            //await promises.read(fd, buffer, 0, length, position);
            readSync(fd.fd, buffer, 0, length, position);
            soFar = Buffer.concat([buffer, soFar]);
            let pos = soFar.byteLength;
            do {
                pos = soFar.lastIndexOf('\n', pos - 1);
                lines++;
            } while (pos !== -1 && lines < options.tail);
            data = Buffer.concat([soFar.slice(pos), data]);
            soFar = soFar.slice(0, pos);
        }
        fd.close();
        return Promise.resolve([data.slice(1), meta]);
    }
    return promises.readFile(parsedUri.pathname).then(data => [data, meta]);
};

const save = <Protocol.Writer>function(parsedUri: URL, buffer: Buffer, options?: any): Promise<void> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(`storing to ${parsedUri.pathname}`);
    return promises.writeFile(parsedUri.pathname, buffer);
};

const modified = <Protocol.Modifier>async function(parsedUri: URL, options?: any): Promise<Date> {
    if (!parsedUri.pathname) return Promise.reject(`No pathname given in ${parsedUri.href}`);
    debug(`last modification check ${parsedUri.pathname}`);
    const fileinfo = await promises.stat(parsedUri.pathname);
    return Promise.resolve(fileinfo.mtime);
};

export { load, save, modified };
