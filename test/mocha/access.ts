import * as chai from 'chai';

const expect = chai.expect;

import DataStore from '../../src/index';
const example = `file://${__dirname}/../data/example.json`;

describe('access', () => {
    describe('read', () => {
        let ds = new DataStore();
        before(async () => await ds.load({ source: example }));

        it('should be loaded with example', async () => {
            expect(ds.data.name).to.equal('example');
        });

        describe('has', () => {
            it('should exist', async () => {
                expect(ds.has('person.name')).to.be.true;
            });
            it('should not exist', async () => {
                expect(ds.has('person.age')).to.be.false;
            });
            it('should access root', async () => {
                expect(ds.has('')).to.be.true;
            });
        });

        describe('get', () => {
            it('should get path', async () => {
                expect(ds.get()).to.equal(ds.data);
            });
            it('should get path', async () => {
                expect(ds.get('name')).to.equal('example');
            });
            it('should get deep path', async () => {
                expect(ds.get('complex.0.name')).to.equal('Egon');
            });
            it('should get root', async () => {
                expect(ds.get('')).to.deep.equal(ds.data);
            });
        });

        describe('filter', () => {
            it('should get path', async () => {
                const part = ds.filter();
                expect(part).to.be.instanceof(DataStore);
                expect(part.data).to.deep.equal(ds.data);
            });
            it('should get deep path', async () => {
                const part = ds.filter('complex.0');
                expect(part.get('name')).to.equal('Egon');
            });
        });
    });

    describe('change', () => {
        let ds = new DataStore();
        let ds2 = new DataStore();
        before(async () => {
            await ds.load({ source: example });
            await ds2.load({ source: example });
        });

        describe('set', () => {
            it('should change element', async () => {
                expect(ds.data.name).to.equal('example');
                expect(ds.set('name', 'changed')).to.equal('example');
                expect(ds.data.name).to.equal('changed');
            });
            it('should add element', async () => {
                expect(ds.data.person.sex).to.not.exist;
                expect(ds.set('person.sex', 'w')).to.equal(undefined);
                expect(ds.data.person.sex).to.equal('w');
            });
            it('should add bracket element', async () => {
                ds.set('[person][sex]', 'm');
                expect(ds.data.person.sex).to.equal('m');
            });
            it('should add deeper element', async () => {
                expect(ds.set('person.address.street', 'Allee 5')).to.equal(undefined);
                expect(ds.data.person.address).to.exist;
                expect(ds.data.person.address.street).to.equal('Allee 5');
            });
            it('should not overwrite', async () => {
                expect(ds.set('person.name', 'unknown', true)).to.equal('Alexander Schilling');
                expect(ds.data.person.name).to.not.equal('unknown');
            });
            it('should set root', async () => {
                expect(ds2.set('', 'football')).to.has.keys(
                    'name',
                    'null',
                    'boolean',
                    'string',
                    'complex',
                    'list',
                    'date',
                    'number',
                    'person'
                );
                expect(ds2.data).to.equal('football');
            });
        });
        describe('insert', () => {
            it('should add element at the start', async () => {
                ds.insert('list', 0);
                expect(ds.data.list).to.deep.equal([0, 1, 2, 3]);
            });
            it('should add element at the end', async () => {
                ds.insert('list', 4, 4);
                expect(ds.data.list).to.deep.equal([0, 1, 2, 3, 4]);
            });
            it('should add element in the middle', async () => {
                ds.insert('list', 'x', 2);
                expect(ds.data.list).to.deep.equal([0, 1, 'x', 2, 3, 4]);
            });
            it('should add element with negative position', async () => {
                ds.insert('list', 'y', -1);
                expect(ds.data.list).to.deep.equal([0, 1, 'x', 2, 3, 'y', 4]);
            });
            it('should add element at end with too large number', async () => {
                ds.insert('list', 9, 99);
                expect(ds.data.list).to.deep.equal([0, 1, 'x', 2, 3, 'y', 4, 9]);
            });
            it('should add element on nonexistend array', async () => {
                ds.insert('list2', 9, 99);
                expect(ds.data.list2).to.deep.equal([9]);
            });
        });
        describe('push', () => {
            it('should add element at the end', async () => {
                ds.push('list', 'a');
                expect(ds.data.list).to.deep.equal([0, 1, 'x', 2, 3, 'y', 4, 9, 'a']);
            });
            it('should add multiple', async () => {
                ds.push('list', 'b', 'c', 'd');
                expect(ds.data.list).to.deep.equal([
                    0,
                    1,
                    'x',
                    2,
                    3,
                    'y',
                    4,
                    9,
                    'a',
                    'b',
                    'c',
                    'd'
                ]);
            });
        });
        describe('delete', () => {
            it('should should remove element', async () => {
                expect(ds.delete('person.job')).to.deep.equal({
                    name: 'Alexander Schilling',
                    sex: 'm',
                    address: { street: 'Allee 5' }
                });
                expect(ds.data.person.job).to.not.exist;
            });
            it('should should remove array element', async () => {
                expect(ds.delete('list.2')).to.deep.equal([
                    0,
                    1,
                    2,
                    3,
                    'y',
                    4,
                    9,
                    'a',
                    'b',
                    'c',
                    'd'
                ]);
                expect(ds.data.list).to.deep.equal([0, 1, 2, 3, 'y', 4, 9, 'a', 'b', 'c', 'd']);
            });
            it('should should remove with negative index', async () => {
                ds.delete('list.-2');
                expect(ds.data.list).to.deep.equal([0, 1, 2, 3, 'y', 4, 9, 'a', 'b', 'd']);
            });
            it('should delete root', async () => {
                expect(ds2.delete('')).to.be.equal('football');
                expect(ds2.data).to.be.undefined;
            });
        });
        describe('empty', () => {
            it('should clear boolean', async () => {
                expect(ds.empty('boolean')).to.equal(true);
                expect(ds.data.boolean).to.equal(false);
            });
            it('should clear number', async () => {
                expect(ds.empty('number')).to.equal(5.6);
                expect(ds.data.number).to.equal(0);
            });
            it('should clear string', async () => {
                expect(ds.empty('name')).to.equal('changed');
                expect(ds.data.name).to.equal('');
            });
            it('should clear array', async () => {
                ds.empty('list');
                expect(ds.data.list).to.deep.equal([]);
            });
            it('should clear object', async () => {
                ds.empty('person');
                expect(ds.data.person).to.deep.equal({});
            });
            it('should clear root', async () => {
                ds2.data = 'football';
                ds2.empty('');
                expect(ds2.data).to.deep.equal({});
            });
        });
    });
});
