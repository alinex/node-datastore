import * as chai from "chai";

const expect = chai.expect;

import DataStore from "../../src/index";
const example = `file://${__dirname}/../data/example.json`;
const example1 = `http://gitlab.com/alinex/node-datastore/raw/master/test/data/example.json`;

describe("class access", function() {
    let ds: DataStore;
    it("should create empty datastore", () => {
        ds = new DataStore();
        expect(ds.data).deep.equal({});
    });
    it("should load datastore", async () => {
        await ds.load({ source: example });
        expect(ds.data.name).to.equal("example");
    });
    it("should load datastore (predefined path)", async () => {
        ds = new DataStore({ source: example });
        expect(ds.data).deep.equal({});
        await ds.load();
        expect(ds.data.name).to.equal("example");
    });
    it("should get empty string as root element", async () => {
        ds = new DataStore();
        ds.data = "";
        expect(ds.data).to.equal("");
        expect(ds.get()).to.equal("");
    });
    it("should load multiple datastores", async () => {
        ds = new DataStore({ source: example }, { data: { add: "xxx" } });
        await ds.load();
        expect(ds.data.add).to.equal("xxx");
        expect(ds.data.name).to.equal("example");
    });
    it("should load with not existing files", async () => {
        ds = new DataStore({ source: example }, { data: { add: "xxx" } });
        await ds.load({ source: "/not-existing-file" }, { source: example });
        expect(ds.data.name).to.equal("example");
    });
    it("should init and load datastore in one step", async () => {
        ds = await DataStore.load({ source: example });
        expect(ds.data.name).to.equal("example");
    });
    it("should init and load url as datastore in one step", async () => {
        ds = await DataStore.url(example);
        expect(ds.data.name).to.equal("example");
    });
    it("should init preset datastore", async () => {
        ds = await DataStore.data([1, 2, 3]);
        expect(ds.data).to.deep.equal([1, 2, 3]);
    });
    it("should get meta data on load", async function() {
        this.timeout(5000);
        // format needs to be given because server sends as text/plain
        ds = await DataStore.url(example1, {format:'json'});
        expect(ds.data.name).to.equal("example");
    });
});
