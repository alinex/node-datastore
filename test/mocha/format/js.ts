import { expect } from 'chai';

import protocol from '../../../src/protocol';
import * as Format from '../../../src/format';

import * as url from 'url';

const type = 'js';
const typeModule = `../../../src/format/${type}`;
const base: any = require('../../data/base.js');
const list: any = require('../../data/list.js');
const file = `file://${__dirname}/../../data/example.js`;
const fileMJS = `file://${__dirname}/../../data/example.mjs`;
const fileTS = `file://${__dirname}/../../data/example.ts`;

describe('formatter', () => {
    describe(type, () => {
        let format: Format.Handler;
        let parsed = new url.URL(file);
        let parsedMJS = new url.URL(fileMJS);
        let parsedTS = new url.URL(fileTS);
        let data: any;
        it('should load async', async () => {
            format = <Format.Handler>await import(typeModule);
            expect(format).is.not.null;
            expect(format.parse).to.be.a('function');
        });
        it('should parse example', async () => {
            const [buffer] = await protocol.load(parsed);
            data = await format.parse(parsed, buffer);
            expect(data).to.not.be.null;
            expect(data.name).to.equal('example');
        });
        it('should format example', async () => {
            const reformat = await format.format(parsed, data);
            expect(reformat.toString()).to.contain('name: "example"');
        });
        it('should make a roundtrip with object', async () => {
            const reformat = await format.format(parsed, base);
            const data = await format.parse(parsed, reformat);
            expect(data).to.deep.equal(base);
        });
        it('should make a roundtrip with object (in module format)', async () => {
            const reformat = await format.format(parsed, base, { module: true });
            const data = await format.parse(parsed, reformat);
            expect(data).to.deep.equal(base);
        });
        it('should make a roundtrip with object (.mjs format)', async () => {
            const reformat = await format.format(parsedMJS, base);
            const data = await format.parse(parsedMJS, reformat);
            expect(data).to.deep.equal(base);
        });
        it('should make a roundtrip with object (.ts format)', async () => {
            const reformat = await format.format(parsedTS, base);
            const data = await format.parse(parsedTS, reformat);
            expect(data).to.deep.equal(base);
        });
        it('should make a roundtrip with list', async () => {
            const reformat = await format.format(parsed, list);
            const data = await format.parse(parsed, reformat);
            expect(data).to.deep.equal(list);
        });
    });
});
