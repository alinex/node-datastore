import { expect } from "chai";

import protocol from "../../../src/protocol";
import * as Format from "../../../src/format";
import * as url from "url";

const type = "xls";
const typeModule = `../../../src/format/${type}`;
const base: any = require("../../data/base.js");
const list: any = require("../../data/list.js");
const file = `file://${__dirname}/../../data/example.xls`;

describe("formatter", () => {
    describe(type, () => {
        let format: Format.Handler;
        let parsed = new url.URL(file);
        it("load formatter", async () => {
            format = await import(typeModule);
            expect(format).to.not.be.null;
            expect(format.parse).to.be.a("function");
        });
        it("should create example", async () => {
            const filedata = await format.format(parsed, base);
            await protocol.save(parsed, filedata);
        });
        it("should make a roundtrip with object", async () => {
            const reformat = await format.format(parsed, base);
            const data = await format.parse(parsed, reformat);
            expect(data).to.deep.equal(base);
        });
        it("should make a roundtrip with list", async () => {
            const reformat = await format.format(parsed, list);
            const data = await format.parse(parsed, reformat);
            expect(data).to.deep.equal(list);
        });
    });
});
