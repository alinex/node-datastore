import { expect } from "chai";

import protocol from "../../../src/protocol";
import * as Format from "../../../src/format";
import * as url from "url";

const type = "csv";
const typeModule = `../../../src/format/${type}`;
const base: any = require("../../data/base.js");
const list: any = require("../../data/list.js");
const file = `file://${__dirname}/../../data/example.csv`;
const tab = `file://${__dirname}/../../data/example.tab.csv`;
const custom = `file://${__dirname}/../../data/example.custom.csv`;

describe("formatter", () => {
    describe(type, () => {
        let format: Format.Handler;
        let parsed = new url.URL(file);
        it("load formatter", async () => {
            format = await import(typeModule);
            expect(format).to.not.be.null;
            expect(format.parse).to.be.a("function");
        });
        it("should create example", async () => {
            format = await import(typeModule);
            const filedata = await format.format(parsed, base);
            await protocol.save(parsed, filedata);
        });
        it("should make a roundtrip with object", async () => {
            const reformat = await format.format(parsed, base);
            const data = await format.parse(parsed, reformat);
            expect(data).to.deep.equal(base);
        });
        it("should make a roundtrip with list", async () => {
            const reformat = await format.format(parsed, list);
            const data = await format.parse(parsed, reformat);
            expect(data).to.deep.equal(list);
        });
        it("should work with tab list", async () => {
            let parsed = new url.URL(tab);
            const [buffer] = await protocol.load(parsed);
            const data = await format.parse(parsed, buffer, { delimiter: "\t", quote: "" });
            expect(data).to.deep.equal([
                { name: "node-1", ip: "10.18.0.10", priority: 1 },
                { name: "node-2", ip: "192.168.1.10", priority: 2 }
            ]);
        });
        it("should work with custom list", async () => {
            let parsed = new url.URL(custom);
            const [buffer] = await protocol.load(parsed);
            const data = await format.parse(parsed, buffer, { delimiter: ": ", quote: "" });
            expect(data['with spaces']).to.equal('possible');
        });
    });
});
