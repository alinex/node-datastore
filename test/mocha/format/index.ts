import * as chai from 'chai';
const expect = chai.expect;

import protocol from '../../../src/protocol';
import format from '../../../src/format';
import * as url from 'url';

const example = `file://${__dirname}/../../data/example.json`;

describe(`formatter`, () => {
    describe(`loader (json)`, () => {
        it('should parse example', async () => {
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
            const data = await format.parse(parsed, buffer);
            expect(data).to.not.be.null;
            expect(data.name).to.equal('example');
        });
        it('should format example', async () => {
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed);
            await protocol.save(parsed, buffer);
            const data = await format.parse(parsed, buffer);
            const reformat = await format.format(parsed, data);
            expect(reformat.toString()).to.contain('"name": "example"');
        });
    });
});
