import { expect } from 'chai';
import { inspect } from 'util';

import protocol from '../../../src/protocol';
import * as Format from '../../../src/format';

import * as url from 'url';

const type = 'text';
const typeModule = `../../../src/format/${type}`;
const base: any = require('../../data/base.js');
const file = `file://${__dirname}/../../data/example.txt`;
const log = `file://${__dirname}/../../data/example.log`;

describe('formatter', () => {
    describe(type, () => {
        let format: Format.Handler;
        let parsed = new url.URL(file);
        let data: any;
        it('should load async', async () => {
            format = <Format.Handler>await import(typeModule);
            expect(format).is.not.null;
            expect(format.parse).to.be.a('function');
        });
        it('should parse example', async () => {
            const [buffer] = await protocol.load(parsed);
            data = await format.parse(parsed, buffer);
            expect(data).to.not.be.null;
            expect(data).to.contain("name: 'example',");
        });
        it('should format example', async () => {
            const reformat = await format.format(parsed, data);
            expect(reformat.toString()).to.contain("name: 'example',");
        });
        it('should make a roundtrip with object', async () => {
            const reformat = await format.format(parsed, base);
            const data = await format.parse(parsed, reformat);
            expect(data).to.deep.equal(inspect(base));
        });
        it('should parse log', async () => {
            const parsed = new url.URL(log);
            const [buffer] = await protocol.load(parsed);
            data = await format.parse(parsed, buffer, {
                pattern: /(?<date>\d+-\d+-\d+ \d+:\d+:\d+,\d+)\s+.*?\[(?<level>[A-Z+]+)\s*\] - (\[(?<code>.*?)\] )?(?<message>.*)/g
            });
            expect(data).to.not.be.null;
            expect(data[0].level).to.be.equal('WARN');
        });
    });
});
