import { expect } from 'chai';

import protocol from '../../../src/protocol';
import * as Format from '../../../src/format';

import * as url from 'url';

const type = 'yaml';
const typeModule = `../../../src/format/${type}`;
const base: any = require('../../data/base.js');
const list: any = require('../../data/list.js');
const file = `file://${__dirname}/../../data/example.yml`;

describe('formatter', () => {
    describe(type, () => {
        let format: Format.Handler;
        let parsed = new url.URL(file);
        let data: any;
        it('should load async', async () => {
            format = <Format.Handler>await import(typeModule);
            expect(format).is.not.null;
            expect(format.parse).to.be.a('function');
        });
        it('should parse example', async () => {
            const [buffer] = await protocol.load(parsed);
            data = await format.parse(parsed, buffer);
            expect(data).to.not.be.null;
            expect(data.name).to.equal('example');
        });
        it('should format example', async () => {
            const reformat = await format.format(parsed, data);
            expect(reformat.toString()).to.contain('name: example');
        });
        it('should make a roundtrip with object', async () => {
            const reformat = await format.format(parsed, base);
            const data = await format.parse(parsed, reformat);
            expect(data).to.deep.equal(base);
        });
        it('should make a roundtrip with list', async () => {
            const reformat = await format.format(parsed, list);
            const data = await format.parse(parsed, reformat);
            expect(data).to.deep.equal(list);
        });
    });
});
