import { expect } from 'chai';

import protocol from '../../../src/protocol';
import * as Format from '../../../src/format';

import * as url from 'url';

const type = 'html';
const typeModule = `../../../src/format/${type}`;
//const base: any = require("../../data/base.js");
//const list: any = require("../../data/list.js");
const file = `file://${__dirname}/../../data/example.html`;

describe('formatter', () => {
    describe(type, () => {
        let format: Format.Handler;
        let parsed = new url.URL(file);
        let data: any;
        it('should load async', async () => {
            format = <Format.Handler>await import(typeModule);
            expect(format).is.not.null;
            expect(format.parse).to.be.a('function');
        });
        it('should parse example', async () => {
            const [buffer] = await protocol.load(parsed);
            data = await format.parse(parsed, buffer);
            expect(data).to.not.be.null;
            expect(data[2].html[1].body[1].h1[0]._).to.equal('My First Heading');
        });
        it('should format example', async () => {
            const reformat = await format.format(parsed, data);
            expect(reformat.toString()).to.contain(
                '<!DOCTYPE html>\n<html>\n    <body>\n        <h1>My First Heading</h1>\n        <p class="line">My first paragraph.</p>\n    </body>\n</html>\n'
            );
        });
        it('should make a roundtrip with object', async () => {
            const reformat = await format.format(parsed, data);
            const ndata = await format.parse(parsed, reformat);
            expect(ndata).to.deep.equal(data);
        });
    });
});
