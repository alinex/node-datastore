import * as chai from 'chai';
const expect = chai.expect;

import protocol from '../../../src/protocol';
import compressor from '../../../src/compression';
import format from '../../../src/format';
import * as url from 'url';

const example = `file://${__dirname}/../../data/example.js.gz`;

describe(`compressor`, () => {
    describe(`compressor (js)`, () => {
        it('should uncompress and parse example', async () => {
            let parsed = new url.URL(example);
            let [buffer] = await protocol.load(parsed);
            buffer = await compressor.uncompress(parsed, buffer);
            expect(buffer.toString()).to.contain('example');
            const data = await format.parse(parsed, buffer);
            expect(data).to.not.be.null;
            expect(data.name).to.equal('example');
        });
        it('should compress example', async () => {
            let parsed = new url.URL(example);
            let [orig] = await protocol.load(parsed);
            orig = await compressor.uncompress(parsed, orig);
            const data = await format.parse(parsed, orig);
            let reformat = await format.format(parsed, data);
            reformat = await compressor.compress(parsed, reformat);
            reformat = await compressor.uncompress(parsed, reformat);
            expect(reformat).to.be.deep.equal(orig);
        });
    });
});
