import { expect } from 'chai';
import * as url from 'url';

import protocol from '../../../src/protocol';
import format from '../../../src/format';
import * as Compression from '../../../src/compression';
import DataStore from '../../../src/index';

const type = 'tlz';
const typeModule = `../../../src/compression/${type}`;
const file = `file://${__dirname}/../../data/example.tar.lzma#base.js`;
const fileShort = `file://${__dirname}/../../data/example.tar.lzma`;
const base: any = require('../../data/base.js');
const temp = `file:///tmp/datastore-test-base.tlz#base.js`;
const temp2 = `file:///tmp/datastore-test-base.tar.lzma#base.js`;

describe('compressor', () => {
    let compressor: Compression.Handler;
    let parsed = new url.URL(file);
    let parsedShort = new url.URL(fileShort);
    describe(type, () => {
        it('load compressor', async () => {
            compressor = await import(typeModule);
            expect(compressor).to.not.be.null;
            expect(compressor.compress).to.be.a('function');
        });
        it('should load compressed data', async () => {
            let [buffer] = await protocol.load(parsed);
            buffer = await compressor.uncompress(parsed, buffer);
            const data = await format.parse(parsed, buffer);
            expect(data.name).to.be.equal('example');
        });
        it('should load compressed data without hash', async () => {
            let [buffer] = await protocol.load(parsedShort);
            buffer = await compressor.uncompress(parsedShort, buffer);
            const data = await format.parse(parsedShort, buffer);
            expect(data.name).to.be.equal('example');
        });
        it('should make a roundtrip', async () => {
            let buffer = await format.format(parsed, base);
            buffer = await compressor.compress(parsed, buffer);
            buffer = await compressor.uncompress(parsed, buffer);
            const data = await format.parse(parsed, buffer);
            expect(data).to.deep.equal(base);
        });
        it('should work integrated in lib', async () => {
            const ds = new DataStore({ source: temp });
            ds.data = base;
            await ds.save();
            ds.data = {};
            await ds.load({ source: temp });
            expect(ds.data).to.deep.equal(base);
        });
        it('should work integrated in lib (long format)', async () => {
            const ds = new DataStore({ source: temp2 });
            ds.data = base;
            await ds.save();
            ds.data = {};
            await ds.load({ source: temp });
            expect(ds.data).to.deep.equal(base);
        });
    });
});
