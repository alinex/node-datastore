import { expect } from 'chai';

import * as Protocol from '../../../src/protocol';
import * as url from 'url';

// SETUP:
// sudo apt-get install openssh-server
// and use PASSWORD=<local user password>

const type = 'sftp';
const typeModule = `../../../src/protocol/${type}`;
//const example = 'file://test/data/example.json'
const user = require('os').userInfo().username;
const example = `sftp://${user}:${process.env.PASSWORD}@localhost/home/alex/code/node-datastore/test/data/example.json`;

describe('protocol', () => {
    describe(type, function() {
        this.timeout(5000);
        it('should load async', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            expect(protocol).is.not.null;
            expect(protocol.load).to.be.a('function');
        });
        it('should get modification date', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const date = await protocol.modified(parsed);
            expect(date).to.be.instanceOf(Date);
        });
        it('should load example', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
        });
        it('should save example', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed);
            await protocol.save(parsed, buffer);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
        });
    });
});
