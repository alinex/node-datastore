import { expect } from 'chai';

import * as Protocol from '../../../src/protocol';
import * as url from 'url';

// SETUP:
// sudo apt install vsftpd
// sudo vi /etc/vsftpd.conf  // set write_enable=YES
// sudo systemctl restart vsftpd.service
// ftp 127.0.0.1

const type = 'ftp';
const typeModule = `../../../src/protocol/${type}`;
//const example = 'file://test/data/example.json'
const user = require('os').userInfo().username;
const example = `ftp://${user}:${process.env.PASSWORD}@localhost/home/alex/code/node-datastore/test/data/example.json`;

describe('protocol', () => {
    describe(type, function () {
        this.timeout(5000);
        it('should load async', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            expect(protocol).is.not.null;
            expect(protocol.load).to.be.a('function');
        });
        it('should get modification date', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const date = await protocol.modified(parsed);
            expect(date).to.be.instanceOf(Date);
        });
        it('should load example', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
        });
        it('should save example', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed);
            await protocol.save(parsed, buffer);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
        });
    });
});
