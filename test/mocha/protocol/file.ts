import { expect } from 'chai';

import * as Protocol from '../../../src/protocol';
import * as url from 'url';

const type = 'file';
const typeModule = `../../../src/protocol/${type}`;
//const example = 'file://test/data/example.json'
const example = `file://${__dirname}/../../data/example.json`;
const log = `file://${__dirname}/../../data/example.log`;

describe('protocol', () => {
    describe(type, () => {
        it('should load async', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            expect(protocol).is.not.null;
            expect(protocol.load).to.be.a('function');
        });
        it('should get modification date', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const date = await protocol.modified(parsed);
            expect(date).to.be.instanceOf(Date);
        });
        it('should load example', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
        });
        it('should save example', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed);
            await protocol.save(parsed, buffer);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
        });
        it('should load with tail', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(log);
            const [buffer] = await protocol.load(parsed, { tail: 3 });
            expect(buffer.toString().split('\n').length).to.equal(3);
        });
    });
});
