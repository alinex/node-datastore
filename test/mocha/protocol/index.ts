import * as chai from 'chai';
const expect = chai.expect;

import protocol from '../../../src/protocol';
import * as url from 'url';

const example = `file://${__dirname}/../../data/example.json`;

describe(`protocol`, () => {
    describe(`loader (file)`, () => {
        it('should get modification date', async () => {
            let parsed = new url.URL(example);
            const date = await protocol.modified(parsed);
            expect(date).to.be.instanceOf(Date);
        });
        it('should load example', async () => {
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
        });
        it('should save example', async () => {
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed);
            await protocol.save(parsed, buffer);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
        });
    });
});
