import { expect } from "chai";

import * as Protocol from "../../../src/protocol";
import * as url from "url";

const type = "http";
const typeModule = `../../../src/protocol/${type}`;
const example1 = `http://gitlab.com/alinex/node-datastore/raw/master/test/data/example.json`;
const example2 = `https://gitlab.com/alinex/node-datastore/raw/master/test/data/example.json`;

describe("protocol", () => {
    describe(type, function () {
        this.timeout(10000);
        it("should load async", async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            expect(protocol).is.not.null;
            expect(protocol.load).to.be.a("function");
        });
        it("should get modification date", async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example1);
            const date = await protocol.modified(parsed);
            expect(date).to.be.instanceOf(Date);
        });
        it("should get modification date (https)", async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example2);
            const date = await protocol.modified(parsed);
            expect(date).to.be.instanceOf(Date);
        });
        it("should load example", async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example1);
            const [buffer, meta] = await protocol.load(parsed);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain("example");
            expect(meta.status).to.be.equal(200);
        });
        it("should load example (https)", async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example2);
            const [buffer] = await protocol.load(parsed);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain("example");
        });
        it.skip("should load using proxy", async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example1);
            const [buffer] = await protocol.load(parsed, { proxy: "http://35.231.145.151" });
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain("example");
        });
        it.skip("should save example", async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example1);
            const [buffer] = await protocol.load(parsed);
            await protocol.save(parsed, buffer);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain("example");
        });
        it.skip("should save example (https)", async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example2);
            const [buffer] = await protocol.load(parsed);
            await protocol.save(parsed, buffer);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain("example");
        });
    });
});
