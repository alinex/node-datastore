import { expect } from 'chai';

import * as Protocol from '../../../src/protocol';
import * as url from 'url';

// SETUP:
// sudo apt install postgresql
// sudo -u postgres initdb --locale $LANG -E UTF8 -D '/var/lib/postgres/data/'
// sudo -u postgres createuser $USER
// sudo -u postgres createdb $USER
// sudo -u postgres psql -c "alter user $USER with encrypted password '<password>';"
// sudo -u postgres psql -c "grant all privileges on database $USER to $USER;"
// psql < test/data/example.sql

const type = 'postgres';
const typeModule = `../../../src/protocol/${type}`;
//const example = 'file://test/data/example.json'
const user = require('os').userInfo().username;
const example = `postgres://${user}:${process.env.PASSWORD}@localhost:5432/${user}#SELECT * FROM example`;
const records = `postgres://${user}:${process.env.PASSWORD}@localhost:5432/${user}#SELECT * FROM records`;

describe('protocol', () => {
    describe(type, function () {
        this.timeout(5000);
        it('should load async', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            expect(protocol).is.not.null;
            expect(protocol.load).to.be.a('function');
        });
        it('should get modification date', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const date = await protocol.modified(parsed);
            expect(date).to.be.instanceOf(Date);
        });
        it('should load example', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed, {});
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
            //            console.log(inspect(JSON.parse(buffer.toString()), { depth: null }));
        });
        it('should load records', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(records);
            const [buffer] = await protocol.load(parsed, {});
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
            //            console.log(inspect(JSON.parse(buffer.toString()), { depth: null }));
        });
    });
});
