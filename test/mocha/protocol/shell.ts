import { expect } from 'chai';

import * as Protocol from '../../../src/protocol';
import * as url from 'url';

const type = 'shell';
const typeModule = `../../../src/protocol/${type}`;
const user = require('os').userInfo().username;
const example = `shell://${user}:${process.env.PASSWORD}@localhost${__dirname}#cat ../../data/example.json`;
const store = `shell://${__dirname}#cat > ../../data/example2.json`;

describe('protocol', () => {
    describe(type, () => {
        it('should load async', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            expect(protocol).is.not.null;
            expect(protocol.load).to.be.a('function');
        });
        it('should get modification date', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const date = await protocol.modified(parsed);
            expect(date).to.be.instanceOf(Date);
        });
        it('should load example', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            const [buffer] = await protocol.load(parsed);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
        });
        it('should save example', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(example);
            let pstore = new url.URL(store);
            const [buffer] = await protocol.load(parsed);
            await protocol.save(pstore, buffer);
            expect(buffer.length).to.be.greaterThan(0);
            expect(buffer.toString()).to.contain('example');
        });
        it('should call command with arguments', async () => {
            const protocol = <Protocol.Handler>await import(typeModule);
            let parsed = new url.URL(`shell:///#${encodeURIComponent('echo my@value')}`);
            const [buffer] = await protocol.load(parsed);
            expect(buffer.length).to.be.greaterThan(0);
            console.log(buffer.toString())
            expect(buffer.toString()).to.contain('my@value');
        });
    });
});
