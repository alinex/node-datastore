DROP TABLE IF EXISTS example;
CREATE TABLE example (
    name VARCHAR(120) PRIMARY KEY,
    value VARCHAR(64)
);
INSERT INTO example (name, value)
VALUES
   ('name','example'),
   ('null',NULL),
   ('boolean','true'),
   ('string','test'),
   ('number','5.6'),
   ('date','2016-05-10T19:06:36.909Z'),
   ('list.0','1'),
   ('list.1','2'),
   ('list.2','3'),
   ('person.name','Alexander Schilling'),
   ('person.job','Developer'),
   ('complex.0.name','Egon'),
   ('complex.1.name','Janina');

DROP TABLE IF EXISTS records;
CREATE TABLE records (
    company VARCHAR(20) PRIMARY KEY,
    "chief.name" VARCHAR(32),
    "chief.job" VARCHAR(32),
    "chief.partner.0.name" VARCHAR(32),
    "chief.partner.1.name" VARCHAR(32)
);
INSERT INTO records
VALUES
    ('example 1','Alexander Schilling','Developer','Egon','Janina'),
    ('example 2','Glen Campaign','Developer','Marc','Dennon'),
    ('example 3','Marie Mc Fling','Architect','Anton','Anina');
