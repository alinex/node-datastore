// use an object
module.exports = {
    name: 'example',
    // null value
    null: null,
    // boolean setting
    boolean: true,
    // include a string
    string: 'test',
    // any integer or float number
    number: 5.6,
    // a date as string
    date: new Date('2016-05-10T19:06:36.000Z'),
    // and a list of numbers
    list: [1, 2, 3],
    // add a sub object
    person: {
        name: 'Alexander Schilling',
        job: 'Developer'
    },
    // complex list with object
    complex: [{ name: 'Egon' }, { name: 'Janina' }]
};
