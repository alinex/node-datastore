module.exports = [
    {
        company: 'example 1',
        chief: {
            name: 'Alexander Schilling',
            job: 'Developer'
        },
        partner: [{ name: 'Egon' }, { name: 'Janina' }]
    },
    {
        company: 'example 2',
        chief: {
            name: 'Glen Campaign',
            job: 'Developer'
        },
        partner: [{ name: 'Marc' }, { name: 'Dennon' }]
    },
    {
        company: 'example 3',
        chief: {
            name: 'Marie Mc Fling',
            job: 'Architect'
        },
        partner: [{ name: 'Anton' }, { name: 'Anina' }]
    }
];
