#!/bin/sh
systemctl is-active --quiet sshd || sudo systemctl start sshd
systemctl is-active --quiet vsftpd || sudo systemctl start vsftpd
systemctl is-active --quiet postgresql || sudo systemctl start postgresql
