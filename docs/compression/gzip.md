title: GZip

# GZip Compression

Gzip is based on the DEFLATE algorithm, which is a combination of LZ77 and Huffman coding. That's the same algorithm as used in ZIP archives. But Gzip is used to compress just a single files. Compressed archives are typically created by assembling collections of files into a single `tar` archive and then compressing them. See the [tgz compression](tgz.md).

!!! definition

    Format: `gzip`<br>
    File extension: `.gz`

The following options are possible:

-   `compressionLevel: number` - gzip compression level 0 (no compression) to 9 (best compression, default)

An example (base.js) compressed will look like:

```hex
00000000: 1f8b 0800 0000 0000 0003 554f 3d0b c230  ..........UO=..0
00000010: 10dd fd15 4726 855a d28a 8566 13ea e2aa  ....G&.Z...f....
00000020: 93d2 216d 0fad 5c13 49e3 0714 ffbb 972a  ..!m..\.I......*
00000030: 4297 70f7 debd 8f0c 3300 a33b 5420 f0a5  B.p.....3..;T ..
00000040: bb1b a188 0274 2752 e31b b6ca 5a42 6d14  .....t'R....ZBm.
00000050: 7877 c700 f4de b5e6 cc1a 8fbd ff09 ba0a  xw..............
00000060: 9d82 759c 85b5 d19e 2d0d 3ea1 e069 2e52  ..u.....-.>..i.R
00000070: 9964 4bb9 5e26 f290 e44a 666a 95c5 b9cc  .dK.^&...Jfj....
00000080: 8f62 11ae a9ed bd82 5312 411a c1aa 0cd0  .b......S.A.....
00000090: 0d5d 6f39 71e0 f9df 7043 dcd1 34e8 605f  .]o9q...pC..4.`_
000000a0: 5f5a 22ee 3086 035c 6dc5 7c81 0f24 cb52  _Z".0..\m.|..$.R
000000b0: c1e0 3b30 b50d 5f7a b1f9 c468 7bb6 e67b  ..;0.._z...h{..{
000000c0: 330d d869 d31a 3d32 e5ec fd01 c5c7 e3f1  3..i..=2........
000000d0: 1c01 0000                                ....
```

It consists of:

-    a 10-byte header, containing a magic number (`1f 8b`), compression ID (08 for DEFLATE), file flags, a 32-bit timestamp, compression flags and operating system ID.
-    optional extra headers denoted by file flags, such as the original filename
-    a body, containing a DEFLATE-compressed payload
-    an 8-byte footer, containing a CRC-32 checksum and the length of the original uncompressed data, modulo 2 32 {\displaystyle 2^{32}} 2^{32}

{!docs/assets/abbreviations.txt!}
