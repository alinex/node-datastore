title: TAR + BZip2

# TAR + BZip2 Archives

Based on a [TAR](tar.md) archive, which can be used standalone this adds compression to it.

!!! definition

    Format: `tbz2#<file>`<br>
    File extension: `.tbz2` or `.tar.bz2`

If no hash is given on reading the first file the archive is used. So if the archive only contains one file the hash part of the URI is not needed on reading.

An example (base.js) compressed archive will look like:

```hex
00000000: 425a 6839 3141 5926 5359 385b fbdc 0000  BZh91AY&SY8[....
00000010: d77f 91dc 1000 4040 e7ff b226 100c 3a7f  ......@@...&..:.
00000020: f7df ea04 0020 0000 0830 0179 5b56 a1a2  ..... ...0.y[V..
00000030: 1a99 1a9a 6645 1e26 a646 8000 3688 c8f4  ....fE.&.F..6...
00000040: d211 1880 a7a9 ea19 0034 0003 4000 004a  .........4..@..J
00000050: 6a6a 3499 a44f 29ea 01b2 8680 681a 0681  jj4..O).....h...
00000060: 89a7 e1a6 ca76 b14c 406d e981 be5c 80c2  .....v.L@m...\..
00000070: 668b 40d9 681a 3ed4 b67a 553c f751 19bc  f.@.h.>..zU<.Q..
00000080: 2b2d ac05 8cbd c186 b018 cc34 79a3 226b  +-.........4y."k
00000090: 35d8 e61e b728 4109 1a11 5c57 7561 c875  5....(A...\Wua.u
000000a0: e1b7 0e0c 7aa8 7874 bbdb e8c8 de26 9a5c  ....z.xt.....&.\
000000b0: 1d1d 3db7 825c b76a ee5d 3d52 39dd 9e6c  ..=..\.j.]=R9..l
000000c0: 5de2 89be c1ce 07f6 3ccd 41d3 01a7 cebe  ].......<.A.....
000000d0: 10ea 35b9 e593 c5ef f399 f4ce b272 51d0  ..5..........rQ.
000000e0: 6b32 8551 841a c50c f209 ac24 56f7 0bdc  k2.Q.......$V...
000000f0: d059 a816 93af 9528 9b15 fd27 a7b0 5b9e  .Y.....(...'..[.
00000100: 1599 91e6 b916 3541 8b01 c7d0 dc98 04e9  ......5A........
00000110: 3835 6e55 071c 7df7 1e4b e591 0a19 4fcc  85nU..}..K....O.
00000120: 48a2 8271 9ec7 a9a2 bdd1 b493 1280 0190  H..q............
00000130: 7d40 cb51 b77f 1025 4e96 8bc0 5e7d 3445  }@.Q...%N...^}4E
00000140: 0855 8cd9 6d49 19ac 4c6b 516f a4b1 9d85  .U..mI..LkQo....
00000150: 0b28 1685 8e29 30d0 d2e5 c2ba aa9d 906b  .(...)0........k
00000160: 5588 4951 92a2 a79c 7831 0501 d24f 2bdd  U.IQ....x1...O+.
00000170: b058 8919 72dc 3a48 b5f2 96bc d008 6561  .X..r.:H......ea
00000180: 1852 b3d8 a4a8 dec8 071a 303c e04d a481  .R........0<.M..
00000190: 95b7 b9e8 d44a 4f68 f852 3476 aa04 999e  .....JOh.R4v....
000001a0: 07b2 e2dc 8099 bfc5 dc91 4e14 240e 16fe  ..........N.$...
000001b0: f700
```

{!docs/assets/abbreviations.txt!}
