title: Brotli

# Brotli Compression

Brotli is based on the DEFLATE algorithm like Gzip but uses a dictionary to gain better compression but with fast speed. It is designed by Google specifically for web transfer.

!!! definition

    Format: `brotli`<br>
    File extension: `.br`

An example (base.js) compressed will look like:

```hex
00000000: 1b1b 0120 2c0e 7813 eb94 f0aa 86a6 7e9e  ... ,.x.......~.
00000010: 0860 5c96 2953 7b0d d2a7 2656 ddf4 7a4b  .`\.)S{...&V..zK
00000020: 6e95 3a51 30d7 39f1 7063 8830 da2a 526a  n.:Q0.9.pc.0.*Rj
00000030: 1005 9698 9637 21e7 f7f6 1b9b 201e ca4f  .....7!..... ..O
00000040: fac8 1445 562b a7db 0c8e 66fb d51d c450  ...EV+....f....P
00000050: 5e0f cd4e 785e c3c2 0689 a0c0 3ef7 ff5b  ^..Nx^......>..[
00000060: 66f3 1672 82a0 ab3f c357 1bd5 d426 9592  f..r...?.W...&..
00000070: d9e7 e588 6f38 45ad 31ab 2bbf da58 5aea  ....o8E.1.+..XZ.
00000080: da6f 58c4 d19d e341 ae20 6546 9955 a112  .oX....A. eF.U..
00000090: 05a2 03a4 d70e 7a6c 635c dc1f 4263 2934  ......zlc\..Bc)4
000000a0: 4a06 1f80 0657 c1b0 ddb6 bd52 cb65 c716  J....W.....R.e..
000000b0: 300b                                     0.
```

It's compressed size is mostly smaller than GZip and also faster to compress and decompress.
The reason being, it uses a dictionary of common keywords and phrases on both client and server side and thus gives a better compression ratio. It is supported by all major browsers.

But for file compression it is rarely used, today.

{!docs/assets/abbreviations.txt!}
