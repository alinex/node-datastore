title: TAR + LZMA

# TAR + LZMA Archives

Based on a [TAR](tar.md) archive, which can be used standalone this adds compression to it.

!!! definition

    Format: `tlz#<file>`<br>
    File extension: `.tlz` or `.tar.lzma`

The following options are possible:

-   `compressionLevel: number` - gzip compression level 0 (no compression) to 9 (best compression, default)

If no hash is given on reading the first file the archive is used. So if the archive only contains one file the hash part of the URI is not needed on reading.

An example (base.js) compressed archive will look like:

```hex
00000000: 5d00 0000 0200 0800 0000 0000 0000 3118  ].............1.
00000010: 4acc eca6 897a f175 50ae 9fc2 7578 4bcb  J....z.uP...uxK.
00000020: 7a73 cc15 7c64 d27c c4bd 6ca0 882f a1f0  zs..|d.|..l../..
00000030: 0385 20cb 6118 3d7c 730b f091 0e7d 929e  .. .a.=|s....}..
00000040: aa06 e45c 98bc 942f 96d8 516a 7810 ab59  ...\.../..Qjx..Y
00000050: 5595 cc9a b3c0 413f 1519 6cfb fdcf a1c8  U.....A?..l.....
00000060: da4d afc7 a629 061f 3dc1 8db9 1545 662b  .M...)..=....Ef+
00000070: 7655 d408 9a05 45b4 3adf 459f 55da 854f  vU....E.:.E.U..O
00000080: 42b6 7bcb 4d89 4b96 44ee ee94 6145 c23a  B.{.M.K.D...aE.:
00000090: ed68 cc4a 748d 6c56 cfef ea75 c327 e7d2  .h.Jt.lV...u.'..
000000a0: 6bc2 0a44 3324 a354 5bda ce10 ff65 48d5  k..D3$.T[....eH.
000000b0: ef01 0940 2dff 6e65 adab 1224 d319 2f66  ...@-.ne...$../f
000000c0: 65b8 a46c 0253 cb30 8cc0 5730 943b 7e69  e..l.S.0..W0.;~i
000000d0: 0094 639c af64 bc80 67f2 9b08 4a9b 0191  ..c..d..g...J...
000000e0: 2c85 e253 00ef 88cc 6921 14b4 d097 e499  ,..S....i!......
000000f0: a734 3eb7 3c4d 7a57 297b 8427 9654 1781  .4>.<MzW){.'.T..
00000100: 9e56 9cfa 2b56 007e aa41 ba46 51bc cf64  .V..+V.~.A.FQ..d
00000110: 7018 aa9a 40ff 50b3 aa00                 p...@.P...
```

{!docs/assets/abbreviations.txt!}
