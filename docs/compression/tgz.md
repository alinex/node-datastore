title: TAR + GZip

# TAR + GZip Archives

Based on a [TAR](tar.md) archive, which can be used standalone this adds compression to it.

!!! definition

    Format: `tgz#<file>`<br>
    File extension: `.tgz` or `.tar.gz`

The following options are possible:

-   `compressionLevel: number` - gzip compression level 0 (no compression) to 9 (best compression, default)

If no hash is given on reading the first file the archive is used. So if the archive only contains one file the hash part of the URI is not needed on reading.

An example (base.js) compressed archive will look like:

```hex
00000000: 1f8b 0800 3e51 c55c 0003 ed52 bb6e 8330  ....>Q.\...R.n.0
00000010: 14cd cc57 dc8d 56ca c33c 4214 a40e 95d2  ...W..V..<B.....
00000020: a56b 3bb5 ca60 e026 2132 7684 4d42 15e5  .k;..`.&!2v.MB..
00000030: df7b 7984 30b5 5354 55e2 0cd8 3ef7 718e  .{y.0.STU...>.q.
00000040: 2f8e b8c6 e95e 8fee 09c6 5810 f840 abb3  /....^....X..@..
00000050: 98b3 fe5a c361 3e03 c7f3 e7fe 8279 0b2f  ...Z.a>......y./
00000060: 00e6 b86e e08d 80dd d555 8b42 1b9e 9315  ...n.....U.B....
00000070: 2eb0 fc29 efb7 7873 17e8 d67f 82d9 0c0a  ...)..xs........
00000080: 8dc0 25a8 688f b1b1 3295 1402 a758 1e54  ..%.h...2....X.T
00000090: 6e34 3cc1 d902 82e4 1986 6063 c9b3 8340  n4<.......`c...@
000000a0: 7b5c 9354 2c0b 21e0 c845 814d 1a1d c3fa  {\.T,.!..E.M....
000000b0: db65 444a 09a4 fe1a 8d49 e5b6 a65b 2e04  .eDJ.....I...[..
000000c0: 9317 d865 a632 1645 4266 409b fc9a da6c  ...e.2.EBf@....l
000000d0: 49da a036 375d 2ebf 28df e016 7350 396c  I..67]..(...sP9l
000000e0: 84e2 8674 b308 f3d6 48b5 0d61 3e0d 6e35  ...t....H..a>.n5
000000f0: 9070 43ed 75bf 7f45 9163 3cc1 8a76 0fb6  .pC.u..E.c<..v..
00000100: cb9c 60c2 e613 87bd 3bcb 9005 a117 4c97  ..`.....;.....L.
00000110: 6cf9 613f f6b4 13ea 2552 6d40 6d5a 255d  l.a?....%Rm@mZ%]
00000120: 072b 2e84 4f67 0cee 18bc f5ad 22a9 2a74  .+..Og......".*t
00000130: 115d a75c f107 aa52 3483 66c0 bd21 3fd3  .].\...R4.f..!?.
00000140: 5323 0dba d95b bc4b 8520 abed c52b ec55  S#...[.K. ...+.U
00000150: 4439 2b3c a250 d4c2 ae03 974e 2a56 d50f  D9+<.P.....N*V..
00000160: 2a1b 7ba7 d4ec fa92 6d90 2c9e af62 2f5b  *.{.....m.,..b/[
00000170: 256d aa87 8e79 e532 959c b8b5 75b1 fefa  %m...y.2....u...
00000180: 7d0e 1830 60c0 80fb e01b eb93 51c1 000a  }..0`.......Q...
00000190: 0000
```

{!docs/assets/abbreviations.txt!}
