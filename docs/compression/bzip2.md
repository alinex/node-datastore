title: BZip2

# BZip2 Compression

The bzip2 compression uses the Burrows–Wheeler algorithm. But BZip2 is used to compress just a single files. Compressed archives are typically created by assembling collections of files into a single `tar` archive and then compressing them. See the [tar.bz2 compression](tbz2.md).

!!! definition

    Format: `bzip2`<br>
    File extension: `.bz2`

An example (base.js) compressed will look like:

```hex
00000000: 425a 6839 3141 5926 5359 de0d 114d 0000  BZh91AY&SY...M..
00000010: 72df 8044 1040 e7fb 3226 100c 1a3f f7df  r..D.@..2&...?..
00000020: ea30 013a 081a 048d 1a93 6123 614f 4d4f  .0.:......a#aOMO
00000030: 5347 9469 9a86 9e28 61a1 a680 1a00 0d00  SG.i...(a.......
00000040: 00c8 6812 9a9a a7a7 aa3c 927a 10c1 a086  ..h......<.z....
00000050: 8018 80ce 0c40 1c39 2e5f de5c e73a c59f  .....@.9._.\.:..
00000060: f3e1 4066 6c60 c6b1 a056 feda e767 adc9  ..@fl`...V...g..
00000070: 10c1 98c9 c9b2 ed38 7b8e 6ebd baf7 6824  .......8{.n...h$
00000080: d00f 93f6 3c5a 9a84 b2c9 d9f4 d975 6096  ....<Z.......u`.
00000090: 4c6d bd67 ba25 1cfd 99f8 838b c880 c001  Lm.g.%..........
000000a0: 847c eaca 19b6 59f0 9de8 c7f0 ff96 5549  .|....Y.......UI
000000b0: b032 af95 beed 5de2 9ab7 ce66 47b9 9059  .2....]....fG..Y
000000c0: 3a01 19ec 8041 600d 3341 a74c cf65 d48c  :....A`.3A.L.e..
000000d0: 9d7f 782a c095 ecd9 48e7 a45d 38f1 364d  ..x*....H..]8.6M
000000e0: c02a 0d49 28b5 f6ec a0dd 2d3e 128b 0f94  .*.I(.....->....
000000f0: d4b8 870d 25d7 edd1 37a0 94d5 0bfa 72d1  ....%...7.....r.
00000100: 4a8a b784 1ab3 c5b1 16da 5345 ce23 0f85  J.........SE.#..
00000110: 6884 9463 a147 98f2 624a 43a4 9ebb 1db6  h..c.G..bJC.....
00000120: ac25 4bef cc61 b334 5713 7765 3026 cec8  .%K..a.4W.we0&..
00000130: 94b3 abba e983 2909 ca2c 8d17 026d 17c5  ......)..,...m..
00000140: 66d1 4a3e 41f6 e21a c2c1 2625 23dd 68ea  f.J>A.....&%#.h.
00000150: 91d0 5dc9 14e1 4243 7834 4534            ..]...BCx4E4
```

{!docs/assets/abbreviations.txt!}
