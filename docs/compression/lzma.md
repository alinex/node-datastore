title: LZMA

# LZMA Compression

The LZMA compression uses the Lempel-Ziv-Markov (LZMA) chain compression algorithm. But LZMA is used to compress just a single files. Compressed archives are typically created by assembling collections of files into a single `tar` archive and then compressing them. See the [tar.lzma compression](tlz.md).

!!! definition

    Format: `lzma`<br>
    File extension: `.lzma`

The following options are possible:

-   `compressionLevel: number` - gzip compression level 0 (no compression) to 9 (best compression, default)

An example (base.js) compressed will look like:

```hex
00000000: 5d00 0000 021c 0100 0000 0000 0000 3d82  ].............=.
00000010: 8017 6817 1424 a1c6 43a6 dd4f d57f 32d9  ..h..$..C..O..2.
00000020: 0fa4 0a98 756f 05b1 badb 5635 8534 7ad2  ....uo....V5.4z.
00000030: ee77 9b91 cbbd 4a11 5aa3 811c 0e6c 61bb  .w....J.Z....la.
00000040: 9e6b af85 8a28 ab5f 56b0 d51b 8037 51c5  .k...(._V....7Q.
00000050: 9a46 e85c bcf2 13c9 d1c2 dc8f 0c52 7782  .F.\.........Rw.
00000060: 28bd 467e 50f9 0e25 4651 8e76 0f6d a39a  (.F~P..%FQ.v.m..
00000070: dc86 8b3b ff8b 8c6d 99af 7f2c 2523 8935  ...;...m...,%#.5
00000080: d6bb f04a 9d62 c274 e036 4533 27bc c071  ...J.b.t.6E3'..q
00000090: d5aa 1a6f 09df 1312 b6f0 1311 12c9 6e19  ...o..........n.
000000a0: bb3d b415 a6ef 507b abcd 5aa1 7ad8 e073  .=....P{..Z.z..s
000000b0: 86ba 1121 062c 9b2f 07da 8d84 30f4 f93d  ...!.,./....0..=
000000c0: 5ef1 ebc6 7637 527d befe bc53 58b0 05cd  ^...v7R}...SX...
000000d0: fd65 3690 ffee 5231 7e                   .e6...R1~
```

{!docs/assets/abbreviations.txt!}
