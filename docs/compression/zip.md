title: ZIP

# ZIP Archives

ZIP is based on the DEFLATE algorithm, which is a combination of LZ77 and Huffman coding. That's the same algorithm as used in GZip archives. But while Gzip is used to compress just a single file. ZIP files are archives which may contain multiple files.

!!! definition

    Format: `zip#<file>`<br>
    File extension: `.zip`

The following options are possible:

-   `compressionLevel: number` - gzip compression level 0 (no compression) to 9 (best compression, default)

This format will auto detect the filename if only one file is given. Therefore the hash part within the URL is not needed for single file ZIP archives on reading.

An example (base.js) compressed archive will look like:

```hex
00000000: 504b 0304 1403 0000 0800 7daa 8e4e f964  PK........}..N.d
00000010: 397a 2e01 0000 2002 0000 0700 0000 6261  9z.... .......ba
00000020: 7365 2e6a 734d 51cb 6ec2 3010 bce7 2bf6  se.jsMQ.n.0...+.
00000030: 9656 e291 8088 84a5 1e2a d14b afed a915  .V.......*.K....
00000040: 0727 5920 c8b1 51bc 8654 887f efda 9810  .'Y ..Q..T......
00000050: 1fa2 cdec ccce 783d 9f83 b308 5283 298f  ......x=....R.).
00000060: 5851 d29a da29 9c61 7f32 1d59 7883 6b02  XQ...).a.2.Yx.k.
00000070: 7cb4 6c51 408a bd6c 4f0a d349 0059 ac9d  |.lQ@..lO..I.Y..
00000080: 5270 96ca a147 c2af 08df 8151 1aa3 90e7  Rp...G.....Q....
00000090: 5b24 6af4 dec3 0f4c 0075 0e07 66a3 2be5  [$j....L.u..f.+.
000000a0: 6a0e 0396 ba48 8d25 5b13 5a1a 7c39 f01f  j....H.%[.Z.|9..
000000b0: f309 f7d8 81e9 60a7 8c24 f66d 4bec 3c25  ......`..$.mK.<%
000000c0: 9602 56b3 e2a9 815a 128f b7a3 f901 e2c4  ..V....Z........
000000d0: 7881 0d57 2fe9 22cb 8b69 b69a e6d9 77be  x..W/."..i....w.
000000e0: 1659 2196 c56c 9dad 7fd2 d791 77cd b354  .Y!..l......w..T
000000f0: 6309 cc2e 3a59 df0c 9880 df7c 028b 092c  c...:Y.....|...,
00000100: b74f 45ed 15d6 9571 cb01 3fb1 ca68 1117  .OE....q..?..h..
00000110: 3c5a f2bb c29e 3df8 665f d5a1 518a a3fa  <Z....=.f_..Q...
00000120: 8bc7 7334 2573 3678 4665 7844 1a1a b7c1  ..s4%s6xFexD....
00000130: aa32 fe81 fa7b bc4b 4387 91e5 a3c9 11af  .2...{.KC.......
00000140: 0fb3 8fbd d129 eb61 403e a56e b464 6c9b  .....).a@>.n.dl.
00000150: dc92 7f50 4b01 023f 0314 0300 0008 007d  ...PK..?.......}
00000160: aa8e 4ef9 6439 7a2e 0100 0020 0200 0007  ..N.d9z.... ....
00000170: 0024 0000 0000 0000 0020 80b4 8100 0000  .$....... ......
00000180: 0062 6173 652e 6a73 0a00 2000 0000 0000  .base.js.. .....
00000190: 0100 1800 0083 6e0c f7f2 d401 809a 8d05  ......n.........
000001a0: 80f6 d401 0083 6e0c f7f2 d401 504b 0506  ......n.....PK..
000001b0: 0000 0000 0100 0100 5900 0000 5301 0000  ........Y...S...
000001c0: 0000                                     ..
```

{!docs/assets/abbreviations.txt!}
