title: JSON

# JSON Format

This format uses the JavaScript object notation a human readable structure. It is widely used in different languages, not only in JavaScript. See description at [Wikipedia](http://en.wikipedia.org/wiki/Json).

!!! definition

    Format: `json`<br>
    File extension: `.json`<br>
    Mimetype: `application/json`

An example may look like:

```json
{!test/data/example.json!}
```

JSON's basic data types are:

-   Number: a signed decimal number that may contain a fractional part and may use exponential E notation, but cannot include non-numbers like `NaN`.
-   String: a sequence of zero or more Unicode characters. Strings are delimited with double-quotation marks and support a backslash escaping syntax.
-   Boolean: either of the values `true` or `false`
-   Array: an ordered list of zero or more values, each of which may be of any type. Arrays use square bracket notation with elements being comma-separated.
-   Object: an unordered collection of name/value pairs where the keys are strings. Objects are delimited with curly brackets and use commas to separate each pair, while within each pair the colon ':' character separates the key or name from its value.
-   Date: will be formatted as ISO string and stay as string after parsing
-   null: An empty value, using the word null

Whitespace (space, horizontal tab, line feed, and carriage return) is allowed and ignored around or between syntactic elements.

JSON won't allow comments but you may use JavaScript like comments using `//` and `/*...*/` like known in javascript. Therefore use the javascript parsing described below.

## Transformations

`Date` will be stored in its ISO `string` representation. Therefore the parser will also cast `strings` which are valid dates back to `Date` objects.

{!docs/assets/abbreviations.txt!}
