title: HTML

# HTML Format

This format converts the hypertext document in a structured definition using objects and arrays.

!!! definition

    Format: `html`<br>
    File extension: `.htm .html`<br>
    Mimetype: `text/html`

An example may look like:

```html
{!test/data/example.html!}
```

This will make a structure like:

```js
[
    { "!": "!DOCTYPE html" },
    { _: "\n" },
    {
        html: [
            { _: "\n    " },
            {
                body: [
                    { _: "\n        " },
                    { h1: [{ _: "My First Heading" }] },
                    { _: "\n        " },
                    {
                        p: [{ $: { class: "line" } }, { _: "My first paragraph." }]
                    },
                    { _: "\n    " }
                ]
            },
            { _: "\n" }
        ]
    },
    { _: "\n" }
];
```

Attributes are stored in `$`, declarations in `!` element while text is stored in `_` element if sub-tags exist.

{!docs/assets/abbreviations.txt!}
