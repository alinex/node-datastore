title: INI

# INI Format

This is one of the oldest formats used for configurations. It is very simple but
allows also complex objects through extended groups.

!!! definition

    Format: `ini`<br>
    File extension: `.ini`<br>
    Mimetype: `text/x-ini`

An example may look like:

```ini
{!test/data/example.ini!}
```

Spaces around equal signs are prevented to be compatible with older parsers.

Comments start with semicolon and groups or sections are marked by square brackets.
The group name defines the object to add the properties to.

## Transformations

`Date` will be stored in its ISO `string` representation. Therefore the parser will also cast `strings` which are valid dates back to `Date` objects.

Arrays of objects will be converted to objects of objects on write to be presented as sections (using numbers within). On parsing objects with numbered keys are ported back to arrays.

{!docs/assets/abbreviations.txt!}
