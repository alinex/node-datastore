title: CSV

# CSV Format

The CSV format is a flat two dimensional format for which the structure will be stored in a flattened format.

!!! definition

    Format: `csv`<br>
    File extension: `.csv`<br>
    Mimetype: `text/csv`

The following options are possible:

-   `records` - set to `true` for record format, see below
-   `delimiter` - character to be used as field separator in CSV data (default: `,`).
-   `quote`- character used to quote field values in CSV data (default: `"`).

An example may look like:

```csv
{!test/data/example.csv!}
```

If the top level element is an array the format will be more like normal CSV:

```csv
"company","chief.name","chief.job","partner.0.name","partner.1.name"
"example 1","Alexander Schilling","Developer","Egon","Janina"
"example 2","Glen Campaign","Developer","Marc","Dennon"
"example 3","Marie Mc Fling","Architect","Anton","Anina"
```

## Transformations

The CSV will have no header because it only has name and value columns in it. It uses the defaults like `,` as field separator and `"` to quote strings. All strings will be quoted.

Unquoted values will be converted to `number`, `boolean` or `null` if possible. Dates are represented as ISO string and will also be back ported to `Date`.
The structure in any depth will be stored with its path and value lines.

All of these is converted back on parse.

## Custom Format

To import a custom format like:

```csv
{!test/data/example.custom.csv!}
```

You can do so with the options:

- `delimiter: ': '`
- `quote: ''`

{!docs/assets/abbreviations.txt!}
