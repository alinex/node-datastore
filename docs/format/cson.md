title: CSON

# CSON Format

Like JSON but here the object is defined using CoffeeScript instead of JavaScript which makes it better readable.

!!! definition

    Format: `cson`<br>
    File extension: `.cson`<br>
    Mimetype: `text/x-coffeescript`

An example may look like:

```coffee
{!test/data/example.cson!}
```

CSON solves several major problems with hand-writing JSON by providing:

- the ability to use both single-quoted and double-quoted strings
- the ability to write multi-line strings in multiple lines
- the ability to write a redundant comma
- comments start with `#` and are allowed

Besides this facts it's the same as JSON and have the same types.

## Transformations

`Date` will be stored in its ISO `string` representation. Therefore the parser will also cast `strings` which are valid dates back to `Date` objects.

{!docs/assets/abbreviations.txt!}
