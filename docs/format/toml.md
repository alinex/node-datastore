title: TOML

# TOML Format

TOML (Tom's Obvious, Minimal Language) aims to be a minimal configuration file format that's easy to read due to obvious semantics. TOML is designed to map unambiguously to a hash table.

!!! definition

    Format: `toml`<br>
    File extension: `.toml`<br>
    Mimetype: `text/x-toml`

An example may look like:

```ini
{!test/data/example.toml!}
```

Read more about the [format](https://github.com/mojombo/toml).

!!! attention

    `NULL` values are not stored, the whole entry will be removed which should be the same for further processing.

## Transformations

As TOML doesn't allow the top level element to be an array, this will be converted to object for storing and transformed back on parse.

{!docs/assets/abbreviations.txt!}
