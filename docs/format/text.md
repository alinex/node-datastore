title: TEXT

# Plain Text Format

This is not really a specific format but works more like a simple put through.

!!! definition

    Format: `txt`<br>
    File extension: `.txt`, `.log`<br>
    Mimetype: `text/plain`

Everything is returned as a simple string with the complete content.

But if data structures are given to store, they will be formatted as human readable structure using the `util.inspect` method.

The following options are possible:

-   `pattern: RegExp` - for text format to be split into pattern matches (Using named groups) for event logs

## RegExp Matching

The pattern matching is used to match event log entries into a data structure:

    2019-10-30 09:04:36,829 (          ) [WARN ] - SQL Error: 0, SQLState: null
    2019-10-30 09:04:36,829 (          ) [ERROR] - [ajp-bio-8093-exec-2336] Timeout: Pool empty. Unable to fetch a connection in 30 seconds, none available[size:4; busy:4; idle:0; lastwait:30000].
    2019-10-30 09:04:36,830 (          ) [WARN ] - Handler execution resulted in exception: Could not open JPA EntityManager for transaction; nested exception is org.hibernate.exception.GenericJDBCException: Could not open connection
    2019-10-30 09:04:36,883 (          ) [WARN ] - SQL Error: 0, SQLState: null
    2019-10-30 09:04:36,884 (          ) [ERROR] - [ajp-bio-8093-exec-2236] Timeout: Pool empty. Unable to fetch a connection in 30 seconds, none available[size:4; busy:4; idle:0; lastwait:30000].
    2019-10-30 09:04:36,884 (          ) [WARN ] - Handler execution resulted in exception: Could not open JPA EntityManager for transaction; nested exception is org.hibernate.exception.GenericJDBCException: Could not open connection

Such a text from log could be split up with the following expression:

```js
pattern: /(?<date>\d+-\d+-\d+ \d+:\d+:\d+,\d+)\s+.*?\[(?<level>[A-Z+]+)\s*\] - (\[(?<code>.*?)\] )?(?<message>.*)/g;
```

This gives you the following data structure:

```js
[
    {
        date: '2019-10-30 09:04:36,829',
        level: 'WARN',
        code: undefined,
        message: 'SQL Error: 0, SQLState: null'
    },
    {
        date: '2019-10-30 09:04:36,829',
        level: 'ERROR',
        code: 'ajp-bio-8093-exec-2336',
        message:
            'Timeout: Pool empty. Unable to fetch a connection in 30 seconds, none available[size:4; busy:4; idle:0; lastwait:30000].'
    },
    {
        date: '2019-10-30 09:04:36,830',
        level: 'WARN',
        code: undefined,
        message:
            'Handler execution resulted in exception: Could not open JPA EntityManager for transaction; nested exception is org.hibernate.exception.GenericJDBCException: Could not open connection'
    },
    {
        date: '2019-10-30 09:04:36,883',
        level: 'WARN',
        code: undefined,
        message: 'SQL Error: 0, SQLState: null'
    },
    {
        date: '2019-10-30 09:04:36,884',
        level: 'ERROR',
        code: 'ajp-bio-8093-exec-2236',
        message:
            'Timeout: Pool empty. Unable to fetch a connection in 30 seconds, none available[size:4; busy:4; idle:0; lastwait:30000].'
    },
    {
        date: '2019-10-30 09:04:36,884',
        level: 'WARN',
        code: undefined,
        message:
            'Handler execution resulted in exception: Could not open JPA EntityManager for transaction; nested exception is org.hibernate.exception.GenericJDBCException: Could not open connection'
    }
];
```

{!docs/assets/abbreviations.txt!}
