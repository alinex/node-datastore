title: XML

# XML Format

The XML format should only use Tags and values, but no arguments for normal data structures.
Arguments and combined text with sub-tags are possible but leads to a specific data structure.

!!! definition

    Format: `xml`<br>
    File extension: `.xml`<br>
    Mimetype: `application/xml`

The following options are possible:

-   `rootName: string` - define the root element name in formatting as XML
-   `records: boolean` - flag to read the CSV or table data always as with records (only needed with less than 3 columns)

An example may look like:

```xml
{!test/data/example.xml!}
```

If the top level element is an array, this will look like (the `entry` tag is a fixed name here):

```xml
{!test/data/list.xml!}
```

Attributes are stored in `$` element while text is stored in `_` element if sub-tags exist.

## Transformations

As all values are text numbers and `Date` will be auto converted back on parsing.

The root element name in formatting may be defined using the option `rootName`, the default is 'data'.

If the data structure is based on a top-level array this is converted to object named `entry` containing the array to store and removed on parsing again.

{!docs/assets/abbreviations.txt!}
