title: Properties

# Properties Format

Mainly in the Java world properties are used to setup configuration values.
But it won't have support for arrays, you only may use objects with numbered keys.

!!! definition

    Format: `properties`<br>
    File extension: `.properties`<br>
    Mimetype: `text/x-java-properties`

An example may look like:

```ini
{!test/data/example.properties!}
```

This format supports:

- key and value may be divided by `=`, `:` with spaces or only a space
- comments start with `!` or `#`
- structure data using sections with square brackets like in [INI](ini.md) files
- structure data using namespaces in the key using dot as separator
- references to other values with `${key}`
- references work also as section names or reference name

## Transformations

`Date` will be stored in its ISO `string` representation. Therefore the parser will also cast `strings` which are valid dates back to `Date` objects.

{!docs/assets/abbreviations.txt!}
