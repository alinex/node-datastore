title: MessagePack

# MessagePack Format

MessagePack is an efficient binary serialization format. It lets you exchange data among multiple languages like JSON. But it's faster and smaller.

!!! definition

    Format: `msgpack`<br>
    File extension: `.msgpack`<br>
    Mimetype: `application/x-msgpack`

An example binary may look like:

```hex
00000000: 89a4 6e61 6d65 a765 7861 6d70 6c65 a46e  ..name.example.n
00000010: 756c 6cc0 a762 6f6f 6c65 616e c3a6 7374  ull..boolean..st
00000020: 7269 6e67 a474 6573 74a6 6e75 6d62 6572  ring.test.number
00000030: cb40 1666 6666 6666 66a4 6461 7465 b832  .@.ffffff.date.2
00000040: 3031 362d 3035 2d31 3054 3139 3a30 363a  016-05-10T19:06:
00000050: 3336 2e39 3039 5aa4 6c69 7374 9301 0203  36.909Z.list....
00000060: a670 6572 736f 6e82 a46e 616d 65b3 416c  .person..name.Al
00000070: 6578 616e 6465 7220 5363 6869 6c6c 696e  exander Schillin
00000080: 67a3 6a6f 62a9 4465 7665 6c6f 7065 72a7  g.job.Developer.
00000090: 636f 6d70 6c65 7892 81a4 6e61 6d65 a445  complex...name.E
000000a0: 676f 6e81 a46e 616d 65a6 4a61 6e69 6e61  gon..name.Janina
```

It is fully compatible to JSON it is one of the smallest serialization format.

[Specification](https://github.com/msgpack/msgpack/blob/master/spec.md)

{!docs/assets/abbreviations.txt!}
