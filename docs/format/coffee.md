title: CoffeeScript

# CoffeeScript Format

The Coffee Script format is nearly the same as CSON but caused by the changed parser it may contain calculations, too.

!!! definition

    Format: `coffee`<br>
    File extension: `.coffee`<br>
    Mimetype: `text/x-coffeescript`

An example may look like:

```coffee
{!test/data/example.coffee!}
```

## Transformations

If you store these it will be in CSON format, which makes `Date` to be written in its ISO `string` representation. Therefore the parser will also cast `strings` which are valid dates back to `Date` objects.

{!docs/assets/abbreviations.txt!}
