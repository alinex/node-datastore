title: JS

# JS Format

Also allowed are normal JavaScript files. In comparison to the JSON format it is more loosely so you may use single quotes, keys don't need quotes at all and at last you may use calculations.

But to import it they have to be run in a sandbox which makes execution a bit slower.

!!! definition

    Format: `js`, 'javascript'<br>
    File extension: `.js`, `.mjs` or `.ts`<br>
    Mimetype: `application/javascript`

The following options are possible:

-   `module: boolean` - use module format in storing as JavaScript, so you can load it using normal `require` or `import`

An example may look like:

=== "simple"

    ```js
    {!test/data/example.js!}
    ```

=== "module"

    ```js
    {!test/data/base.js!}
    ```

Both of the above formats are possible to read, but to write the module format is only selected with file extension `.mjs`, `.ts` or the option:

-   `module` - set to `true` to write in module format so you can load it using normal `require` or `import`

As comments are ignored on reading, they will get lost on writing the data structure.

## XSS Protection

A primary feature of this package is to serialize code to a string of literal JavaScript which can be embedded in an HTML document by adding it as the contents of the `<script>` element. In order to make this safe, HTML characters and JavaScript line terminators are escaped automatically.

```js
data = '</script>';
formatted = '\\u003C\\u002Fscript\\u003E';
```

If you don't want this, you may use JSON format.

{!docs/assets/abbreviations.txt!}
