title: YAML

# YAML Format

This is a simplified and best human readable language to write structured
information. See some examples at [Wikipedia](http://en.wikipedia.org/wiki/YAML).

!!! definition

    Format: `yaml`<br>
    File extension: `.yml`<br>
    Mimetype: `text/x-yaml`

An example may look like:

```yaml
{!test/data/example.yml!}
```

The YAML syntax is very powerful but also easy to write in it's basics:

- comments allowed starting with `#`
- dates are allowed as ISO string, too
- different multiline text entries
- special number values
- references with `*xxx` to the defined `&xxx` anchor

See the example above.

{!docs/assets/abbreviations.txt!}
