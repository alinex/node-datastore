title: BSON

# BSON Format

Binary JSON is a more compressed version of JSON but not human readable because it's a binary format. It is mainly used in the MongoDB database.

!!! definition

    Format: `bson`<br>
    File extension: `.bson`<br>
    Mimetype: `application/bson`

An example binary may look like:

```hex
00000000: 0701 0000 026e 616d 6500 0800 0000 6578  .....name.....ex
00000010: 616d 706c 6500 0a6e 756c 6c00 0862 6f6f  ample..null..boo
00000020: 6c65 616e 0001 0273 7472 696e 6700 0500  lean...string...
00000030: 0000 7465 7374 0001 6e75 6d62 6572 0066  ..test..number.f
00000040: 6666 6666 6616 4002 6461 7465 0019 0000  fffff.@.date....
00000050: 0032 3031 362d 3035 2d31 3054 3139 3a30  .2016-05-10T19:0
00000060: 363a 3336 2e39 3039 5a00 046c 6973 7400  6:36.909Z..list.
00000070: 1a00 0000 1030 0001 0000 0010 3100 0200  .....0......1...
00000080: 0000 1032 0003 0000 0000 0370 6572 736f  ...2.......perso
00000090: 6e00 3600 0000 026e 616d 6500 1400 0000  n.6....name.....
000000a0: 416c 6578 616e 6465 7220 5363 6869 6c6c  Alexander Schill
000000b0: 696e 6700 026a 6f62 000a 0000 0044 6576  ing..job.....Dev
000000c0: 656c 6f70 6572 0000 0463 6f6d 706c 6578  eloper...complex
000000d0: 0035 0000 0003 3000 1400 0000 026e 616d  .5....0......nam
000000e0: 6500 0500 0000 4567 6f6e 0000 0331 0016  e.....Egon...1..
000000f0: 0000 0002 6e61 6d65 0007 0000 004a 616e  ....name.....Jan
00000100: 696e 6100 0000 00                        ina....
```

Compared to JSON, BSON is designed to be efficient both in storage space and scan-speed. Large elements in a BSON document are prefixed with a length field to facilitate scanning.

But in spite of its name, BSON's compatibility with JSON.
BSON has special types like "ObjectId", "Min key", "UUID" or "MD5" (comming from MongoDB). These types are not compatible with JSON. That means some type information can be lost when you convert objects from BSON to JSON, but of course only when these special types are in the BSON source. It can be a disadvantage to use both JSON and BSON in single service.

## Transformations

BSON don't allow top-level array into valid BSON according to the [BSON spec](http://bsonspec.org/#/specification). Therefor the top-level array is converted into an object with string numbered keys.
The deserialization of this module will detect such top-level objects and convert them back.

```js
[ { company: 'example 1',
    chief: { name: 'Alexander Schilling', job: 'Developer' },
    partner: [ [Object], [Object] ] },
  { company: 'example 2',
    chief: { name: 'Glen Campaign', job: 'Developer' },
    partner: [ [Object], [Object] ] },
  { company: 'example 3',
    chief: { name: 'Marie Mc Fling', job: 'Architect' },
    partner: [ [Object], [Object] ] } ]
```

This is stored in binary format like:

```js
{ '0':
   { company: 'example 1',
     chief: { name: 'Alexander Schilling', job: 'Developer' },
     partner: [ [Object], [Object] ] },
  '1':
   { company: 'example 2',
     chief: { name: 'Glen Campaign', job: 'Developer' },
     partner: [ [Object], [Object] ] },
  '2':
   { company: 'example 3',
     chief: { name: 'Marie Mc Fling', job: 'Architect' },
     partner: [ [Object], [Object] ] } }
```

So this is how other tools may display loaded BSON top-level arrays.

{!docs/assets/abbreviations.txt!}
