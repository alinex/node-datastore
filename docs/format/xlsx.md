title: XLSX

# Excel 2007+ XML Format

The XLSX format module allows to handle spreadsheets with one work sheet only. The content is like in the CSV format. This format is mainly to be preferred for Excel because CSV is not as easy to import.

!!! definition

    Format: `xlsx`<br>
    File extension: `.xlsx`<br>
    Mimetype: `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`

The following options are possible:

-   `records` - set to `true` for record format, see below

An example may look like (shown as CSV):

```csv
{!test/data/example.csv!}
```

If the top level element is an array the format will be more like normal table:

```csv
"company","chief.name","chief.job","partner.0.name","partner.1.name"
"example 1","Alexander Schilling","Developer","Egon","Janina"
"example 2","Glen Campaign","Developer","Marc","Dennon"
"example 3","Marie Mc Fling","Architect","Anton","Anina"
```

{!docs/assets/abbreviations.txt!}
