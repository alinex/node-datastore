title: Loading

# Loading from Persistent Store

## Initialization

If you want to load or save the DataStore it has to be initialized. Therefore an URL and maybe some options defining the concrete handling are needed.
But you can also set a list of data sources which will be merged together.

!!! definition

    ```ts
    interface DataSource {
        // URL to load this structure
        source?: string;
        options?: Options;
        // alteratively directly give data
        data?: any;
        // set base path in final structure
        base?: string;
        // relations
        depend?: string[];
        alternate?: string[];
        // default combine methods
        object?:  'combine' | 'replace' | 'missing' | 'alternate'; // default: 'combine'
        array?: 'concat' | 'replace' | 'overwrite' | 'combine'; // default: 'concat'
        // overwrite per position
        pathObject?: {[key: string]: 'combine' | 'replace' | 'missing' | 'alternate' }
        pathArray?: {[key: string]: 'concat' | 'replace' | 'overwrite' | 'combine' }
        // general settings
        clone?: boolean;
        allowFlags?: boolean; // default: true
        map?: boolean; // default: false
        // Automatically added on loading
        meta?: any;
    }

    interface Options {
        // http or https protocol
        proxy?: string;
        httpMethod?: string;
        httpHeader?: string[];
        httpData?: string;
        ignoreError?: boolean;
        // security
        privateKey?: Buffer | string;
        passphrase?: string;
        // file protocol
        tail?: number;
        // compression
        compression?: 'gzip' | 'brotli' | 'bzip2' | 'lzma' | 'tar' | 'tgz' |
                'tbz2' | 'tlz' | 'zip';
        compressionLevel?: number;
        // formatting
        format?: 'bson' | 'coffee' | 'cson' | 'csv' | 'ini' | 'js' | 'json' |
                'msgpack' | 'properties' | 'toml' | 'xml' | 'yaml';
        pattern?: RegExp;
        records?: boolean;
        module?: boolean;
        rootName?: string;
    }
    ```

    This can be used to initialize a new DataStrore:

    ```ts
    // initialize new store
    DataStore.constructor(...input?: DataSource[]): DataStore
    ```

Additionally three static methods allow to initialize and load a DataSource which afterwards may directly be red.

!!! definition

    Create a new data store and load it directly:

    ```ts
    DataStore.load(...input: DataSource[]): Promise<DataStore>
    ```

    From single source this can be called with:

    - `source` URL to load from
    - `options` options for loading

    Or with a predefined data structure.

    ```ts
    DataStore.url(source: string, options?: Options): Promise<DataStore>
    ```

    And to setup with predefined data:

    ```ts
    DataStore.data(data: any, source?: string): Promise<DataStore>
    ```

The first two attributes from DataSource are used for loading, the others are described in more detail in the [merge](https://alinex.gitlab.io/node-data/merge/) function.

You may also set or change the sources on each `load()` or `save()` method.

### Single source

The simplest use case is to use a source defined by an URI string with possible options to specify how to read them:

```ts
const ds = new DataStore({ source: "file:config.yml" });
const ds = new DataStore({ source: "http://my-site/config", options: { format: "json" } });
```

All option settings are defined below.

For single sources you can also initialize and load a DataSource in one step:

```ts
const ds = DataStore.url("file:config.yml");
```

### Combine multiple sources

It is also possible to load the data structure from multiple sources. This allows to separate big configuration in multiple files or allow for base settings which can be overwritten with specific ones at another location.

Each DataStore can either be used as multi source loader or with single source load and save.

```ts
const ds = new DataStore(
    { source: '/etc/myapp/base.json' },
    { source: '/etc/myapp/base.yaml', alternate: ['/etc/myapp/base.json'] },
    { source: '/etc/myapp/base.ini', alternate: ['/etc/myapp/base.json','/etc/myapp/base.yaml'] },
    { source: '/etc/myapp/email.yaml', base: 'email', depend: ['/etc/myapp/base.json','/etc/myapp/base.yaml'] },
    { source: '~/.myapp/base.yaml', object: 'replace' }
    { source: '~/.myapp/email.yaml', base: 'email', depend: '~/.myapp/base.yaml' },
);
```

This will load `/etc/myapp/base.json` (2) or if not found the yaml (3) or ini (4) format of it. The `/etc/myapp/email.yaml` (5) is only loaded if one of the base configs in the same directory is loaded.
If also a local config like `~/.myapp/base.yaml` (7) is found it will replace the global config and also has an dependent `email.yml` (8).

### Preset Data Structure

If you already have the data loaded and want to directly create a DataStore with it as result data structure this is possible using:

```ts
const ds = new DataStore();
ds.data = { name: "preset data structure" };
ds.source = 'preset:config' // optional to set where the data comes from
// no loading is necessary
```

The same is possible in one async call using:

```ts
const ds = await DataStore.data([1, 2, 3], 'config');
```

But you can also initialize and load it in two steps:

```ts
const ds = new DataStore({ data: { name: "preset data structure" }, source: 'preset:config' });
await ds.load();
```

You can also add a source URL to one of this examples later and store it, if you want.

But really useful is this in combination with a list of DataSources. This allows to use default values or overwrite specific elements with code calculated values.

```ts
const ds = new DataStore({ source: "file:config.yml" }, { data: { vehicle: "car", model: "BMW" } });
await ds.load();
```

If you try to load a DataStore, which has no real sources defined, it will throw an error.

## Options

This can be done directly as parameters to the `load()` or `save()` methods or directly as described here. The following options may also be specified.

-   `proxy: string` - A proxy server and port string like 'myserver.de:80'. But an IP Address is also possible.
    You may also give the real server here to not use the normal name resolution.
-   `httpMethod: string` - one of: get, delete, head, options, post, put, patch (default: get)
-   `httpHeader: string[]` - add specific HTTP/HTTPS header lines
-   `httpData: string` - send additional data with HTTP/HTTPS POST or PUT
-   `ignoreError: boolean` - to ignore response code not equal 2xx for HTTP/HTTPS protocol
-   `privateKey: Buffer | string` - used for SFTP and SHELL/SSH protocol
-   `passphrase: string` - used for SFTP and SHELL/SSH protocol
-   `search: string` - defines the search path for postgres to use like `my_schema, public`
-   `compression: string` - compression method to be used (default is detected by file extension)
-   `compressionLevel: number` - gzip compression level 0 (no compression) to 9 (best compression, default)
-   `format: string` - set a specific format handler to be used (default is detected by file extension)
-   `pattern: RegExp` - for text format to be split into pattern matches (Using named groups) for event logs
-   `module: boolean` - use module format in storing as JavaScript, so you can load it using normal `require` or `import`
-   `rootName: string` - define the root element name in formatting as XML
-   `records: boolean` - flag to read the CSV, XLS or XLSX or table data always as with records (only needed with less than 3 columns)
-   `delimiter: string` - character to be used as field separator in CSV data (default: `,`).
-   `quote: string`- character used to quote field values in CSV data (default: `"`).

To set the options on the DataStore directly use:

=== "TypeScript"

    ```ts
    import DataStore from "@alinex/datastore";

    async function config() {
        const ds = new DataStore({
            source: "file:/etc/my-app.json",
            options: { format: "json" }
        });
        await ds.load();
        // go on using the datastore
    }
    ```

=== "JavaScript"

    ```js
    const DataStore = require("@alinex/datastore").default;

    async function config() {
        const ds = new DataStore({
            source: "file:/etc/my-app.json",
            options: { format: "json" }
        });
        await ds.load();
        // go on using the datastore
    }
    ```

## Load

Load a data structure from the defined source URL.

!!! definition

    ```ts
    <DataStore>.load(...input: DataSource[]): Promise<any>
    ```

As described above you may load a data source if it is already defined using:

```js
const ds = new DataStore({ source: "file:/etc/my-app.json" });
await ds.load();
console.log(ds.data);
```

Calling load without parameters will only load the DataStore if not already done. So you may call it also if you don't know if it is already loaded and directly use the returning data structure. To load again use the `reload()` method below.

But you can also set them as needed in the call to `load`:

```js
const ds = new DataStore();
await ds.load({ source: "http://my-server.de/config", options: { format: "json" } });
console.log(ds.data);
```

The above example needs to define the format in the options, because it is not set as file extension in the URL.

### Meta Data

Some of the protocols like HTML and filr will also store meta data gathered while data retrieval beside the data structure:

```js
ds.meta = {
    status: 200,
    statusText: 'OK',
    headers: {
        server: 'nginx',
        date: 'Sat, 26 Oct 2019 19:47:18 GMT',
        'content-type': 'text/plain; charset=utf-8',
        'content-length': '317',
        connection: 'close',
        'cache-control': 'max-age=60, public',
        'content-disposition': 'inline',
        etag: 'W/"24d4aa47151123702a7a3b4e92ff8320"',
        'referrer-policy': 'strict-origin-when-cross-origin, strict-origin-when-cross-origin',
        'set-cookie': [
            'experimentation_subject_id=Ijg4Nzg4MTAzLTFiM2EtNDZhNi05MDY5LWE5NTg5YzlhYzZhNCI%3D--f9891296ec3948ca54191d83b2fbaec0403f52d4; domain=.gitlab.com; path=/; expires=Wed, 26 Oct 2039 19:47:18 -0000; secure'
        ],
        'x-content-type-options': 'nosniff',
        'x-download-options': 'noopen',
        'x-frame-options': 'DENY',
        'x-permitted-cross-domain-policies': 'none',
        'x-request-id': 'mfxS9d29uS1',
        'x-runtime': '0.063831',
        'x-ua-compatible': 'IE=edge',
        'x-xss-protection': '1; mode=block',
        'strict-transport-security': 'max-age=31536000',
        'gitlab-lb': 'fe-16-lb-gprd',
        'gitlab-sv': 'web-23-sv-gprd'
    },
    raw: <buffer>
};
```

If multiple sources are used you will get a list of meta structures equivalent to the list of sources.

### Mapping Information

Only available if multiple sources are used and the mapping flag is set to `true`:

```js
ds.map = {
    __map: ["x.yml", ""],
    name: { __map: ["y.yml", "name"] },
    hair: {
        __map: ["x.yml", "hair"],
        color: { __map: ["x.yml", "hair.color"] },
        cut: { __map: ["y.yml", "hair.cut"] }
    },
    family: {
        "0": { __map: ["x.yml", "family.0"] },
        "1": { __map: ["x.yml", "family.1"] },
        "2": { __map: ["y.yml", "family.0"] },
        __map: ["x.yml", "family"]
    },
    gender: { __map: ["y.yml", "gender"] }
};
```

This mapping has the same structure as the `ds.data` but contains the additional `__map` information with the source URL and the path within the source from which the value comes from.

## Save

Store the current data structure to the defined source URL.

!!! definition

    ```ts
    <DataStore>.save(output?: DataSource): Promise<boolean>
    ```

If you want to transfer to another format or you changed something you may save the data structure again. If it should go in another URI you may give it directly here or set it before.

```js
await ds.save();
console.log(ds.data);
```

Like for loading you can also give a source and options here.

## Reload

Reload the current data structure if it has been changed on disk.

!!! definition

    ```ts
    <DataStore>.reload(time?: number): Promise<boolean>
    ```

If you want to reload the DataStore later, you can easily call the `ds.reload()` method. This will reload if the file was changed (returning true) or not.
If a `time` (number of seconds) setting is given, the reload will not take place within this range.

There are no parameters here, because it is meant to update only. But you can also call `load()` a second time to load it again - but this will always load, also if the source wasn't changed.

{!docs/assets/abbreviations.txt!}
