title: Overview

# Application Programming Interface

First install it as module:

```bash
npm install @alinex/datastore --save
```

## Basic Usage

The DataStore has a class based interface. To use it create an instance and load or save it as you need.

1. load and save or transform between different [protocols and formats](../spec.md).
2. [access methods](access) to read or modify data structures

A complete process may look like:

=== "TypeScript"

    ```ts
    import DataStore from '@alinex/datastore';

    async function config(): any {
        const ds = new DataStore({ source: 'file:/etc/my-app.json' });
        await ds.load(); // load data
        return ds.data;
    }

    config().then(conf => {
        // work with it
    });
    ```

=== "JavaScript"

    ```js
    const DataStore = require('@alinex/datastore').default;

    async function config() {
        const ds = new DataStore({ source: 'file:/etc/my-app.json' });
        await ds.load(); // load data
        return ds.data;
    }

    config().then(conf => {
        // work with it
    });
    ```

Read in the next pages about all the options and see more examples.

## TypeScript

This module is written in TypeScript, and comes with all the definitions included. You can import it directly in TypeScript and get the benefit of static type checking and auto-complete within your IDE.

{!docs/assets/abbreviations.txt!}
