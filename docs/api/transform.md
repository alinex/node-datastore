# Transform

Instead of loading form a persistent store or save to one you can also only use the parsing and formatting. This may be necessary if the source is already loaded.

## Parse

!!! definition

    ```ts
    public async <DataStore>.parse(
        uri: string,
        buffer: Buffer | string,
        options?: Options
    ): Promise<any>
    ```

The URI specifying where the data comes from is necessary to also read the format from it's file extension if no `format` option is given.

## Format

!!! definition

    ```ts
    public async <DataStore>.format(
        uri: string,
        options?: Options
    ): Promise<Buffer>
    ```

As with the `parse()` method the uri and `options.format` used for specifying the correct format.
