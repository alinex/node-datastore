title: Access

# Access and Manipulate Data

The examples below use this data structure:

```js
{ name: 'example',
  null: null,
  boolean: true,
  string: 'test',
  number: 5.6,
  date: '2016-05-10T19:06:36.909Z',
  list: [ 1, 2, 3 ],
  person: { name: 'Alexander Schilling', job: 'Developer' },
  complex: [
      { name: 'Egon' },
      { name: 'Janina' }
  ] }
```

!!! info

    All of the following methods use a string as first parameter.

    The modifying elements need a `path` which is the full way to the selected element, concatenating the object keys or array index numbers using dots. Alternatively using names in square brackets.
    In case of arrays, the index may also be negativ to specify element from the end.

    But the accessing methods `get()` and `has()` supports an greatly extended `filter` string which is basically the same so each `path` can also be used. But see the [filter description](https://alinex.gitlab.io/node-data/filter/) for all the possibilities.

## Has Element

Check if the given element path is defined.

!!! definition

    ```ts
    public <DataStore>.has(
        path: string | number | (string | number)[]
    ): boolean
    ```

This method will check the given path and return a `boolean` value. It's `true` if this element is defined and `false` if not.

```js
ds.has('person.name'); // true
ds.has(['person', 'age']); // false
```

See the [filter documentation](https://alinex.gitlab.io/node-data/filter/) for all the possibilities here.

## Get Element

Get a specific element by path.

!!! definition

    ```ts
    public <DataStore>.get(
        path: string | number | (string | number)[],
        fallback?: any
    ): any
    ```

Use the name or index number (array) to specify the element to retrieve.

```js
ds.get(); // return all data
ds.get('name'); // returns "example"
ds.get('complex.0.name'); // returns "Egon"
```

See the [filter documentation](https://alinex.gitlab.io/node-data/filter/) for all the possibilities here.

## Filter

This is like `get()` but instead of returning the filtered data, it will create a new DataStore with this data within. This can be also used further on.

!!! definition

    ```ts
    public <DataStore>.filter(
        path: string | number | (string | number)[],
        fallback?: any
    ): DataStore
    ```

So it is also possible to concatenate multiple DataStores with the filter method together.

```js
ds.get(); // return all data
// instead of ds.get('complex.0.name') use
ds.filter('complex.0').get('name'); // returns "Egon"
// or in steps
const parts = ds.filter('complex.0');
parts.get('name'); // returns "Egon"
```

See the [filter documentation](https://alinex.gitlab.io/node-data/filter/) for all the possibilities here.

## Coalesce

Get the first defined element from the given path definitions.

!!! definition

    ```ts
    public <DataStore>.coalesce(
        paths: string | number | (string | number)[] | (string | number | (string | number)[])[],
        fallback?: any
    ): any
    ```

This allows to define multiple paths, from which the first found path will be returned:

```js
ds.coalesce(['alias', 'person.alias', ['person', 'name']], 'name');
```

Also a default value can be given at the end to be used if nothing matched.

## Set Element

Set the value of an element by path specification.

!!! definition

    ```ts
    public <DataStore>.set(
        path: string | number | (string | number)[],
        value: any,
        doNotReplace?: boolean
    ): any
    ```

As the get methods return references you may always change this reference's value. But a clearer way is to use the `set` method. This will automatically create new values.

```js
ds.set('person.sex', 'm');
ds.get('person.sex'); // returns "m"
```

If elements on the path are missing they will also be created:

```js
ds.set('person.address.street', 'Allee 5');
```

If the `doNotReplace` flag is added it will only set the value if it is not already defined.

```js
ds.set('person.name', 'unknown', true);
```

## Insert into Array

!!! definition

    ```ts
    public <DataStore>.insert(
        path: string | number | (string | number)[],
        value: any,
        pos?: number
    ): any
    ```

To insert elements in an array the following method can be used (position may be specified):

```js
ds.insert('list', 'm', 1);
```

If the specified element isn't an array, it will be replaced with one and the value inserted as first element.

Negative positions can also be used to position counting from the back.

```text
      [ 0, 1, 2, 3, 4 ]
      ↗     ↗   ↖     ↖
pos: 0     2     -2    5..999
```

## Push to Array

!!! definition

    ```ts
    public <DataStore>.push(
        path: string | number | (string | number)[],
        ...values: any[]
    ): any
    ```

Like `insert` this will work on arrays only and adds all values to the end of the array.

```js
ds.push('lost', 4);
ds.push('lost', 4, 5, 6);
```

As seen above you can push multiple values at once.

    public empty(path: string | number | (string | number)[]): any {

## Empty Element

!!! definition

    ```ts
    public <DataStore>.empty(
        path: string | number | (string | number)[]7
    ): any
    ```

This will keep the specified element but make it an empty array or object (depending on type).

```js
ds.empty('name'); // string is now ''
ds.empty('list'); // array is now []
ds.empty('person'); // object is now {}
```

The following values will be set:

| Type    | Empty Value |
| ------- | ----------- |
| boolean | `false`     |
| number  | `0`         |
| string  | `''`        |
| array   | `[]`        |
| object  | `{}`        |

## Delete Element

!!! definition

    ```ts
    public <DataStore>.delete(
        path: string | number | (string | number)[]
    ): any
    ```

This removes a element from the data structure, it will be `undefined` afterwards.

```js
ds.delete('list.3');
```
