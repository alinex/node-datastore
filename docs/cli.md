title: CLI Usage

# Command Line Usage

The CLI allows to work in two major modes and also can generate a bash completion script:

-   working on a single input source
-   use input as definition to allow multiple sources

    datastore [options] # work with single input
    datastore definition [options] # use input as definition (allow multiple sources)
    datastore bashrc-script # generate completion script

The general options are:

    --version    Show version number                                         [boolean]
    --quiet, -q  only output result                                          [boolean]
    --help       Show help                                                   [boolean]

## Input

The input can be a defined file or if not specified `STDIN` is used as for pipes. If no input file or URI the `--input-format` is always needed because the filename is unknown to the DataStore.
The other options are used to specify how to read this input:

    --input, -i          input path or URI to read from                       [string]
    --input-format       format used to parse input
            [choices: "bson", "coffee", "cson", "csv", "ini", "js", "json", "msgpack",
                                          "properties", "text", "toml", "xml", "yaml"]
    --input-compression  compression method to be used
      [choices: "gzip", "brotli", "bzip2", "lzma", "tar", "tgz", "tbz2", "tlz", "zip"]
    --records            flag to read the CSV always as with records         [boolean]

Protocol specific options:

    --proxy            HTTP method to use for call
                 [choices: "get", "delete", "head", "options", "post", "put", "patch"]
    --http-method      HTTP method to use for call
                 [choices: "get", "delete", "head", "options", "post", "put", "patch"]
    --http-header      send additional HTTP header                             [array]
    --http-data        send additional HTTP POST or PUT data                  [string]
    --ignore-error     ignore HTTP error response codes                      [boolean]
    --sftp-privatekey  private key file                                       [string]
    --sftp-passphrase  passphrase for private key, if needed                  [string]

!!! info

    See the [specifications](spec.md) for details to the supported protocols, compression, archive and file formats.

At last some filter rules may be used to reduce the input result:

    --filter, -f          filter rule to modify data structure                [string]

## Output

The same goes for the output, if no file URI is specified `STDOUT` will be used. If no output file or URI is given and no `--output-format` is defined, JavaScript is used as default.
The other options are used to define the correct format:

    --proxy            HTTP method to use for call
                 [choices: "get", "delete", "head", "options", "post", "put", "patch"]
    --http-method      HTTP method to use for call
                 [choices: "get", "delete", "head", "options", "post", "put", "patch"]
    --http-header      send additional HTTP header                             [array]
    --http-data        send additional HTTP POST or PUT data                  [string]
    --ignore-error     ignore HTTP error response codes                      [boolean]
    --sftp-privatekey  private key file                                       [string]
    --sftp-passphrase  passphrase for private key, if needed                  [string]

With the [filter](https://alinex.gitlab.io/node-data/filter/) rule a specific part can be selected or the data can be slightly manipulated.

```bash
datastore -i test.json -f 'person'
datastore -i test.json -f 'person.age + " years"'
```

## Transform Files

Therefore you simply need the input and output parameters and the DataStore will convert between all of the supported formats.

```bash
datastore -i test.json -o test.yml
```

## Using Definition

As noted on top of the page, you can specify multiple sources to be merged together into one resulting data structure. As this needs a more complex definition as possible using different command arguments it is done in two steps:

1.  the given single source will be loaded as definition -> this makes it the real loading definition
2.  the real loading and merge will take place based on the above definition

```bash
datastore definition -i loader.json -o test.yml
```

Here the loader.json may look like:

```js
[
    { source: '/etc/myapp/base.json' },
    { source: '/etc/myapp/base.yaml', alternate: ['/etc/myapp/base.json'] },
    { source: '/etc/myapp/base.ini', alternate: ['/etc/myapp/base.json','/etc/myapp/base.yaml'] },
    { source: '/etc/myapp/email.yaml', base: 'email', depend: ['/etc/myapp/base.json','/etc/myapp/base.yaml'] },
    { source: '~/.myapp/base.yaml', object: 'replace' }
    { source: '~/.myapp/email.yaml', base: 'email', depend: '~/.myapp/base.yaml' },
]
```

Effectively this is the same as if this definition was given as parameters to the [JavaScript API](api/loader.md).

A simple call using pipes may look like:

```bash
echo '[{"source": "test.yml"}]' | datastore definition --input-format json
```

## Examples

!!! example "Get Token from REST Server"

    ```bash
    datastore -q \
        --input http://localhost:3030/authentication \
        --http-method post \
        --http-header 'Content-Type: application/json' \
        --http-data '{ "strategy": "local", "email": "demo@alinex.de", "password": "demo123" }' \
        --input-format json \
        --filter accessToken \
        --output-format text
    ```

{!docs/assets/abbreviations.txt!}
