# Specifications

The following protocols can be used to access data:

| Protocol Handler                 | Protocol   | Load | Save       | Meta |
| -------------------------------- | ---------- | ---- | ---------- | ---- |
| [file](protocol/file.md)         | file       | yes  | yes        | yes  |
| [sftp](protocol/sftp.md)         | sftp       | yes  | yes        | yes  |
| [ftp](protocol/ftp.md)           | ftp ftps   | yes  | yes        | yes  |
| [http](protocol/http.md)         | http https | yes  | (untested) | yes  |
| [postgres](protocol/postgres.md) | postgres   | yes  | not yet    | no   |
| [shell](protocol/shell.md)       | shell ssh  | yes  | yes        | yes  |

Supported compression formats (optional):

| Compression Handler               | Extension      | Load | Save | Dir |
| --------------------------------- | -------------- | ---- | ---- | --- |
| [gzip](compression/gzip.md)       | .gzip .gz      | yes  | yes  | no  |
| [deflate](compression/deflate.md) | .deflate       | yes  | yes  | no  |
| [brotli](compression/brotli.md)   | .br            | yes  | yes  | no  |
| [bzip2](compression/bzip2.md)     | .bz2           | yes  | yes  | no  |
| [lzma](compression/lzma.md)       | .lzma          | yes  | yes  | no  |
| [tar](compression/tar.md)         | .tar           | yes  | yes  | yes |
| [tgz](compression/tgz.md)         | .tgz .tar.gz   | yes  | yes  | yes |
| [tbz2](compression/tbz2.md)       | .tbz2 .tar.bz2 | yes  | yes  | yes |
| [tlz](compression/tlz.md)         | .tlz .tar.lzma | yes  | yes  | yes |
| [zip](compression/zip.md)         | .zip           | yes  | yes  | yes |

In the moment archive formats which store multiple files are not supported.

And these formats are parsable:

| Format Handler                     | Extension   | Load | Save    |
| ---------------------------------- | ----------- | ---- | ------- |
| [text](format/text.md)             | .txt        | yes  | yes     |
| [binary](format/binary.md)         |             | yes  | yes     |
| [json](format/json.md)             | .json       | yes  | yes     |
| [bson](format/bson.md)             | .bson       | yes  | yes     |
| [msgpack](format/msgpack.md)       | .msgpack    | yes  | yes     |
| [js](format/js.md)                 | .js         | yes  | yes     |
| [cson](format/cson.md)             | .cson       | yes  | yes     |
| [coffee](format/coffee.md)         | .coffee     | yes  | as CSON |
| [yaml](format/yaml.md)             | .yml .yaml  | yes  | yes     |
| [ini](format/ini.md)               | .ini        | yes  | yes     |
| [properties](format/properties.md) | .properties | yes  | yes     |
| [toml](format/toml.md)             | .toml       | yes  | yes     |
| [xml](format/xml.md)               | .xml        | yes  | yes     |
| [html](format/html.md)             | .htm .html  | yes  | yes     |
| [csv](format/csv.md)               | .csv        | yes  | yes     |
| [xls](format/xls.md)               | .xls        | yes  | yes     |
| [xlsx](format/xlsx.md)             | .xlsx       | yes  | yes     |

What to use is specified based on a URI which defines both. Comments are possible in some formats, but will not be imported or written on save.

## URI

The URI is specific to the protocol and file format.

!!! example

    URI: `<protocol>://<path>.<extensions>#<sub-path>`

The `protocol` specifies how to access the persistent storage and extension is used to decide which `format` to use. It will also detect compression and data format from the extensions, which may also be a concatenation of multiple like `.yml.gz`.

The `sub-path` is used within archives to locate the file within. This is also used for the file extension if defined.

Some protocols may have specialties see at example the psql database access.

{!docs/assets/abbreviations.txt!}
