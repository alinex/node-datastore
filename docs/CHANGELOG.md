# Last Changes

## Version 1.16.0 - (12.05.2021)

-   upgrade to ES2019, Node >= 12
-   enhanced debug mode for ftp
-   update packages: bson, msgpack, json, zip, csv, pg, xlsx
-   replace outdated moment.js with luxon
-   refactor format casting

- HotFix 1.16.1 - (18.06.2021) - fix url decoding

## Version 1.15.0 - (02.01.2021)

- new doc theme
- update packages for ssh, ini, html
- move man page creation into core
- update and fix zip

## Version 1.13.0 - (16.06.2020)

- Excel xls and xlsx to read (one table only) and write

- Update 1.13.1 - (11.08.2020)

    - also allow pattern in text parser which match no line
    - add `error.data` on parse error
    - update ftp, cson, csv, tar, pg, bson, xlsx packages

- HotFix 1.13.2 - (10.09.2020) - update packages
- Update 1.13.3 - (11.09.2020) - allow source description for preset data with preset:....
- HotFix 1.13.4 - (12.09.2020) - allow ignoreError in shell
- Update 1.13.5 - (29.09.2020) - fix storing `error.data` on all string sources
- Update 1.13.6 - (09.10.2020) - add time to meta data on loading
- Update 1.13.7 - (23.10.2020) - update packages and doc structure

## Version 1.12.0 - (27.01.2020)

-   csv now with `delimiter` and `quote` option support
-   html tree parser

Update 1.12.1 (15.02.2020) - update package management and ssh, ftp

Update 1.12.2 (02.06.2020)

-   changed default output format to JSON
-   add search path option for postgres protocol
-   upgrade 3rd party modules including new major postgres driver

Update 1.12.3 (05.06.2020) - fix line ending in CSV export

Update 1.12.4 (09.06.2020) - fix date parsing bug occurring for numerical strings

Update 1.12.5 (15.06.2020) - fix to also work with empty result sets in postgres

## Version 1.11.0 - (13.01.2020)

-   file tail optimized
-   adding meta.raw with buffer data
-   adding deflate compression support

## Version 1.10.0 - (22.11.2019)

-   Add binary format returning byte array.

## Version 1.9.1 - (13.11.2019)

-   Add `shell` protocol for local and remote execution
-   Add `tail` option for file protocol
-   support meta data in file protocol
-   Bug fix: return content also if error (with ignoreError)

Bug Fix Version 1.9.2 - (19.11.2019) - switch to request lib to work better on complex rest calls

Bug Fix Version 1.9.3 - (21.11.2019) - allow to change HTTP method, again

## Version 1.8.0 - (31.10.2019)

-   Store HTTP meta data from server under `ds.meta`
-   Added `ds.map` to acquire mapping information if enabled in DataSource settings
-   Added support for pattern matching in text formatter
-   fixed bug in not closing sftp connection after retrieval

## Version 1.7.0 - (13.10.2019)

-   Breaking Change: Option `ignoreResponseCode` was renamed to `ignoreError`
-   Added HTTP options: `httpHeader`, `httpData`
-   Made all HTTP options available through CLI

## Version 1.6.0 - (01.10.2019)

-   Added `httpMethod` to be set for http protocol
-   Added `ignoreResponseCode` option for http protocol
-   Fixed bug in don't using options if input is defined using cli
-   Update packages: data (filter), ftp, sftp, csv

Version 1.6.1 - (08.10.2019)

-   Fix: Add merge result as debug message
-   Update: CSV parser

BugFix 1.6.2 - (11.10.2019)

-   Fix: Not recognizing --output-format
-   Update: FTP protocol handler

## Version 1.5.0 - (28.08.2019)

New shortcut calls:

-   `DataStore.load()` to easily initialize and load data
-   `DataStore.url()` to easily initialize and load single sources in one step
-   `DataStore.data()` to initialize with preset data structure

Fixes:

-   load of data only stopped because no real loading necessary

Fix Version 1.5.1 (01.09.2019)

-   Update packages data filter, protocols sftp, postgres and xml format

Fix Version 1.5.2 (16.09.2019)

-   Update packages data filter, protocols sftp+ftp and xml format

Fix Version 1.5.3 (17.09.2019)

-   Added option `proxy` for http/https handler

## Version 1.4.0 - (15.08.2019)

Extension:

-   updated data module to get depend and alternate support in merging
-   added support for multi file in CLI using definition data structures

Fixes:

-   don't fail if one of multiple files are not found

## Version 1.3.0 - (6.08.2019)

The biggest change is the added support for multiple sources.

Breaking Changes:

-   constructor now only allows one or multiple DataSource structures which may be a simple source string, options or alternatively a direct data structure
-   allow to set merge definitions beside the data source
-   same changes for load and save methods
-   set/get source is no longer available

Fixes:

-   `ds.load()` without source will only load if not already done
-   method `filter()` was extracted into [data module](https://alinex.gitlab.io/node-data/filter/)

Hotfix 1.3.1 - fix type DataSource export

## Version 1.2.0 - (22.06.2019)

New:

-   added `ds.filter(string)` which will work like `get()` but return a new DataStore with the results instead of directly returning them
-   added postgres protocol to read from database
-   added plain text formatter

Extended:

-   `new DataStore(data)` can now directly create a datastore with a preset data structure
-   the http handler will auto detect format based on mime-type if not defined

Hotfix 1.2.1 - (23.06.2019)

-   `new DataStore(data, true)` now uses the second parameter to work with any data structure

Update 1.2.2 - (13.07.2019)

-   Internal restructure using retry from core module
-   replace `request` package with `axios`
-   export formats and compressions definition

## Version 1.1.0 - (17.05.2019)

-   add time range in reload() call

Hotfix 1.1.1 - (19.05.2019)

-   fix `get()` on empty string in root returned undefined

Hotfix 1.1.2 - (27.05.2019)

-   fix usage of paths in brackets like '[xxx],yyy[zzz]' in change methods like `set()`
-   update serialize-to-js with internal restructuring

## Version 1.0.0 - (12.05.2019)

-   add new rule engine in get() with filter
-   filter add negative selections
-   filter add combine and alternative syntax
-   filter add function handling
-   filter add list concat, mathematical operators and grouping
-   remove coalesce() method and use filter in has()
-   allow filter within cli call

Hotfix 1.0.1 - (13.05.2019)

-   allow `get()` and `has()` without arguments

## Version 0.7.0 (29.04.2019)

-   support FTP and FTPS
-   added archive support using tar
-   support for .tar.gz archives
-   all options available through CLI
-   add support for zip compression
-   hash part not needing for single file archives in reading

Hotfix 0.7.1 (30.04.2019)

-   fix cli calls
-   support for bzip2 and tar-bzip2 compression
-   support for lzip and tar-lzip compression

Hotfix 0.7.2 (01.05.2019)

-   reduce symbol imports
-   update code style
-   add support for `.mjs` and `.ts` extensions

Hotfix 0.7.3 (03.05.2019)

-   fix access methods with empty path
-   fix reload if changed using access methods
-   add inline doc tags

## Version 0.6.0 (26.04.2019)

-   support HTTP and HTTPS retrieval
-   fixes in formatters with Date handling
-   full support in all formatters, also for arrays
-   optimized debugging

## Version 0.5.0 (19.04.2019)

-   add sftp support
-   add compression support using gzip and brotli

## Version 0.4.0 (17.04.2019)

-   CLI usage
-   Options: format

## Version 0.3.0 (15.04.2019)

-   adding binary bson, msgpack formats
-   adding js, cson, coffee, ini, properties, toml, xml formatter
-   adding csv formatter

## Version 0.2.0 (12.04.2019)

-   extract by path
-   access and manipulate structure

## Version 0.1.0 (0t.04.019)

-   class based access
-   load json
-   write json

{!docs/assets/abbreviations.txt!}
