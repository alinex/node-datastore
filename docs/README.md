# Alinex Data Store

![datastore icon](https://assets.gitlab-static.net/uploads/-/system/project/avatar/11662964/datastore-icon.png){: .right .icon}

The data store is a in memory store for small to medium sized data structures. It allows you to read, work and write in different formats and therefore also to transform between them.

Big Features:

-   read, write and transform data structures
-   methods to access and work with data structures
-   9 protocols supported (file, sftp, HTTP, https...)
-   17 file formats supported
-   10 compression or archive formats are also supported
-   integrated access filter language
-   multi source loading for single structure

See more on the [specification page](spec.md).

## Usage

It can be used from [command line](cli.md) but also as [module](api/README.md) in other NodeJS scripts.

The main use cases are:

-   reading configuration files
-   pre-processing configuration into other format to easily import later
-   communicating with REST services
-   data provider for the [Validator](https://alinex.gitlab.io/node-validator) module since version 3

Personally I seldom use it standalone and in most times use the additional checking from the [Validator](https://alinex.gitlab.io/node-validator). See also the examples there.

## Debugging

To see some more information you may enable the debug logging using the environment variable `DEBBUG=datastore*`. The following finer debug elements are alternatively usable:

-   `datastore:protocol:*` - output uri accessed by specific handler
-   `datastore:details` - output a lot of verbose information
-   `datastore:compression` - output size changes
-   `datastore:compression:*` - output uri processed through specific handler
-   `datastore:format` - output parsed data
-   `datastore:format:*` - output uri processed through specific handler
-   `datastore` - output general problems like ignore because source not found
-   `datastore*` - output all

A lot more may be available using the additional core debugging of NodeJS using `NODE_DEBUG=xxx`. If there is something available the `datastore:details` logging will inform you about it.

## Module Usage

Because of the big list of supported alternatives this module depends on a lot of other modules but only the ones really used are loaded on demand.

Only the following are loaded always:

-   @alinex/core
-   @alinex/data
-   debug
-   object-path

All others are only loaded at the time the specific format is used. But it will install a bigger collection of modules on your system to be prepared if specific formats are used.

## Chapters

Read all about the [DataStore](https://alinex.gitlab.io/node-datastore) in the chapters:

-   [API](api) - How to use it in your own script.
-   [Specifications](spec) - Detailed format, protocol and file specifications.
-   [Extending](dev) - Structure of this module for those extending it.

## Support

I don't give any paid support but you may create [GitLab Issues](https://gitlab.com/alinex/node-datastore/-/issues):

-   Bug Reports or Feature: Please make sure to give as much information as possible. And explain how the system should behave in your opinion.
-   Code Fixing or Extending: If you help to develop this package I am thankful. Develop in a fork and make a merge request after done. I will have a look at it.
-   Should anybody be willing to join the core team feel free to ask, too.
-   Any other comment or discussion as far as it is on this package is also kindly accepted.

Please use for all of them the [GitLab Issues](https://gitlab.com/alinex/node-datastore/-/issues) which only requires you to register with a free account.

{!docs/assets/abbreviations.txt!}
