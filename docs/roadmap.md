# Roadmap

It's not a clear roadmap here, but more a list of possible extension ways divided into thematic groups. If you need a specific extension or change contact me. Or send me a pull request.

- [X] ~~*ftp problems*~~ [2021-05-11]
- [X] ~~*sftp problems*~~ [2021-05-11]
- [X] ~~*replace moment.js*~~ [2021-05-12]
- [X] ~~*support csv with delimiter ':' (for apache status)*~~ [2021-05-12]

- [ ] remove type definition for Blob (needed for js-zip V3.4.0/3.5.0)

## Add Protocols

-   epub extraction

Web Services:

-   REST support with store + test

Databases:

-   mysql like postgres
-   mongodb <https://www.iana.org/assignments/uri-schemes/prov/mongodb>
-   redis <https://www.iana.org/assignments/uri-schemes/prov/redis>
-   postgres: save as list or record - howto delete old???

Code Repositories:

-   git <https://www.iana.org/assignments/uri-schemes/prov/git>
-   svn <https://www.iana.org/assignments/uri-schemes/prov/svn>

Without mounts:

-   smb <https://www.iana.org/assignments/uri-schemes/prov/smb>
-   nfs <https://github.com/scality/node-nfsc>

## Multiple sources

-   store multi using merge.map and diff

{!docs/assets/abbreviations.txt!}
