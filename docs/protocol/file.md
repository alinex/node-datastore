title: FILE

# File Protocol Handler

The file protocol is the simplest. It allows to read and write from local or locally mounted devices using a file path.

This handler is used for the following protocols: `file`

!!! example

    Pattern: `file://<path>` <br>
    Linux: `file:///etc/my-app/config.json`

The path has to be absolute like shown above.

The following options are possible:

-   `tail` - only read the last number of lines from a text file

## Meta Data

The file and file stats are stored as meta data `ds.meta`.

```js
{
  path: '/home/alex/code/node-datastore/test/data/example.log',
  stats: Stats {
    dev: 2051,
    mode: 33204,
    nlink: 1,
    uid: 1000,
    gid: 1000,
    rdev: 0,
    blksize: 4096,
    ino: 16417975,
    size: 17,
    blocks: 8,
    atimeMs: 1572794838988.7334,
    mtimeMs: 1572794798644.053,
    ctimeMs: 1572794798644.053,
    birthtimeMs: 1572794787399.8635,
    atime: 2019-11-03T15:27:18.989Z,
    mtime: 2019-11-03T15:26:38.644Z,
    ctime: 2019-11-03T15:26:38.644Z,
    birthtime: 2019-11-03T15:26:27.400Z
  }
}
```

{!docs/assets/abbreviations.txt!}
