title: PostgreSQL

# PostgreSQL Database Handler

This protocol will directly load from a postgres database. As the data is table the same operations like on CSV format will take place. But here the parsing and reformatting of the value will be done directly in the protocol handler.

!!! example

    Pattern: `postgres://<username>:<password>@<host>:<port>/<database>#<sql>` <br>
    With password: `postgres://alex:mypass4u73@localhost:5432/test#SELECT * FROM address` <br>

The following options are possible:

-   `search: string` - defines the search path to use like `my_schema, public`
-   `records: boolean` - set to `true` for record format, see below

The result will already be formatted in the database and put through the formatter as JSON without any change. So with this protocol no compressor or formatter can be selected.

!!! hint

    **Complex Database Statements**

    Also complexer queries with joins and subselects or dblink calls may be possible. But to keep it simple and easy to maintain better make a database view or function which holds the complexity and only include the simple call to it in the DataStore source.

For stability reasons the connection to the database will be tried to establish 5 times within 2.5 seconds before giving up.

The parsing of the table is like the [CSV Format](../format/csv.md) with a list and record format.
A table with two columns is interpreted as name/value list and will be formatted as one structure with the name as the path in the structure and it's value. If more values are present or the `records` option is set each row is presented as an individual object within a list.

{!docs/assets/abbreviations.txt!}
