title: HTTP/S

# HTTP and HTTPS Handler

This allows you to get the data structure from a Webserver or REST service.

This handler is used for the following protocols: `http`, `https`

!!! example

    Pattern: `[http|https]://[<user>:<passwd>@]<server>:<port>/<path>?<query>` <br>
    Example: `https://my-server/my-app/config.json`<br/>
    Basic Auth: `https://alex:test123@my-server/my-app/config.json`

If no format is defined using the options, it will be auto detected based on the returning mime-type.

The following specific options are possible:

-   `proxy` - A proxy server and port string like 'myserver.de:80'. But an IP Address is also possible.
    You may also give the real server here to not use the normal name resolution.
-   `httpMethod: string` - one of: get, delete, head, options, post, put, patch (default: get)
-   `httpHeader: string[]` - add specific HTTP/HTTPS header lines
-   `httpData: string` - send additional data with HTTP/HTTPS POST or PUT
-   `ignoreError: boolean` - to ignore response code not equal 2xx for HTTP/HTTPS protocol

## Meta Data

The status and server response headers are stored as meta data `ds.meta`.

```js
{ status: 200,
  statusText: 'OK',
  headers: {
    server: 'nginx',
    date: 'Sat, 26 Oct 2019 19:47:18 GMT',
    'content-type': 'text/plain; charset=utf-8',
    'content-length': '317',
    connection: 'close',
    'cache-control': 'max-age=60, public',
    'content-disposition': 'inline',
    etag: 'W/"24d4aa47151123702a7a3b4e92ff8320"',
    'referrer-policy': 'strict-origin-when-cross-origin, strict-origin-when-cross-origin',
    'set-cookie': [
      'experimentation_subject_id=Ijg4Nzg4MTAzLTFiM2EtNDZhNi05MDY5LWE5NTg5YzlhYzZhNCI%3D--f9891296ec3948ca54191d83b2fbaec0403f52d4; domain=.gitlab.com; path=/; expires=Wed, 26 Oct 2039 19:47:18 -0000; secure'
    ],
    'x-content-type-options': 'nosniff',
    'x-download-options': 'noopen',
    'x-frame-options': 'DENY',
    'x-permitted-cross-domain-policies': 'none',
    'x-request-id': 'mfxS9d29uS1',
    'x-runtime': '0.063831',
    'x-ua-compatible': 'IE=edge',
    'x-xss-protection': '1; mode=block',
    'strict-transport-security': 'max-age=31536000',
    'gitlab-lb': 'fe-16-lb-gprd',
    'gitlab-sv': 'web-23-sv-gprd'
  }
}
```

## Debugging

If something went wrong you may get more information by showing the verbose protocol transfer using `DEBUG=datastore:*`:

      datastore:protocol:http loading /alinex/node-datastore/raw/master/test/data/example.json +2s
    REQUEST { method: 'GET',
      url:
       'https://gitlab.com/alinex/node-datastore/raw/master/test/data/example.json',
      headers: { accept: 'application/json;q=0.9, */*;q=0.8' },
      callback: [Function] }
    REQUEST make request https://gitlab.com/alinex/node-datastore/raw/master/test/data/example.json
    REQUEST onRequestResponse https://gitlab.com/alinex/node-datastore/raw/master/test/data/example.json 200 { server: 'nginx',
      date: 'Mon, 29 Apr 2019 19:24:41 GMT',
      'content-type': 'text/plain; charset=utf-8',
      'content-length': '317',
      connection: 'close',
      'cache-control': 'max-age=60, public',
      'content-disposition': 'inline',
      etag: 'W/"24d4aa47151123702a7a3b4e92ff8320"',
      'x-content-type-options': 'nosniff',
      'x-frame-options': 'DENY',
      'x-request-id': 'qPpHwQKGem3',
      'x-runtime': '0.078344',
      'x-ua-compatible': 'IE=edge',
      'x-xss-protection': '1; mode=block',
      'strict-transport-security': 'max-age=31536000',
      'referrer-policy': 'strict-origin-when-cross-origin',
      'content-security-policy':
       "object-src 'none'; worker-src https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net https://gitlab.com blob:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net https://www.google.com/recaptcha/ https://www.recaptcha.net/ https://www.gstatic.com/recaptcha/ https://apis.google.com; style-src 'self' 'unsafe-inline' https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net; img-src * data: blob:; frame-src 'self' https://www.google.com/recaptcha/ https://www.recaptcha.net/ https://content.googleapis.com https://content-compute.googleapis.com https://content-cloudbilling.googleapis.com https://content-cloudresourcemanager.googleapis.com https://*.codesandbox.io; frame-ancestors 'self'; connect-src 'self' https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net wss://gitlab.com https://sentry.gitlab.net https://customers.gitlab.com https://snowplow.trx.gitlab.net" }
    REQUEST reading response's body
    REQUEST finish init function https://gitlab.com/alinex/node-datastore/raw/master/test/data/example.json
    REQUEST response end https://gitlab.com/alinex/node-datastore/raw/master/test/data/example.json 200 { server: 'nginx',
      date: 'Mon, 29 Apr 2019 19:24:41 GMT',
      'content-type': 'text/plain; charset=utf-8',
      'content-length': '317',
      connection: 'close',
      'cache-control': 'max-age=60, public',
      'content-disposition': 'inline',
      etag: 'W/"24d4aa47151123702a7a3b4e92ff8320"',
      'x-content-type-options': 'nosniff',
      'x-frame-options': 'DENY',
      'x-request-id': 'qPpHwQKGem3',
      'x-runtime': '0.078344',
      'x-ua-compatible': 'IE=edge',
      'x-xss-protection': '1; mode=block',
      'strict-transport-security': 'max-age=31536000',
      'referrer-policy': 'strict-origin-when-cross-origin',
      'content-security-policy':
       "object-src 'none'; worker-src https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net https://gitlab.com blob:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net https://www.google.com/recaptcha/ https://www.recaptcha.net/ https://www.gstatic.com/recaptcha/ https://apis.google.com; style-src 'self' 'unsafe-inline' https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net; img-src * data: blob:; frame-src 'self' https://www.google.com/recaptcha/ https://www.recaptcha.net/ https://content.googleapis.com https://content-compute.googleapis.com https://content-cloudbilling.googleapis.com https://content-cloudresourcemanager.googleapis.com https://*.codesandbox.io; frame-ancestors 'self'; connect-src 'self' https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net wss://gitlab.com https://sentry.gitlab.net https://customers.gitlab.com https://snowplow.trx.gitlab.net" }
    REQUEST end event https://gitlab.com/alinex/node-datastore/raw/master/test/data/example.json
    REQUEST has body https://gitlab.com/alinex/node-datastore/raw/master/test/data/example.json 317
    REQUEST emitting complete https://gitlab.com/alinex/node-datastore/raw/master/test/data/example.json

To get a further step deeper, set environment: `NODE_DEBUG=http,https`.

{!docs/assets/abbreviations.txt!}
