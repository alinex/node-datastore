title: SHELL

# Shell Execution Protocol Handler

This protocol will call a command locally or remotely (using ssh from local shell) to execute a command and read data from `STDOUT`.

This handler is used for the following protocols: `shell`, `ssh`

!!! example

    Pattern: `shell://<username>:<password>@<host><path>#<command>` <br>
    Local: `shell:///etc/my-app#cat config.json`
    Remote: `ssh://alex@my-server/etc/my-app#cat config.json`

The path has to be absolute like shown above.

The following options are possible:

-   `privateKey` - Buffer or string that contains a private key for either key-based or hostbased user authentication
-   `passphrase` - for an encrypted private key, this is the passphrase used to decrypt it

## Meta Data

The command stores additional information as meta data `ds.meta` (only on local calls).

```js
{
  command: 'ssh alex@localhost cd /home/alex/code/node-datastore/test/mocha/protocol \\&\\& cat ../../data/example.json',
  exitCode: 0,
  stderr: '',
  all: undefined,
  failed: false,
  timedOut: false,
  isCanceled: false,
  killed: false
}
```

## Writing

To write the data is send as string through `STDIN` to the process.

```ts
const load1 = `shell://${__dirname}#cat   ../../data/example.json`;
const store = `shell://${__dirname}#cat > ../../data/example.json`;
```

The above example shows how to read and store.

{!docs/assets/abbreviations.txt!}
