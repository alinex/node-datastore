title: FTP/S

# FTP and FTPS Handler

It supports explicit FTPS over TLS, Passive Mode over IPv6

!!! info

    Prefer alternative transfer protocols like [HTTPS](http.md) or [SFTP](sftp.md) (SSH). Use this protocol only when you have no choice and need to use FTP. Try to use FTPS whenever possible, FTP alone does not provide any security.

This handler is used for the following protocols: `ftp`, `ftps`

!!! example

    Pattern: `[ftp|ftps]://[<user>:<passwd>@]<server>:<port>/<path>` <br>
    Example: `ftp://ftp.my-server/my-app/config.json`<br/>
    Basic Auth: `ftp://alex:test123@ftp.my-server/my-app/config.json`

## Meta Data

The file and file stats are stored as meta data `ds.meta`.

```js
{
  path: 'ftp://user:pass@localhost/home/alex/code/node-datastore/test/data/example.log',
  stats: {
    modifyTime: 1566868566000
  }
};
```

## Debugging

If something went wrong you may get more information by showing the verbose protocol transfer using `DEBUG=datastore:*`:

    datastore:protocol:ftp loading ftp://alex@localhost/home/alex/code/node-datastore/test/data/  example.json +49ms
    datastore:details Connected to 127.0.0.1:21 +3ms
    datastore:details < 220 ProFTPD 1.3.5e Server (Debian) [::ffff:127.0.0.1] +184ms
    datastore:details Login security: No encryption +0ms
    datastore:details > USER alex +0ms
    datastore:details < 331 Password required for alex +1ms
    datastore:details > PASS ### +0ms
    datastore:details < 230 User alex logged in +8ms
    datastore:details > TYPE I +0ms
    datastore:details < 200 Type set to I +0ms
    datastore:details > STRU F +0ms
    datastore:details < 200 Structure set to F +1ms
    datastore:details Trying to find optimal transfer strategy... +0ms
    datastore:details > EPSV +0ms
    datastore:details < 229 Entering Extended Passive Mode (|||10345|) +0ms
    datastore:details Optimal transfer strategy found. +1ms
    datastore:details > RETR /home/alex/code/node-datastore/test/data/example.json +0ms
    datastore:details < 150 Opening BINARY mode data connection for /home/alex/code/node-datastore/test/  data/example.json (317 bytes) +0ms
    datastore:details Downloading from 127.0.0.1:10345 (No encryption) +0ms
    datastore:details < 226 Transfer complete +1ms

    datastore:protocol:ftp storing to ftp://alex@localhost/home/alex/code/node-datastore/test/data/   example.json +197ms
    datastore:details Connected to 127.0.0.1:21 +3ms
    datastore:details < 220 ProFTPD 1.3.5e Server (Debian) [::ffff:127.0.0.1] +200ms
    datastore:details Login security: No encryption +0ms
    datastore:details > USER alex +0ms
    datastore:details < 331 Password required for alex +1ms
    datastore:details > PASS ### +0ms
    datastore:details < 230 User alex logged in +8ms
    datastore:details > TYPE I +0ms
    datastore:details < 200 Type set to I +0ms
    datastore:details > STRU F +0ms
    datastore:details < 200 Structure set to F +1ms
    datastore:details Trying to find optimal transfer strategy... +0ms
    datastore:details > EPSV +0ms
    datastore:details < 229 Entering Extended Passive Mode (|||10232|) +1ms
    datastore:details Optimal transfer strategy found. +0ms
    datastore:details > STOR /home/alex/code/node-datastore/test/data/example.json +0ms
    datastore:details < 150 Opening BINARY mode data connection for /home/alex/code/node-datastore/test/  data/example.json +1ms
    datastore:details Uploading to 127.0.0.1:10232 (No encryption) +0ms
    datastore:details < 226 Transfer complete +3ms

{!docs/assets/abbreviations.txt!}
