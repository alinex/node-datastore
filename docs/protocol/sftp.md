title: SFTP

# SSH File Transfer Protocol Handler

The sftp protocol is based on a ssh connection to a host and allows to access files after login using username and password or key file.

This handler is used for the following protocols: `sftp`

!!! example

    Pattern: `sftp://<username>:<password>@<host>:<port><path>` <br>
    With password: `sftp://alex:mypass4u73@localhost/etc/my-app/config.json` <br>
    With key: `sftp://alex@localhost/etc/my-app/config.json`

The path has to be absolute like shown above.

The following options are possible:

-   `privateKey` - Buffer or string that contains a private key for either key-based or hostbased user authentication
-   `passphrase` - for an encrypted private key, this is the passphrase used to decrypt it

## Meta Data

The file and file stats are stored as meta data `ds.meta`.

```js
{
  path: 'sftp://user:pass@localhost/home/alex/code/node-datastore/test/data/example.log',
  stats: {
    mode: 33279, // integer representing type and permissions
    uid: 1000, // user ID
    gid: 985, // group ID
    size: 5, // file size
    accessTime: 1566868566000, // Last access time. milliseconds
    modifyTime: 1566868566000, // last modify time. milliseconds
    isDirectory: false, // true if object is a directory
    isFile: true, // true if object is a file
    isBlockDevice: false, // true if object is a block device
    isCharacterDevice: false, // true if object is a character device
    isSymbolicLink: false, // true if object is a symbolic link
    isFIFO: false, // true if object is a FIFO
    isSocket: false // true if object is a socket
  }
};
```

## Debugging

If something went wrong you may get more information by showing the verbose protocol transfer using `DEBUG=datastore:*`:

    datastore:protocol:sftp loading sftp://alex@localhost/home/alex/code/node-datastore/test/data/    example.json +224ms
    datastore:details DEBUG: Local ident: 'SSH-2.0-ssh2js0.4.2' +0ms
    datastore:details DEBUG: Client: Trying localhost on port 22 ... +0ms
    datastore:details DEBUG: Client: Connected +1ms
    datastore:details DEBUG: Parser: IN_INIT +5ms
    datastore:details DEBUG: Parser: IN_GREETING +0ms
    datastore:details DEBUG: Parser: IN_HEADER +0ms
    datastore:details DEBUG: Remote ident: 'SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.3' +0ms
    datastore:details DEBUG: Outgoing: Writing KEXINIT +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 8) +1ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: pktLen:1076,padLen:6,remainLen:1072 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: KEXINIT +0ms
    datastore:details DEBUG: Comparing KEXINITs ... +0ms
    datastore:details DEBUG: (local) KEX algorithms: ecdh-sha2-nistp256,ecdh-sha2-nistp384,   ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group14-sha1 +0ms
    datastore:details DEBUG: (remote) KEX algorithms: curve25519-sha256,curve25519-sha256@libssh.org, ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,   diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group14-sha256, diffie-hellman-group14-sha1 +0ms
    datastore:details DEBUG: KEX algorithm: ecdh-sha2-nistp256 +0ms
    datastore:details DEBUG: (local) Host key formats: ssh-rsa,ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,   ecdsa-sha2-nistp521 +0ms
    datastore:details DEBUG: (remote) Host key formats: ssh-rsa,rsa-sha2-512,rsa-sha2-256,    ecdsa-sha2-nistp256,ssh-ed25519 +0ms
    datastore:details DEBUG: Host key format: ssh-rsa +0ms
    datastore:details DEBUG: (local) Client->Server ciphers: aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm, aes128-gcm@openssh.com,aes256-gcm,aes256-gcm@openssh.com +0ms
    datastore:details DEBUG: (remote) Client->Server ciphers: chacha20-poly1305@openssh.com,aes128-ctr,   aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com +0ms
    datastore:details DEBUG: Client->Server Cipher: aes128-ctr +0ms
    datastore:details DEBUG: (local) Server->Client ciphers: aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm, aes128-gcm@openssh.com,aes256-gcm,aes256-gcm@openssh.com +0ms
    datastore:details DEBUG: (remote) Server->Client ciphers: chacha20-poly1305@openssh.com,aes128-ctr,   aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com +0ms
    datastore:details DEBUG: Server->Client Cipher: aes128-ctr +0ms
    datastore:details DEBUG: (local) Client->Server HMAC algorithms: hmac-sha2-256,hmac-sha2-512,hmac-sha1    +0ms
    datastore:details DEBUG: (remote) Client->Server HMAC algorithms: umac-64-etm@openssh.com,    umac-128-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,   hmac-sha1-etm@openssh.com,umac-64@openssh.com,umac-128@openssh.com,hmac-sha2-256,hmac-sha2-512,    hmac-sha1 +0ms
    datastore:details DEBUG: Client->Server HMAC algorithm: hmac-sha2-256 +0ms
    datastore:details DEBUG: (local) Server->Client HMAC algorithms: hmac-sha2-256,hmac-sha2-512,hmac-sha1    +0ms
    datastore:details DEBUG: (remote) Server->Client HMAC algorithms: umac-64-etm@openssh.com,    umac-128-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,   hmac-sha1-etm@openssh.com,umac-64@openssh.com,umac-128@openssh.com,hmac-sha2-256,hmac-sha2-512,    hmac-sha1 +0ms
    datastore:details DEBUG: Server->Client HMAC algorithm: hmac-sha2-256 +0ms
    datastore:details DEBUG: (local) Client->Server compression algorithms: none,zlib@openssh.com,zlib +0ms
    datastore:details DEBUG: (remote) Client->Server compression algorithms: none,zlib@openssh.com +0ms
    datastore:details DEBUG: Client->Server compression algorithm: none +0ms
    datastore:details DEBUG: (local) Server->Client compression algorithms: none,zlib@openssh.com,zlib +0ms
    datastore:details DEBUG: (remote) Server->Client compression algorithms: none,zlib@openssh.com +0ms
    datastore:details DEBUG: Server->Client compression algorithm: none +0ms
    datastore:details DEBUG: Outgoing: Writing KEXECDH_INIT +1ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 8) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +1ms
    datastore:details DEBUG: Parser: pktLen:636,padLen:7,remainLen:632 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: KEXECDH_REPLY +0ms
    datastore:details DEBUG: Checking host key format +0ms
    datastore:details DEBUG: Checking signature format +0ms
    datastore:details DEBUG: Verifying host fingerprint +0ms
    datastore:details DEBUG: Host accepted by default (no verification) +0ms
    datastore:details DEBUG: Verifying signature +1ms
    datastore:details DEBUG: Outgoing: Writing NEWKEYS +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 8) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: pktLen:12,padLen:10,remainLen:8 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: NEWKEYS +0ms
    datastore:details DEBUG: Outgoing: Writing SERVICE_REQUEST (ssh-userauth) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +1ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:28,padLen:10,remainLen:16 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: SERVICE_ACCEPT +0ms
    datastore:details DEBUG: Outgoing: Writing USERAUTH_REQUEST (none) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +1ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:44,padLen:19,remainLen:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: USERAUTH_FAILURE +0ms
    datastore:details DEBUG: Client: none auth failed +0ms
    datastore:details DEBUG: Outgoing: Writing USERAUTH_REQUEST (password) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +7ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:12,padLen:10,remainLen:0 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: USERAUTH_SUCCESS +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_OPEN (0, session) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +1ms
    datastore:details DEBUG: Parser: IN_PACKET +183ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:492,padLen:16,remainLen:480 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: GLOBAL_REQUEST (hostkeys-00@openssh.com)     +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +43ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:28,padLen:10,remainLen:16 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +1ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_OPEN_CONFIRMATION +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_REQUEST (0, subsystem: sftp) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:28,padLen:18,remainLen:16 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +1ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_WINDOW_ADJUST (0, 2097152) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:12,padLen:6,remainLen:0 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_SUCCESS (0) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_DATA (0) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +3ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:172,padLen:8,remainLen:160 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_DATA (0) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG[SFTP]: Outgoing: Writing OPEN +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_DATA (0) +1ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:44,padLen:17,remainLen:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_DATA (0) +0ms
    datastore:details DEBUG[SFTP]: Parser: Response: HANDLE +0ms
    datastore:details DEBUG[SFTP]: Outgoing: Writing READ +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_DATA (0) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +1ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:348,padLen:8,remainLen:336 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_DATA (0) +0ms
    datastore:details DEBUG[SFTP]: Parser: Response: DATA +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG[SFTP]: Outgoing: Writing READ +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_DATA (0) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +1ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:60,padLen:18,remainLen:48 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_DATA (0) +0ms
    datastore:details DEBUG[SFTP]: Parser: Response: STATUS +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG[SFTP]: Outgoing: Writing CLOSE +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_DATA (0) +0ms

    datastore:protocol:sftp storing to sftp://alex@localhost/home/alex/code/node-datastore/test/data/ example.json +255ms
    datastore:details DEBUG: Local ident: 'SSH-2.0-ssh2js0.4.2' +1ms
    datastore:details DEBUG: Client: Trying localhost on port 22 ... +0ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:44,padLen:6,remainLen:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_DATA (0) +0ms
    datastore:details DEBUG[SFTP]: Parser: Response: STATUS +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Client: Connected +0ms
    datastore:details DEBUG: Parser: IN_INIT +6ms
    datastore:details DEBUG: Parser: IN_GREETING +0ms
    datastore:details DEBUG: Parser: IN_HEADER +0ms
    datastore:details DEBUG: Remote ident: 'SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.3' +0ms
    datastore:details DEBUG: Outgoing: Writing KEXINIT +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 8) +1ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: pktLen:1076,padLen:6,remainLen:1072 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: KEXINIT +0ms
    datastore:details DEBUG: Comparing KEXINITs ... +0ms
    datastore:details DEBUG: (local) KEX algorithms: ecdh-sha2-nistp256,ecdh-sha2-nistp384,   ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group14-sha1 +0ms
    datastore:details DEBUG: (remote) KEX algorithms: curve25519-sha256,curve25519-sha256@libssh.org, ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,   diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group14-sha256, diffie-hellman-group14-sha1 +0ms
    datastore:details DEBUG: KEX algorithm: ecdh-sha2-nistp256 +0ms
    datastore:details DEBUG: (local) Host key formats: ssh-rsa,ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,   ecdsa-sha2-nistp521 +0ms
    datastore:details DEBUG: (remote) Host key formats: ssh-rsa,rsa-sha2-512,rsa-sha2-256,    ecdsa-sha2-nistp256,ssh-ed25519 +0ms
    datastore:details DEBUG: Host key format: ssh-rsa +0ms
    datastore:details DEBUG: (local) Client->Server ciphers: aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm, aes128-gcm@openssh.com,aes256-gcm,aes256-gcm@openssh.com +0ms
    datastore:details DEBUG: (remote) Client->Server ciphers: chacha20-poly1305@openssh.com,aes128-ctr,   aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com +0ms
    datastore:details DEBUG: Client->Server Cipher: aes128-ctr +0ms
    datastore:details DEBUG: (local) Server->Client ciphers: aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm, aes128-gcm@openssh.com,aes256-gcm,aes256-gcm@openssh.com +0ms
    datastore:details DEBUG: (remote) Server->Client ciphers: chacha20-poly1305@openssh.com,aes128-ctr,   aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com +0ms
    datastore:details DEBUG: Server->Client Cipher: aes128-ctr +0ms
    datastore:details DEBUG: (local) Client->Server HMAC algorithms: hmac-sha2-256,hmac-sha2-512,hmac-sha1    +0ms
    datastore:details DEBUG: (remote) Client->Server HMAC algorithms: umac-64-etm@openssh.com,    umac-128-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,   hmac-sha1-etm@openssh.com,umac-64@openssh.com,umac-128@openssh.com,hmac-sha2-256,hmac-sha2-512,    hmac-sha1 +0ms
    datastore:details DEBUG: Client->Server HMAC algorithm: hmac-sha2-256 +0ms
    datastore:details DEBUG: (local) Server->Client HMAC algorithms: hmac-sha2-256,hmac-sha2-512,hmac-sha1    +0ms
    datastore:details DEBUG: (remote) Server->Client HMAC algorithms: umac-64-etm@openssh.com,    umac-128-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,   hmac-sha1-etm@openssh.com,umac-64@openssh.com,umac-128@openssh.com,hmac-sha2-256,hmac-sha2-512,    hmac-sha1 +0ms
    datastore:details DEBUG: Server->Client HMAC algorithm: hmac-sha2-256 +0ms
    datastore:details DEBUG: (local) Client->Server compression algorithms: none,zlib@openssh.com,zlib +0ms
    datastore:details DEBUG: (remote) Client->Server compression algorithms: none,zlib@openssh.com +0ms
    datastore:details DEBUG: Client->Server compression algorithm: none +0ms
    datastore:details DEBUG: (local) Server->Client compression algorithms: none,zlib@openssh.com,zlib +0ms
    datastore:details DEBUG: (remote) Server->Client compression algorithms: none,zlib@openssh.com +0ms
    datastore:details DEBUG: Server->Client compression algorithm: none +0ms
    datastore:details DEBUG: Outgoing: Writing KEXECDH_INIT +1ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 8) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +1ms
    datastore:details DEBUG: Parser: pktLen:636,padLen:7,remainLen:632 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: KEXECDH_REPLY +0ms
    datastore:details DEBUG: Checking host key format +0ms
    datastore:details DEBUG: Checking signature format +0ms
    datastore:details DEBUG: Verifying host fingerprint +0ms
    datastore:details DEBUG: Host accepted by default (no verification) +0ms
    datastore:details DEBUG: Verifying signature +1ms
    datastore:details DEBUG: Outgoing: Writing NEWKEYS +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 8) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: pktLen:12,padLen:10,remainLen:8 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: NEWKEYS +0ms
    datastore:details DEBUG: Outgoing: Writing SERVICE_REQUEST (ssh-userauth) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:28,padLen:10,remainLen:16 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +1ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: SERVICE_ACCEPT +0ms
    datastore:details DEBUG: Outgoing: Writing USERAUTH_REQUEST (none) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:44,padLen:19,remainLen:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: USERAUTH_FAILURE +0ms
    datastore:details DEBUG: Client: none auth failed +0ms
    datastore:details DEBUG: Outgoing: Writing USERAUTH_REQUEST (password) +1ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +6ms
    datastore:details DEBUG: Parser: Decrypting +1ms
    datastore:details DEBUG: Parser: pktLen:12,padLen:10,remainLen:0 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: USERAUTH_SUCCESS +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_OPEN (0, session) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +156ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:492,padLen:16,remainLen:480 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +1ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: GLOBAL_REQUEST (hostkeys-00@openssh.com)     +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +43ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:28,padLen:10,remainLen:16 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_OPEN_CONFIRMATION +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_REQUEST (0, subsystem: sftp) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +1ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:28,padLen:18,remainLen:16 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_WINDOW_ADJUST (0, 2097152) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +1ms
    datastore:details DEBUG: Parser: pktLen:12,padLen:6,remainLen:0 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_SUCCESS (0) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_DATA (0) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +1ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:172,padLen:8,remainLen:160 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +1ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_DATA (0) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG[SFTP]: Outgoing: Writing OPEN +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_DATA (0) +1ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:44,padLen:17,remainLen:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_DATA (0) +0ms
    datastore:details DEBUG[SFTP]: Parser: Response: HANDLE +0ms
    datastore:details DEBUG[SFTP]: Outgoing: Writing FSETSTAT +1ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_DATA (0) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:44,padLen:6,remainLen:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +1ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_DATA (0) +0ms
    datastore:details DEBUG[SFTP]: Parser: Response: STATUS +0ms
    datastore:details DEBUG[SFTP]: Outgoing: Writing WRITE +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_DATA (0) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +0ms
    datastore:details DEBUG: Parser: IN_PACKET +1ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: pktLen:44,padLen:6,remainLen:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATA +0ms
    datastore:details DEBUG: Parser: Decrypting +0ms
    datastore:details DEBUG: Parser: HMAC size:32 +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY +0ms
    datastore:details DEBUG: Parser: Verifying MAC +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAVERIFY (Valid HMAC) +0ms
    datastore:details DEBUG: Parser: IN_PACKETDATAAFTER, packet: CHANNEL_DATA (0) +0ms
    datastore:details DEBUG[SFTP]: Parser: Response: STATUS +0ms
    datastore:details DEBUG[SFTP]: Outgoing: Writing CLOSE +0ms
    datastore:details DEBUG: Outgoing: Writing CHANNEL_DATA (0) +0ms
    datastore:details DEBUG: Parser: IN_PACKETBEFORE (expecting 16) +1ms

{!docs/assets/abbreviations.txt!}
