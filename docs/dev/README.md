title: Overview

# Extending this Module

!!! warning

    The following information is only needed to develop within this library.
    If you only want to use this module it is not neccessary to know this.

The development is based on the [alinex core rules](https://alinex.gitlab.io/node-core/standard/).

## Structure

![architecture](architecture.svg)

To make it easy to extend with other formats or protocols the functionality is split in parts within separate directories:

-   `protocol` - different access protocols to retrieve or store a Buffer which may be a string or binary
-   `compression` - compression and packaging support (coming later)
-   `format` - different data formats used to retrieve or store the data structure

Within each directory the `index.ts` contains convenient access methods which will select the correct implementation and type definitions for this type of handler.

## Processing

### Load

1. load into `Buffer` using correct protocol handler
2. uncompress and extract if defined
3. parse `Buffer` into data structure using correct format handler

### Save

1. format data structure using correct format handler into `Buffer`
2. compress and include into archive if defined
3. save `Buffer` to persistent store using correct protocol handler

## DataStore

The DataStore itself contains the following fields:

-   `source` - full URL path to get/set
-   `_source` - URL object to access parts of it

{!docs/assets/stats-pdf-license.txt!}

{!docs/assets/abbreviations.txt!}
