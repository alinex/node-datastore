# Test

This module contains a suite of tests for all it's possibilities. But for some protocols you have to setup your environment first to work correctly. The systems will be started automatically so you don't have to enable all the server components.

The tests will write some files under `/tmp/datastore-test-...`. These may be deleted at any time.

To run the tests you have to give your password as environment variable:

```bash
PASSWORD=mypassword npm run test
```

## SFTP

This uses the `openssh` server:

=== "Arch Linux"

    ```bash
    sudo pacman -S openssh
    ```

=== "Debian/Ubuntu"

    ```bash
    sudo apt-get install openssh -y
    ```

## FTP

To test this you need a local ftp server. To install such run:

=== "Arch Linux"

    ```bash
    sudo pacman -S vsftpd
    ```

=== "Debian/Ubuntu"

    ```bash
    sudo apt-get install vsftpd -y
    ```

To also allow writing enable:

```bash
$ sudo vi /etc/vsftpd.conf
# set: write_enable=YES
```

### Postgres

To test this you need a local postgres server running.

=== "Arch Linux"

    ```bash
    sudo pacman -S postgresql
    ```

=== "Debian/Ubuntu"

    ```bash
    sudo apt-get install postgresql -y
    ```

Then you have to initialize your database:

```bash
$ sudo -u postgres initdb --locale $LANG -E UTF8 -D '/var/lib/postgres/data/'
$ sudo -u postgres createuser $USER
$ sudo -u postgres createdb $USER
$ sudo -u postgres psql -c "alter user $USER with encrypted password '<password>';"
$ sudo -u postgres psql -c "grant all privileges on database $USER to $USER;"
$ psql < test/data/example.sql
```

{!docs/assets/abbreviations.txt!}
